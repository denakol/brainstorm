﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Framework.UnitOfWork.Interfaces
{
    public interface IReadWriteUnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Commits current unit of work.
        /// </summary>
        void Commit();

        /// <summary>
        /// Rollbacks current unit of work.
        /// </summary>
        void Rollback();
    }
}
