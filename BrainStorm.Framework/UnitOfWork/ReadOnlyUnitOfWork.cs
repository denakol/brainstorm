﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrainStorm.Framework.UnitOfWork.Interfaces;
using Microsoft.Practices.Unity;

namespace BrainStorm.Framework.UnitOfWork
{
    public class ReadOnlyUnitOfWork : UnitOfWorkBase, IReadOnlyUnitOfWork
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReadonlyUnitOfWork" /> class.
        /// </summary>
        /// <param name="container">The container.</param>
        public ReadOnlyUnitOfWork(IUnityContainer container) : base(container) { }
    }
}
