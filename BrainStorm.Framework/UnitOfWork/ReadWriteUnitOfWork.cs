﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrainStorm.Framework.UnitOfWork.Interfaces;
using Microsoft.Practices.Unity;

namespace BrainStorm.Framework.UnitOfWork
{
    public class ReadWriteUnitOfWork : UnitOfWorkBase, IReadWriteUnitOfWork
    {
        public ReadWriteUnitOfWork(IUnityContainer container) : base(container) { }

        public void Commit() 
        {
            if (Context != null)
            {
                Context.SaveChanges();
            }
        }

        public void Rollback()
        { 

        }
    }
}
