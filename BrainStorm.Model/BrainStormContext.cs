﻿using System.Data.Entity;
using BrainStorm.Model.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace BrainStorm.Model
{
    public class BrainStormContext : DbContext
    {
        public BrainStormContext() : this("name=BrainStormContext")
        {
        }

        public BrainStormContext(string connectionString) : base(connectionString)
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<ConfirmEmailEntity> ConfirmEmails { get; set; }
        public DbSet<GroupEntity> Groups { get; set; }
        public DbSet<UserGroupEntity> UserGroups { get; set; }
        public DbSet<IdeaEntity> Ideas { get; set; }
        public DbSet<CommentEntity> Comments { get; set; }
        public DbSet<AttachedImageEntity> AttachedImages { get; set; }
        public DbSet<VoteEntity> Votes { get; set; }
        public DbSet<MessageEntity> Messages { get; set; }
        public DbSet<RecipientMessage> RecipietnMessages { get; set; }
    }
}