﻿using System.Data.Entity;
using BrainStorm.Model.Migrations;

namespace BrainStorm.Model
{
    public class BrainStormInitWrapper
    {
        public static void Init()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<BrainStormContext, Configuration>());
        }
    }
}