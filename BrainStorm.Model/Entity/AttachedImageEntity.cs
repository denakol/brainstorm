﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Model.Entity
{
    public class AttachedImageEntity
    {

        public Int32 AttachedImageEntityId { get; set; }
        /// <summary>
        /// Url image
        /// </summary>
        public String Url { get; set; }

        /// <summary>
        /// Idea to which the image is attached
        /// </summary>
        [ForeignKey("Idea")]
        public Int32 IdeaId { get; set; }
        public virtual IdeaEntity Idea { get; set; }
    }
}
