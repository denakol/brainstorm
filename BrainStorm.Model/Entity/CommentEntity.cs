﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Model.Entity
{
    public class CommentEntity
    {

        public Int32 CommentEntityId { get; set; }
        /// <summary>
        /// Text
        /// </summary>
        public String Text { get; set; }

        /// <summary>
        /// Creation date
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Idea where the comment is written
        /// </summary>
        public Nullable<Int32> IdeaId { get; set; }

        /// <summary>
        /// Group where the comment is written
        /// </summary>
        public Nullable<Int32> GroupId { get; set; }
        /// <summary>
        /// Author comment
        /// </summary>
        [ForeignKey("Author")]
        public Int32 AuthorId { get; set; }
        public virtual UserEntity Author { get; set; }
    }
}
