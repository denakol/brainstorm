﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BrainStorm.Model.Entity
{
    public class ConfirmEmailEntity
    {


        public Int32 ConfirmEmailEntityId { get; set; }
        /// <summary>
        /// First Name
        /// </summary>
        public String FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        public String LastName { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public String Password { get; set; }

        /// <summary>
        /// Email user
        /// </summary>
        public String Email { get; set; }


        /// Avatar Image Url
        /// </summary>
        [NotMapped]
        public String ImageUrl { get; set; }

        /// <summary>
        /// Confirm key
        /// </summary>
        public String ConfirmKey { get; set; }
    }
}
