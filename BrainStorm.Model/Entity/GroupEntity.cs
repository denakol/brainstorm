﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BrainStorm.Model.Entity
{
    public class GroupEntity
    {

        public Int32 GroupEntityId { get; set; }

        /// <summary>
        /// Name group
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Task group
        /// </summary>
        public String Task { get; set; }

        /// <summary>
        /// Tags group
        /// </summary>
        public String Tags { get; set; }

        /// <summary>
        /// Creation date
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Group is block
        /// </summary>
        public Boolean IsBlock { get; set; }

        /// <summary>
        /// The group is in the archive
        /// </summary>
        public Boolean Archived { get; set; }

        /// <summary>
        /// The brainstorm has finished
        /// </summary>
        public Boolean Ended { get; set; }

        /// <summary>
        /// Ending date the  brainstorm
        /// </summary>
        public DateTime? DateEnded { get; set; }

        /// <summary>
        /// Members
        /// </summary>
        public virtual ICollection<UserGroupEntity> Members { get; set; }

   
    }
}