﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Model.Entity
{
    public class IdeaEntity
    {
        public Int32 IdeaEntityId { get; set; }

        /// <summary>
        /// Еуче
        /// </summary>
        public String Text { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Code example
        /// </summary>
        public String CodeExample { get; set; }

        /// <summary>
        /// Date created the idea
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Count like
        /// </summary>
        public Int32 CountLike { get; set; }

        /// <summary>
        /// Count dislike
        /// </summary>
        public Int32 CountDislike { get; set; }

        /// <summary>
        /// Attached image to idea
        /// </summary>
        public ICollection<AttachedImageEntity> AttachedImages { get; set; }

        /// <summary>
        /// Comments to idea
        /// </summary>
        public ICollection<CommentEntity> Comments { get; set; }

        /// <summary>
        /// Group where the idea is written
        /// </summary>
        [ForeignKey("Group")]
        public Int32? GroupId { get; set; }
        public virtual GroupEntity Group { get; set; }

        /// <summary>
        /// Author idea
        /// </summary>
        [ForeignKey("Author")]
        public Int32 AuthorId { get; set; }
        public virtual UserEntity Author { get; set; }


    }
}
