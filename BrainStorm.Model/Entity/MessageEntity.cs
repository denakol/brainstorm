﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Model.Entity
{
    public class MessageEntity
    {
        public Int32 MessageEntityId{get;set;}

        /// <summary>
        /// Sender id
        /// </summary>
        [ForeignKey("Sender")]
        public Int32 SenderId { get; set; }
        public virtual UserEntity Sender { get; set; }

        public virtual ICollection<RecipientMessage> Recipients { get; set; }

        /// <summary>
        /// Date created message
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Text
        /// </summary>
        public String Text { get; set; }

        /// <summary>
        /// Message deleted
        /// </summary>
        public Boolean Deleted { get; set; }
    }
}
