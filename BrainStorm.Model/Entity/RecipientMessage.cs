﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Model.Entity
{
    public class RecipientMessage
    {
        public Int32 RecipientMessageId { get; set; }


        /// <summary>
        ///Sent message
        /// </summary>
         [ForeignKey("Message")]
        public Int32 MessageId { get; set; }
        public MessageEntity Message { get; set;}

        /// <summary>
        /// Recipient message
        /// </summary>
        [ForeignKey("Recipient")]
        public Int32 RecipientId { get; set; }
        public virtual UserEntity Recipient { get; set; }

        /// <summary>
        /// Message read
        /// </summary>
        public Boolean Read { get; set; }

        /// <summary>
        /// Message deleted
        /// </summary>
        public Boolean Deleted { get; set; }
    }
}
