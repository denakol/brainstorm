﻿
using System;
using System.Collections.Generic;

namespace BrainStorm.Model.Entity
{
    public class UserEntity 
    
    {
        public UserEntity() { }

        /// <summary>
        /// Created UserEntity form Confirm Email
        /// </summary>
        /// <param name="confirmEmail"></param>
        public UserEntity(ConfirmEmailEntity confirmEmail)
        {
            FirstName = confirmEmail.FirstName;
            LastName = confirmEmail.LastName;
            Password = confirmEmail.Password;
            IsBlock = false;
            IsAdmin = false;
            Email = confirmEmail.Email;
        }


        public Int32 UserEntityId { get; set; }
        /// <summary>
        /// First name
        /// </summary>
        public String FirstName { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        public String LastName { get; set; }

        /// <summary>
        /// Hash password
        /// </summary>
        public String Password { get; set; }

        /// <summary>
        /// User is block
        /// </summary>
        public Boolean IsBlock     { get; set; }

        /// <summary>
        /// About user
        /// </summary>
        public String About { get; set; }

        /// <summary>
        /// Interest user
        /// </summary>
        public String Interest { get; set; }

        /// <summary>
        /// Avatar Image Url
        /// </summary>
        public String ImageUrl { get; set; }

        /// <summary>
        /// User is admin
        /// </summary>
        public Boolean IsAdmin { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public String Email { get; set; }

        /// <summary>
        /// Users's group
        /// </summary>
        public virtual ICollection<UserGroupEntity> Groups { get; set; }

             
    }
}