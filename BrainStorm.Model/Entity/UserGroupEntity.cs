﻿using System;

namespace BrainStorm.Model.Entity
{
    public class UserGroupEntity
    {

        public Int32 UserGroupEntityId { get; set; }

        /// <summary>
        /// Member
        /// </summary>
        public Int32 UserEntityId { get; set; }
        public virtual UserEntity User { get; set; }

        /// <summary>
        /// Group
        /// </summary>
        public Int32 GroupEntityId { get; set; }
        public virtual GroupEntity Group { get; set; }

        /// <summary>
        /// State of the relation
        /// </summary>
        public StateUserGroup State { get; set; }
    }
}