﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Model.Entity
{
    public class VoteEntity
    {
        public Int32 VoteEntityId { get; set; }

        /// <summary>
        /// Vote type
        /// </summary>
        public VoteType VoteType{get;set;}

        /// <summary>
        /// Author id
        /// </summary>
         [ForeignKey("Author")]
        public Int32 AuthorId { get; set; }
        public virtual UserEntity Author { get; set; }

        /// <summary>
        /// Idea id
        /// </summary>
        [ForeignKey("Idea")]
        public Int32 IdeaId { get; set; }
        public virtual IdeaEntity Idea { get; set; }
    }
}
