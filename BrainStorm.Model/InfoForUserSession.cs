﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrainStorm.Model
{
    /// <summary>
    /// Info for user session
    /// </summary>
   public  class InfoForSession
    {
       public Int32 UserEntityId { get; set; }       
    }
}
