﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Model
{
    public enum StateUserGroup
    {
        
        Send = 1,
        Adoptet = 2,
        Invite =3,
        NotSend = 4,
        Block =5,
        Admin=6
    };
}
