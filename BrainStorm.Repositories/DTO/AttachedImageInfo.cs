﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.DTO
{
    public class AttachedImageInfo
    {
        /// <summary>
        /// Url image
        /// </summary>
        [JsonProperty("url")]
        public String Url { get; set; }
    }
}
