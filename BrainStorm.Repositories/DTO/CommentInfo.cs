﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.DTO
{
    public class CommentInfo
    {

        public Int32 CommentId { get; set; }
        /// <summary>
        /// Text
        /// </summary>
        public String Text { get; set; }
        /// <summary>
        /// Creation date
        /// </summary>
        public String Created { get; set; }

        /// <summary>
        /// Idea where the comment is written
        /// </summary>
        public Nullable<Int32> IdeaId { get; set; }

        public Nullable<Int32> GroupId { get; set; }
        /// <summary>
        /// Author comment
        /// </summary>
        public Int32 AuthorId { get; set; }

        /// <summary>
        /// Author name
        /// </summary>
        public String AuthorName { get; set; }

        public String AuthorImage { get; set; }
    }
}
