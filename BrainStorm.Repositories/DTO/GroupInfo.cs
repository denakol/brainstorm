﻿using BrainStorm.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.DTO
{
    public class GroupInfo
    {
        /// <summary>
        /// Id
        /// </summary>
        public Int32 GroupId { get; set; }

        /// <summary>
        /// Group name
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Group Task
        /// </summary>
        public String Task { get; set; }

        /// <summary>
        /// Tags
        /// </summary>
        public String Tags { get; set; }

        /// <summary>
        /// CreationDate
        /// </summary>
        public String CreationDate { get; set; }


        public String DateEnded { get; set; }


        public Boolean Ended { get; set; }
        /// <summary>
        /// Full name admin group
        /// </summary>
        public String AdminName { get; set; }

        /// <summary>
        /// Admin ID
        ///</summary>
        public Int32 AdminId { get; set; }

        /// <summary>
        /// State group for user
        /// </summary>
        public StateUserGroup State { get; set; }

        /// <summary>
        /// Group's member
        /// </summary>
        public IEnumerable<MemberGroupInfo> Members { get; set; }


        /// <summary>
        /// user is admin
        /// </summary>
        public Boolean IsUserAdmin { get; set; }

        /// <summary>
        /// group is block
        /// </summary>
        public Boolean IsBlock { get; set; }

        public Boolean Archived { get; set; }
       
    }
}
