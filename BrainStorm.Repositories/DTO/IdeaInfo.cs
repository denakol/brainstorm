﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.DTO
{
    public class IdeaInfo
    {
        /// <summary>
        /// Id
        /// </summary>
        public Int32 IdeaId;

        /// <summary>
        /// Text
        /// </summary>
        public String Text{get; set;}

        /// <summary>
        /// NAme task
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Code Example
        /// </summary>
        public String CodeExample { get; set; }

        /// <summary>
        /// Сreation date
        /// </summary>
        public String Created { get; set; }

        /// <summary>
        /// The user voted for the idea
        /// </summary>
        public Boolean IsVote { get; set; }
        
        /// <summary>
        /// Count like
        /// </summary>
        public Int32 CountLike { get; set; }

        /// <summary>
        /// Count Dislike
        /// </summary>
        public Int32 CountDislike { get; set; }

        /// <summary>
        /// Attached image
        /// </summary>
        public IEnumerable<AttachedImageInfo> AttachedImages { get; set; }

        /// <summary>
        /// Full name author
        /// </summary>
        public String AuthorName { get; set; }

        public String AuthorImage { get; set; }
        /// <summary>
        /// Author id
        /// </summary>
        public Int32 AuthorId { get; set; }

        /// <summary>
        /// Group Id
        /// </summary>
        public Int32 GroupId { get; set; }
    }
}
