﻿using BrainStorm.Model.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.DTO
{
    public  class InputMessage
    {
        [JsonProperty("messageId")]
        public Int32 MessageEntityId { get; set; }

        /// <summary>
        /// Sender id
        /// </summary>
        [JsonProperty("senderId")]
        public Int32 SenderId { get; set; }

        /// <summary>
        /// sender full name
        /// </summary>
        [JsonProperty("senderFullName")]
        public String SenderFullName { get; set; }

        /// <summary>
        /// Avatar Image Url
        /// </summary>
        [JsonProperty("imageUrl")]
        public String ImageUrl { get; set; }

        /// <summary>
        /// Sender is block
        /// </summary>
        [JsonProperty("isBlock")]
        public Boolean IsBlock { get; set; }

        [JsonProperty("recipients")]
        public virtual IEnumerable<RecipientInfo> Recipients { get; set; }

        /// <summary>
        /// Date created message
        /// </summary>
        [JsonProperty("created")]
        public String Created { get; set; }

        /// <summary>
        /// Text 
        /// </summary>
        [JsonProperty("text")]
        public String Text { get; set; }

        /// <summary>
        /// Message read
        /// </summary>
        [JsonProperty("read")]
        public Boolean Read { get; set; }
    }
}
