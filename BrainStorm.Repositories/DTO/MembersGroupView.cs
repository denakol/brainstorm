﻿using System;
using System.Security.Policy;

namespace BrainStorm.Repositories.DTO
{

    public class MemberGroupInfo
    {
        /// <summary>
        /// User id
        /// </summary>
        public Int32 UserId { get; set; }

        /// <summary>
        /// Avatar image url
        /// </summary>
        public Url ImageUrl { get; set; }
    }
}