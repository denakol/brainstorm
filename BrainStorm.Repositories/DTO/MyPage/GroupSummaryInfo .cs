﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.DTO.MyPage
{
   public class GroupSummaryInfo
    {
        [JsonProperty("groupId")]
        public Int32 GroupId { get; set; }
        [JsonProperty("groupName")]
        public String GroupName { get; set; }
     }
}
