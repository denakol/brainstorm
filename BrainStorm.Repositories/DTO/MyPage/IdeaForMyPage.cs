﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.DTO
{
    public class IdeaForMyPage
    {
        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("ideaId")]
        public Int32 IdeaId;

        /// <summary>
        /// Text
        /// </summary>
         [JsonProperty("text")]
        public String Text { get; set; }

        /// <summary>
        /// NAme task
        /// </summary>
       [JsonProperty("name")]
        public String Name { get; set; }

        /// <summary>
        /// Code Example
        /// </summary>
        [JsonProperty("codeExample")]
        public String CodeExample { get; set; }

        /// <summary>
        /// Сreation date
        /// </summary>
       [JsonProperty("created")]
        public String Created { get; set; }


        /// <summary>
        /// Attached image
        /// </summary>
        [JsonProperty("attachedImages")]
        public IEnumerable<AttachedImageInfo> AttachedImages { get; set; }
        /// <summary>
        /// Group Id
        /// </summary>
       [JsonProperty("groupId")]
        public Nullable<Int32> GroupId { get; set; }


       [JsonProperty("groupName")]
        public String GroupName { get; set; }

       [JsonIgnore]
       public Int32 AuthorId { get; set; }
    }
}
