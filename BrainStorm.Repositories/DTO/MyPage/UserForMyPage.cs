﻿using BrainStorm.Repositories.DTO.MyPage;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.DTO
{
    public class UserForMyPage
    {
        /// <summary>
        /// Id
        /// </summary>
        [JsonProperty("id")]
        public Int32 Id { get; set; }

        /// <summary>
        /// First Name
        /// </summary>
        [JsonProperty("firstName")]
        public String FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
          [JsonProperty("lastName")]
        public String LastName { get; set; }

        /// <summary>
        /// Avatar Image Url
        /// </summary>
          [JsonProperty("imageUrl")]
        public String ImageUrl { get; set; }

        /// <summary>
        /// About user
        /// </summary>
          [JsonProperty("about")]
        public String About { get; set; }

        /// <summary>
        /// Interest
        /// </summary>
          [JsonProperty("interest")]
        public String Interest { get; set; }

        /// <summary>
        /// USer is block
        /// </summary>
         [JsonProperty("isBlock")]
        public Boolean IsBlock { get; set; }

         [JsonProperty("groups")]
         public IEnumerable<GroupSummaryInfo> Groups { get; set; }
    }
}
