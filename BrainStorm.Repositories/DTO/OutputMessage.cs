﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.DTO
{
    public class OutputMessage
    {

        [JsonProperty("messageId")]
        public Int32 MessageEntityId { get; set; }

        /// <summary>
        /// Recipient id
        /// </summary>
        [JsonProperty("recipientId")]
        public Int32 RecipientId { get; set; }

        /// <summary>
        /// Recipient full name
        /// </summary>
        [JsonProperty("recipientFullName")]
        public String RecipientFullName { get; set; }


        /// <summary>
        /// Avatar Image Url
        /// </summary>
        [JsonProperty("imageUrl")]
        public String ImageUrl { get; set; }

        /// <summary>
        /// Date created message
        /// </summary>
        [JsonProperty("created")]
        public String Created { get; set; }

        /// <summary>
        /// Text 
        /// </summary>
        [JsonProperty("text")]
        public String Text { get; set; }

        /// <summary>
        /// Message read
        /// </summary>
        [JsonProperty("read")]
        public Boolean Read { get; set; }
    }
}