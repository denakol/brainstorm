﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.DTO
{
    public class RecipientInfo
    {
        /// <summary>
        /// Recipient 
        /// </summary>
        [JsonIgnore]
        public Int32 RecipientMessageId { get; set; }

        /// <summary>
        /// Sent message
        /// </summary>
        [JsonIgnore]
        public Int32 MessageId { get; set; }

        [JsonProperty("recipientId")]
        public Int32 RecipientId { get; set; }

        /// <summary>
        /// Recipient full name
        /// </summary>
        [JsonProperty("recipientFullName")]
        public String RecipientFullName { get; set; }

        /// <summary>
        /// Avatar Image Url
        /// </summary>
        [JsonProperty("imageUrl")]
        public String ImageUrl { get; set; }

       
        /// <summary>
        /// Message read
        /// </summary>
         [JsonIgnore]
        public Boolean Read { get; set; }

        /// <summary>
        /// Message deleted
        /// </summary>
         [JsonIgnore]
        public Boolean Deleted { get; set; }
    }
}
