﻿using BrainStorm.Model;
using System;
using System.Security.Policy;

namespace BrainStorm.Repositories.DTO
{

    /// <summary>
    /// User info fo views
    /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// Id
        /// </summary>
        public Int32 Id { get; set; }

        /// <summary>
        /// First Name
        /// </summary>
        public String FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        public String LastName { get; set; }

        /// <summary>
        /// Avatar Image Url
        /// </summary>
        public Url ImageUrl { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public String Email { get; set; }

        /// <summary>
        /// About user
        /// </summary>
        public String About { get; set; }

        /// <summary>
        /// Interest
        /// </summary>
        public String Interest { get; set; }

        /// <summary>
        /// USer is block
        /// </summary>
        public Boolean IsBlock { get; set; }

        /// <summary>
        /// User is admin
        /// </summary>
        public Boolean IsAdmin { get; set; }


        /// <summary>
        ///State of the user in the group
        /// </summary>
        public StateUserGroup State { get; set; }
    }
}