﻿using BrainStorm.Model;
using System;
using System.Security.Principal;

namespace BrainStorm.Repositories.DTO
{

    /// <summary>
    /// User info for Session
    /// </summary>
    public class UserSessionInfo : InfoForSession, IPrincipal
    {
        private IIdentity _identity;

        /// <summary>
        /// User id
        /// </summary>
        public Int32 UserId { get; set; }

        /// <summary>
        /// Avatar Url Image
        /// </summary>
        public String ImageUrl { get; set; }

        public String Password { get; set; }
        /// <summary>
        /// user full name
        /// </summary>
        public virtual String UserFullName { get; set; }

        /// <summary>
        /// User is admin
        /// </summary>
        public Boolean IsAdmin { get; set; }

        /// <summary>
        /// User is block
        /// </summary>
        public Boolean IsBlock { get; set; }

        public virtual IIdentity Identity
        {
            get
            {
                if (_identity == null)
                {
                    _identity = new GenericIdentity(UserId.ToString());
                }

                return _identity;
            }
        }

        public virtual Boolean IsInRole(String role)
        {
            return false;
        }
    }
}