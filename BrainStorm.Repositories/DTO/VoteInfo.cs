﻿using BrainStorm.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.DTO
{
    public class VoteInfo
    {
        public Int32 VoteEntityId { get; set; }

        /// <summary>
        /// Like or Dislike
        /// </summary>
        public VoteType VoteType { get; set; }

        /// <summary>
        /// Author id
        /// </summary>
        public Int32 AuthorId { get; set; }

        /// <summary>
        /// Idea id
        /// </summary>
        public Int32 IdeaId { get; set; }
    }
}
