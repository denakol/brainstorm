﻿using BrainStorm.Model;
using BrainStorm.Repositories.DTO;
using BrainStorm.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using BrainStorm.Model.Entity;
using System.Data;

namespace BrainStorm.Repositories.Implementation
{
    public class CommentRepository : ICommentRepository
    {
        private readonly BrainStormContext context;

        public CommentRepository(BrainStormContext context)
        {
            this.context = context;
        }
        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="commentId"></param>
        /// <returns></returns>
        public CommentEntity Get(Int32 commentId)
        {
            return context.Comments.FirstOrDefault(x => x.CommentEntityId == commentId);
        }

        /// <summary>
        /// Get by ideaId
        /// </summary>
        /// <param name="ideaId"></param>
        /// <returns></returns>
        public IEnumerable<CommentEntity> GetByIdeaId(Int32 ideaId)
        {
            return context.Comments.Where(x=>x.IdeaId.HasValue&&x.IdeaId ==ideaId ).Include("Author").ToList();
        }

        /// <summary>
        /// Get by groupId
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public IEnumerable<CommentEntity> GetByGroupId(Int32 groupId)
        {
            return context.Comments.Where(x => x.GroupId.HasValue && x.GroupId == groupId).Include("Author").ToList();
        }

        /// <summary>
        /// Add comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public CommentEntity Add(CommentEntity comment)
        {
            return context.Comments.Add(comment);
        }

        /// <summary>
        /// Update comment
        /// </summary>
        /// <param name="comment"></param>
        public void Update(CommentEntity comment) 
        {
            context.Comments.Attach(comment);
            context.Entry(comment).State = EntityState.Modified;
        }

        /// <summary>
        /// Remove comment
        /// </summary>
        /// <param name="comment"></param>
        public void Remove(CommentEntity comment)
        {
            context.Comments.Remove(comment);
        }
    }
}
