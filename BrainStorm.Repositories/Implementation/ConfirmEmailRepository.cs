﻿using System;
using System.Linq;
using BrainStorm.Model;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.Interfaces;

namespace BrainStorm.Repositories.Implementation
{
    public class ConfirmEmailRepository : IConfirmEmailRepository
    {
        private readonly BrainStormContext context;

        public ConfirmEmailRepository(BrainStormContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get confirmEmail by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public ConfirmEmailEntity GetByEmail(String email)
        {
            return context.ConfirmEmails.SingleOrDefault(x => x.Email == email.ToLower());
        }
        /// <summary>
        /// Get confirm email by email and confirm key
        /// </summary>
        /// <param name="email"></param>
        /// <param name="confirmKey"></param>
        /// <returns></returns>
        public ConfirmEmailEntity GetByEmailConfirmKey(String email, String confirmKey)
        {
            return context.ConfirmEmails.SingleOrDefault(x => (x.Email == email) && (x.ConfirmKey == confirmKey));
        }

        /// <summary>
        /// Add new confrim email
        /// </summary>
        /// <param name="confirmEmail"></param>
        /// <returns></returns>
        public ConfirmEmailEntity Add(ConfirmEmailEntity confirmEmail)
        {
            return context.ConfirmEmails.Add(confirmEmail);
        }

        /// <summary>
        /// Remove confirmEmail
        /// </summary>
        /// <param name="confirmEmail"></param>
        /// <returns></returns>
        public ConfirmEmailEntity Remove(ConfirmEmailEntity confirmEmail)
        {
            ConfirmEmailEntity confirm =
                context.ConfirmEmails.FirstOrDefault(x => x.ConfirmEmailEntityId == confirmEmail.ConfirmEmailEntityId);
            return context.ConfirmEmails.Remove(confirm);
        }
    }
}