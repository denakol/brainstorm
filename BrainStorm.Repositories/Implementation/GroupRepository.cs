﻿using System.Collections.Generic;
using System.Linq;
using BrainStorm.Model;
using BrainStorm.Repositories.DTO;
using BrainStorm.Repositories.Interfaces;
using BrainStorm.Model.Entity;
using System;
using System.Data;

namespace BrainStorm.Repositories.Implementation
{
    public class GroupRepository : IGroupRepository
    {
        private readonly BrainStormContext context;

        public GroupRepository(BrainStormContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get group for user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IEnumerable<GroupEntity> GetForUser(UserSessionInfo user)
        {
            var result = context.Groups.Where(x => (x.IsBlock == false  || user.IsAdmin)&& x.Archived == false);
           return result;
        }
        /// <summary>
        /// Get group for admin
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IEnumerable<GroupEntity> GetForAdminArchiveGroup(Int32 adminId)
        {
            var result = context.UserGroups.Where(x=>x.State==StateUserGroup.Admin&&x.UserEntityId==adminId).Where(x => x.Group.Archived).Select(x=>x.Group);
            return result;
        }


        /// <summary>
        /// Get group by groupId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GroupEntity Get(Int32 id)
        {
           return context.Groups.FirstOrDefault(x=>x.GroupEntityId==id);
           
        }

        /// <summary>
        /// Add new group
        /// </summary>
        /// <param name="groupEntity"></param>
        /// <returns></returns>
        public GroupEntity Add(GroupEntity groupEntity)
        {
            return context.Groups.Add(groupEntity);
        }
        
        /// <summary>
        /// Update group
        /// </summary>
        /// <param name="groupEntity"></param>
        public void Update(GroupEntity groupEntity)
        {
            context.Groups.Attach(groupEntity);
            context.Entry(groupEntity).State = EntityState.Modified;
        }
    }
}