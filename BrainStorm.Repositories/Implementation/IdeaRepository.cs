﻿using BrainStorm.Model;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data;
namespace BrainStorm.Repositories.Implementation
{
    public class IdeaRepository : IIdeaRepository
    {
        private readonly BrainStormContext context;

        public IdeaRepository(BrainStormContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="ideaId"></param>
        /// <returns></returns>
        public IdeaEntity Get(Int32 ideaId) 
        {
            return context.Ideas.Include("Author").Include("AttachedImages").FirstOrDefault(x => x.IdeaEntityId == ideaId);
        }

        /// <summary>
        /// Get ideas by group id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public IEnumerable<IdeaEntity> GetByGroupId(Int32 groupId)
        {
            return context.Ideas.Where(x => x.GroupId == groupId&&x.GroupId!=null).Include("Author").Include("AttachedImages").ToList();
        }

        /// <summary>
        /// Get draft by group id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public IEnumerable<IdeaEntity> GetDraftByUserId(Int32 groupId)
        {
            return context.Ideas.Where(x => x.AuthorId == groupId&&x.GroupId==null).Include("Group").Include("Author").Include("AttachedImages").ToList();
        }
        /// <summary>
        /// Get ideas by user id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public IEnumerable<IdeaEntity> GetByUserId(Int32 groupId)
        {
            return context.Ideas.Where(x => x.AuthorId == groupId && x.GroupId != null).Include("Group").Include("Author").Include("AttachedImages").ToList();
            //.Select(x => new { x.AttachedImages, x.Author }).ToList();
                
        }

        /// <summary>
        /// Add new idea
        /// </summary>
        /// <param name="idea"></param>
        /// <returns></returns>
        public IdeaEntity Add(IdeaEntity idea)
        {
            return context.Ideas.Add(idea);
        }

        /// <summary>
        /// Update idea
        /// </summary>
        /// <param name="idea"></param>
        public void Update(IdeaEntity idea)
        {
            context.Ideas.Attach(idea);
            context.Entry(idea).State = EntityState.Modified;
        }

        /// <summary>
        /// Remove idea
        /// </summary>
        /// <param name="idea"></param>
        public void Remove(IdeaEntity idea)
        {
            var votes = context.Votes.Where(x=>x.IdeaId == idea.IdeaEntityId);
            foreach (var vote in votes)
            {
                context.Votes.Remove(vote);
            }
            var attaches = context.AttachedImages.Where(x => x.IdeaId == idea.IdeaEntityId);
            foreach (var attach in attaches)
            {
                context.AttachedImages.Remove(attach);
            }
            context.Ideas.Remove(idea);
            
        }
    }
}
