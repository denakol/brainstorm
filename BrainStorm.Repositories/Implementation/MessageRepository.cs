﻿using System.Data.Entity;
using BrainStorm.Model;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.Implementation
{
    public class MessageRepository : IMessageRepository
    {
        private readonly BrainStormContext context;

        public MessageRepository(BrainStormContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get by messageId
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public MessageEntity Get(Int32 messageId)
        {
            return context.Messages.Include("Recipients").Include("Recipients.Recipient").Include("Sender").FirstOrDefault(x => x.MessageEntityId == messageId);
        }

        /// <summary>
        /// Get message by messageId and userID
        /// </summary>
        /// <param name="messageId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public MessageEntity Get(Int32 messageId, Int32 userId)
        {
            return context.Messages.Include("Recipients").Include("Sender").FirstOrDefault(x => x.MessageEntityId == messageId);
        }


        /// <summary>
        /// Get messages by userid and typemessage(input or output)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public IEnumerable<MessageEntity> Get(Int32 userId, TypeMessage type)
        {
            if (type == TypeMessage.Input)
            {
                return context.RecipietnMessages.Include("Message").Include("Sender").Where(x => x.RecipientId==userId).Select(x=> x.Message).ToList();
            }
            else
            {
                return context.Messages.Include("Recipients").Include("Sender").Where(x => x.SenderId == userId).ToList();
            }

        }
        /// <summary>
        /// Add message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public MessageEntity Add(MessageEntity message)
        {
            return context.Messages.Add(message);
        }

        /// <summary>
        /// Remove message
        /// </summary>
        /// <param name="message"></param>
        public void Remove(MessageEntity message)
        {
            context.Messages.Remove(message);
        }

        /// <summary>
        /// Update message
        /// </summary>
        /// <param name="message"></param>
        public void Update(MessageEntity message)
        {
            context.Entry(message).State = EntityState.Modified;
        }

    }
}