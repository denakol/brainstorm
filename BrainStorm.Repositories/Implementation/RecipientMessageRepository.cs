﻿using BrainStorm.Model;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.Implementation
{
    public class RecipientMessageRepository : IRecipientMessageRepository
    {
        private readonly BrainStormContext context;

        public RecipientMessageRepository(BrainStormContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get recipient  by messageId and userId
        /// </summary>
        /// <param name="messageId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public RecipientMessage GetByMessageIdUserId(Int32 messageId, Int32 userId)
        {
            return context.RecipietnMessages.FirstOrDefault(x => x.MessageId == messageId && x.RecipientId == userId);
        }
        
        /// <summary>
        /// Update recipient
        /// </summary>
        /// <param name="recipientMessage"></param>
        public void Update(RecipientMessage recipientMessage)
        {
             context.Entry(recipientMessage).State = EntityState.Modified;
        }


        /// <summary>
        /// Number Of Unread Messages
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Int32 NumberOfUnreadMessages(Int32 userId)
        {
            return context.RecipietnMessages.Count(x => x.RecipientId == userId && !x.Read);
        }
    }
}
