﻿using System;
using System.Collections.Generic;
using System.Linq;
using BrainStorm.Model;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using BrainStorm.Repositories.Interfaces;
using System.Data;

namespace BrainStorm.Repositories.Implementation
{
    public class UserGroupRepository : IUserGroupRepository
    {
        private readonly BrainStormContext context;

        public UserGroupRepository(BrainStormContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get UserGroupEntity by group's id and user's id
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserGroupEntity GetByGroupIdUserId(Int32 groupId, Int32 userId)
        {
            return context.UserGroups.SingleOrDefault(x => x.GroupEntityId == groupId && x.UserEntityId == userId);
        }

        /// <summary>
        /// Get group's member
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>        
        public IEnumerable<UserGroupEntity> GetByGroupId(Int32 groupId)
        {
            IEnumerable<UserGroupEntity> source =
                context.UserGroups.Where(x => x.GroupEntityId == groupId).AsEnumerable();
            return source;
        }

        /// <summary>
        ///  group's member by user state 
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public IEnumerable<UserGroupEntity> GetByStateGroupId(Int32 groupId, StateUserGroup state)
        {
            IEnumerable<UserGroupEntity> source =
                context.UserGroups.Where(x => x.GroupEntityId == groupId && x.State == state)
                    .AsEnumerable();
            return source;
        }

        /// <summary>
        /// Get by group id and  state = admin&&adoptet
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public IEnumerable<UserGroupEntity> GetMembers(Int32 groupId)
        {
            IEnumerable<UserGroupEntity> source =
               context.UserGroups.Where(x => x.GroupEntityId == groupId && (x.State == StateUserGroup.Adoptet || x.State == StateUserGroup.Admin || x.State == StateUserGroup.Block))
                   .AsEnumerable();
            return source;
        }

        /// <summary>
        /// Get by state and user id
        /// </summary>
        /// <param name="state"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<UserGroupEntity> GetByStateUserId(StateUserGroup state, Int32 userId)
        {
            IEnumerable<UserGroupEntity> source =
                context.UserGroups.Include("Group").Where(x => x.State == state && x.UserEntityId == userId)
                    .AsEnumerable();
            return source;
        }

        /// <summary>
        /// Add new UserGroup
        /// </summary>
        /// <param name="userGroupEntity"></param>
        /// <returns></returns>
        public UserGroupEntity Add(UserGroupEntity userGroupEntity)
        {
            return context.UserGroups.Add(userGroupEntity);
        }

        /// <summary>
        /// Remove UserGroup
        /// </summary>
        /// <param name="idea"></param>
        public void Remove(UserGroupEntity idea)
        {
            context.UserGroups.Remove(idea);

        }
        /// <summary>
        /// Update UserGroup
        /// </summary>
        /// <param name="userGroup"></param>
        public void Update(UserGroupEntity userGroup)
        {
            context.Entry(userGroup).State = EntityState.Modified;
        }

    }
}
