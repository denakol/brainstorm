﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using BrainStorm.Model;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using BrainStorm.Repositories.Interfaces;

namespace BrainStorm.Repositories.Implementation
{
    public class UserRepository : IUserRepository
    {
        private readonly BrainStormContext context;

        public UserRepository(BrainStormContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserEntity> Get()
        {
            return context.Users.AsEnumerable();
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public UserEntity Get(Int32 id)
        {
            return context.Users.SingleOrDefault(x => x.UserEntityId == id);

        }

        /// <summary>
        /// Get user by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public UserEntity GetByEmail(String email)
        {
            return context.Users.SingleOrDefault(x => x.Email == email.ToLower());
        }

        /// <summary>
        /// Get User by email and hash password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public UserEntity GetUserByEmailPassword(String email, String password)
        {
            return context.Users.SingleOrDefault(x => x.Email == email.ToLower() && x.Password == password);
        }

        /// <summary>
        /// Add userEntity
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public UserEntity Add(UserEntity user)
        {
            return (context.Users.Add(user));
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user"></param>
        public void Update(UserEntity user)
        {
            context.Entry(user).State = EntityState.Modified;
        }

       
    }
}
