﻿using BrainStorm.Model;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.Implementation
{
    public class VoteRepository : IVoteRepository
    {
         private readonly BrainStormContext context;

         public VoteRepository(BrainStormContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get by ideaId and userId
        /// </summary>
        /// <param name="ideaId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
         public VoteEntity GetByIdeaIdUserId(Int32 ideaId, Int32 userId)
         {
             return context.Votes.FirstOrDefault(x => x.IdeaId == ideaId && x.AuthorId == userId);
         }
        /// <summary>
         /// Get the number of likes
        /// </summary>
        /// <param name="ideaId"></param>
        /// <returns></returns>
         public Int32 GetCountLike(Int32 ideaId)
         {
             return context.Votes.Where(x => x.IdeaId == ideaId && x.VoteType ==VoteType.Like).Count();
         }

        /// <summary>
         /// Get the number of dislikes
        /// </summary>
        /// <param name="ideaId"></param>
        /// <returns></returns>
         public Int32 GetCountDislike(Int32 ideaId)
         {
             return context.Votes.Where(x => x.IdeaId == ideaId && x.VoteType == VoteType.Dislike).Count();
         }

        /// <summary>
        /// Add vote
        /// </summary>
        /// <param name="vote"></param>
        /// <returns></returns>
         public VoteEntity Add(VoteEntity vote)
         {
             return context.Votes.Add(vote);
         }
    }
}
