﻿using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.Interfaces
{
    public interface ICommentRepository
    {
        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="commentId"></param>
        /// <returns></returns>
        CommentEntity Get(Int32 commentId);

        /// <summary>
        /// Get by idea id
        /// </summary>
        /// <param name="ideaId"></param>
        /// <returns></returns>
        IEnumerable<CommentEntity> GetByIdeaId(Int32 ideaId);

        /// <summary>
        /// Get by group id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        IEnumerable<CommentEntity> GetByGroupId(Int32 groupId);

        /// <summary>
        /// Add new comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        CommentEntity Add(CommentEntity comment);

        /// <summary>
        /// Update comment
        /// </summary>
        /// <param name="comment"></param>
        void Update(CommentEntity comment);

        /// <summary>
        /// Remove comment
        /// </summary>
        /// <param name="commentId"></param>
        void Remove(CommentEntity commentId);
    }
}
