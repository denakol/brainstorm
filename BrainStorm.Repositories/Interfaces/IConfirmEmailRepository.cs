﻿using System;
using BrainStorm.Model.Entity;

namespace BrainStorm.Repositories.Interfaces
{
    public interface IConfirmEmailRepository
    {
        /// <summary>
        /// Add new confrim email
        /// </summary>
        /// <param name="confirmEmail"></param>
        /// <returns></returns>
        ConfirmEmailEntity Add(ConfirmEmailEntity confirmEmail);

        /// <summary>
        /// Get confirmEmail by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        ConfirmEmailEntity GetByEmail(String email);

        /// <summary>
        /// Get confirm email by email and confirm key
        /// </summary>
        /// <param name="email"></param>
        /// <param name="confirmKey"></param>
        /// <returns></returns>
        ConfirmEmailEntity GetByEmailConfirmKey(String email, String confirmKey);

        /// <summary>
        /// Remove confirmEmail
        /// </summary>
        /// <param name="confirmEmail"></param>
        /// <returns></returns>
        ConfirmEmailEntity Remove(ConfirmEmailEntity confirmEmail);
    }
}