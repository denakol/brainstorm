﻿using System.Collections.Generic;
using BrainStorm.Repositories.DTO;
using BrainStorm.Model.Entity;
using System;

namespace BrainStorm.Repositories.Interfaces
{
    public interface IGroupRepository
    {
        /// <summary>
        /// Get group for user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        IEnumerable<GroupEntity> GetForUser(UserSessionInfo user);

        /// <summary>
        /// Get group for admin
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        IEnumerable<GroupEntity> GetForAdminArchiveGroup(Int32 adminId); 

        /// <summary>
        /// Get group by groupId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        GroupEntity Get(Int32 id);

        /// <summary>
        /// Add new group
        /// </summary>
        /// <param name="groupEntity"></param>
        /// <returns></returns>
        GroupEntity Add(GroupEntity groupEntity);

        /// <summary>
        /// Update group
        /// </summary>
        /// <param name="groupEntity"></param>
        void Update(GroupEntity groupEntity);
    }
}