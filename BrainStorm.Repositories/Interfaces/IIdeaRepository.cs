﻿using BrainStorm.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.Interfaces
{
    public interface IIdeaRepository
    {
        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="ideaId"></param>
        /// <returns></returns>
        IdeaEntity Get(Int32 ideaId);

        /// <summary>
        /// Get ideas by group id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        IEnumerable<IdeaEntity> GetByGroupId(Int32 groupId);

        /// <summary>
        /// Get ideas by group id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
       IEnumerable<IdeaEntity> GetByUserId(Int32 groupId);

       /// <summary>
       /// Get draft by group id
       /// </summary>
       /// <param name="groupId"></param>
       /// <returns></returns>
       IEnumerable<IdeaEntity> GetDraftByUserId(Int32 groupId);
        /// <summary>
        /// Add new idea
        /// </summary>
        /// <param name="idea"></param>
        IdeaEntity Add(IdeaEntity idea);

        /// <summary>
        /// Update idea
        /// </summary>
        /// <param name="comment"></param>
        void Update(IdeaEntity comment);

        /// <summary>
        /// Remove idea
        /// </summary>
        /// <param name="commentId"></param>
        void Remove(IdeaEntity commentId);


    }
}
