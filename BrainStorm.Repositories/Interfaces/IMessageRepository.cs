﻿using BrainStorm.Model;
using BrainStorm.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.Interfaces
{
    public interface IMessageRepository
    {
        /// <summary>
        /// Get by messageId
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns></returns>
        MessageEntity Get(Int32 messageid);

        /// <summary>
        /// Get messages by userid and typemessage(input or output)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        IEnumerable<MessageEntity> Get(Int32 userId, TypeMessage type);


        /// <summary>
        /// Add message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        MessageEntity Add(MessageEntity message);

        /// <summary>
        /// Remove message
        /// </summary>
        /// <param name="message"></param>
        void Remove(MessageEntity message);

        /// <summary>
        /// Update message
        /// </summary>
        /// <param name="message"></param>
        void Update(MessageEntity message);
    }
}