﻿using BrainStorm.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.Interfaces
{
    public interface IRecipientMessageRepository
    {

        /// <summary>
        /// Get recipient  by messageId and userId
        /// </summary>
        /// <param name="messageId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        RecipientMessage GetByMessageIdUserId(Int32 messageId, Int32 userId);

        /// <summary>
        /// Update recipient
        /// </summary>
        /// <param name="recipientMessage"></param>
        void Update(RecipientMessage recipientMessage);

        /// <summary>
        /// Number Of Unread Messages
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
       Int32 NumberOfUnreadMessages(Int32 userId);

    }
}
