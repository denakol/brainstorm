﻿using System;
using System.Collections.Generic;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using BrainStorm.Model;

namespace BrainStorm.Repositories.Interfaces
{
    public interface IUserGroupRepository
    {



        /// <summary>
        /// UserGroupEntity by group's id and user's id
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserGroupEntity GetByGroupIdUserId(Int32 groupId, Int32 userId);


      
        /// <summary>
        /// Get group's member
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>        
        IEnumerable<UserGroupEntity> GetByGroupId(Int32 groupId);   
     
        /// <summary>
        /// Get by Email group's member by user state 
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        IEnumerable<UserGroupEntity> GetByStateGroupId(Int32 groupId, StateUserGroup state);

        /// <summary>
        /// Get by group id and  state = admin&&adoptet
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        IEnumerable<UserGroupEntity> GetMembers(Int32 groupId);

        /// <summary>
        /// Get by state and user id
        /// </summary>
        /// <param name="state"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<UserGroupEntity> GetByStateUserId( StateUserGroup state, Int32 userId);
        
        /// <summary>
        /// Add new UserGroupEntity
        /// </summary>
        /// <param name="userGroupEntity"></param>
        /// <returns></returns>
        UserGroupEntity Add(UserGroupEntity userGroupEntity);

        /// <summary>
        /// Remove UserGroup
        /// </summary>
        /// <param name="idea"></param>
        void Remove(UserGroupEntity idea);
        /// <summary>
        /// Update UserGroup
        /// </summary>
        /// <param name="userGroup"></param>
        void Update(UserGroupEntity userGroup);
       
    }
}