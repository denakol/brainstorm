﻿using System;
using System.Collections.Generic;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;

namespace BrainStorm.Repositories.Interfaces
{
    public interface IUserRepository
    {
        /// <summary>
        /// GetByEmail all users
        /// </summary>
        /// <returns></returns>
        IEnumerable<UserEntity> Get();

        /// <summary>
        /// GetByEmail user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        UserEntity Get(Int32 id);

        /// <summary>
        /// GetByEmail user by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        UserEntity GetByEmail(String email);

        /// <summary>
        /// Add userEntity
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        UserEntity Add(UserEntity user);

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="user"></param>
        void Update(UserEntity user);

        /// <summary>
        /// GetByEmail User by email and hash password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        UserEntity GetUserByEmailPassword(String email, String password);
    }
}
