﻿using BrainStorm.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Repositories.Interfaces
{
    public interface IVoteRepository
    {
        /// <summary>
        /// Get by ideaId and userId
        /// </summary>
        /// <param name="ideaId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        VoteEntity GetByIdeaIdUserId(Int32 ideaId, Int32 userId);

        /// <summary>
        /// Get the number of likes
        /// </summary>
        /// <param name="ideaId"></param>
        /// <returns></returns>
        Int32 GetCountLike(Int32 ideaId);

        /// <summary>
        /// Get the number of dislikes
        /// </summary>
        /// <param name="ideaId"></param>
        /// <returns></returns>
        Int32 GetCountDislike(Int32 ideaId);

        /// <summary>
        /// Add vote
        /// </summary>
        /// <param name="vote"></param>
        /// <returns></returns>
        VoteEntity Add(VoteEntity vote);
    }
}
