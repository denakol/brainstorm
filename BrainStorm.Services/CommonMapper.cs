﻿using System.Collections.Generic;
using AutoMapper;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using System.Linq;
using System;
using System.Security.Policy;
using BrainStorm.Model;
using BrainStorm.Repositories.DTO.MyPage;

namespace BrainStorm.Services
{
    public class CommonMapper
    {
        static CommonMapper()
        {
            Mapper.CreateMap<UserEntity, UserInfo>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserEntityId))
                .ForMember(dest=> dest.ImageUrl, opt=> opt.MapFrom(src =>src.ImageUrl!=null?new Url( src.ImageUrl):null));
            Mapper.CreateMap<UserEntity, UserSessionInfo>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserEntityId))
                .ForMember(dest => dest.UserFullName, opt => opt.MapFrom(src=> String.Format("{0} {1}", src.FirstName, src.LastName)));
            Mapper.CreateMap<UserEntity, MemberGroupInfo>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserEntityId))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.ImageUrl != null ? new Url(src.ImageUrl) : null));
            Mapper.CreateMap<GroupEntity, GroupInfo>()
                .ForMember(dest => dest.GroupId, opt => opt.MapFrom(src => src.GroupEntityId))
                .ForMember(dest => dest.CreationDate, opt => opt.MapFrom(src => src.CreationDate.ToShortDateString()))
                .ForMember(dest => dest.DateEnded, opt => opt.MapFrom(src => src.DateEnded.HasValue? src.DateEnded.Value.ToShortDateString() : null))
                .ForMember(dest => dest.Members, opt => opt.Ignore());
            Mapper.CreateMap<AttachedImageEntity,AttachedImageInfo>();
            Mapper.CreateMap<AttachedImageInfo,AttachedImageEntity>();
            Mapper.CreateMap<IdeaEntity, IdeaInfo>()
                .ForMember(dest => dest.IdeaId, opt => opt.MapFrom(src => src.IdeaEntityId))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => src.Created.ToShortDateString()))
                .ForMember(dest => dest.AuthorName, opt => opt.MapFrom(src => String.Format("{0} {1}", src.Author.FirstName, src.Author.LastName)))
                  .ForMember(dest => dest.AuthorImage, opt => opt.MapFrom(src => src.Author.ImageUrl))
                .ForMember(dest => dest.AttachedImages, opt =>opt.MapFrom(src => src.AttachedImages!=null ? src.AttachedImages.ToList() : null));
            Mapper.CreateMap<CommentEntity, CommentInfo>()
                .ForMember(dest => dest.CommentId, opt => opt.MapFrom(src => src.CommentEntityId))
                 .ForMember(dest => dest.AuthorImage, opt => opt.MapFrom(src => src.Author.ImageUrl))
                 .ForMember(dest => dest.AuthorName, opt => opt.MapFrom(src => String.Format("{0} {1}", src.Author.FirstName, src.Author.LastName)))
                 .ForMember(dest => dest.Created, opt => opt.MapFrom(src => src.Created.ToShortDateString()));

            Mapper.CreateMap<IdeaEntity, IdeaForMyPage>()
                .ForMember(dest => dest.IdeaId, opt => opt.MapFrom(src => src.IdeaEntityId))
                .ForMember(dest => dest.Created, opt => opt.MapFrom(src => src.Created.ToShortDateString()))
                .ForMember(dest =>dest.GroupName, opt=> opt.MapFrom(src =>src.Group.Name))
                .ForMember(dest => dest.AttachedImages, opt => opt.MapFrom(src => src.AttachedImages != null ? src.AttachedImages.ToList() : null));
            Mapper.CreateMap<VoteInfo, VoteEntity>();

            Mapper.CreateMap<RecipientMessage, RecipientInfo>()
                .ForMember(dest => dest.RecipientFullName,
                    opt => opt.MapFrom(src => String.Format("{0} {1}", src.Recipient.FirstName, src.Recipient.LastName)))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.Recipient.ImageUrl));

            Mapper.CreateMap<MessageEntity, InputMessage>()
                 .ForMember(dest => dest.SenderFullName, opt => opt.MapFrom(src => String.Format("{0} {1}", src.Sender.FirstName, src.Sender.LastName)))
                 .ForMember(dest => dest.Created, opt => opt.MapFrom(src => src.Created.ToShortDateString()))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.Sender.ImageUrl))
                .ForMember(dest => dest.IsBlock, opt => opt.MapFrom(src => src.Sender.IsBlock));

           Mapper.CreateMap<UserEntity, UserForMyPage>()
              .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserEntityId))
              .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.ImageUrl))
             .ForMember(dest => dest.Groups, opt=> opt.Ignore());
           Mapper.CreateMap<GroupEntity, GroupSummaryInfo>()
              .ForMember(dest => dest.GroupId, opt => opt.MapFrom(src => src.GroupEntityId))
              .ForMember(dest => dest.GroupName, opt => opt.MapFrom(src => src.Name));
              
        }


        public static IEnumerable<GroupSummaryInfo> MapGroupSummaryInfo(IEnumerable<GroupEntity> source)
        {
            return Mapper.Map<IEnumerable<GroupEntity>, IEnumerable<GroupSummaryInfo>>(source);
        }

        public static UserForMyPage MapUserForMyPage(UserEntity source)
        {
            return Mapper.Map<UserEntity, UserForMyPage>(source);
        }

        public static IdeaForMyPage MapIdeaForMyPage(IdeaEntity source)
        {
            return Mapper.Map<IdeaEntity, IdeaForMyPage>(source);
        }
        public static IEnumerable<IdeaForMyPage> MapIdeaForMyPage(IEnumerable<IdeaEntity> source)
        {
            return Mapper.Map<IEnumerable<IdeaEntity>, IEnumerable<IdeaForMyPage>>(source);
        }

        public static InputMessage MapMessageInfo(MessageEntity source)
        {
            return Mapper.Map<MessageEntity, InputMessage>(source);
        }


        public static IEnumerable<InputMessage> MapMessageInfo(IEnumerable<MessageEntity> source)
        {
            return Mapper.Map<IEnumerable<MessageEntity>, IEnumerable<InputMessage>>(source);
        }


        public static OutputMessage MapMessageInfoOld(MessageEntity source)
        {
            return Mapper.Map<MessageEntity, OutputMessage>(source);
        }


        public static IEnumerable<OutputMessage> MapMessageInfoOld(IEnumerable<MessageEntity> source)
        {
            return Mapper.Map<IEnumerable<MessageEntity>, IEnumerable<OutputMessage>>(source);
        }


        public static IEnumerable<AttachedImageEntity> MapAttachedImageInfo(IEnumerable<AttachedImageInfo> source)
        {
            return Mapper.Map<IEnumerable<AttachedImageInfo>, IEnumerable<AttachedImageEntity>>(source);
        }
        public static VoteEntity MapVoteEntity(VoteInfo source)
        {
            return Mapper.Map<VoteInfo, VoteEntity>(source);
        }
        public static IEnumerable<CommentInfo> MapCommentInfo(IEnumerable<CommentEntity> source)
        {
            return Mapper.Map<IEnumerable<CommentEntity>, IEnumerable<CommentInfo>>(source);
        }

        public static CommentInfo MapCommentInfo(CommentEntity source)
        {
            return Mapper.Map<CommentEntity, CommentInfo>(source);
        }

        public static IEnumerable<UserInfo> MapUserInfo(IEnumerable<UserEntity> source)
        {
            return Mapper.Map<IEnumerable<UserEntity>, IEnumerable<UserInfo>>(source);
        }

        public static MemberGroupInfo MapMembersGroupInfo(UserEntity source)
        {
            return Mapper.Map<UserEntity, MemberGroupInfo>(source);
        }

        public static GroupInfo MapGroupInfo(GroupEntity source)
        {
            return Mapper.Map<GroupEntity, GroupInfo>(source);
        }
        public static UserSessionInfo MapUserSession(UserEntity source)
        {
            return Mapper.Map<UserEntity, UserSessionInfo>(source);
        }

        public static UserInfo MapUserInfo(UserEntity source)
        {
            return Mapper.Map<UserEntity, UserInfo>(source);
        }
        public static UserInfo MapMembersInfo(UserEntity source, StateUserGroup state)
        {
            var member= Mapper.Map<UserEntity, UserInfo>(source);
            member.State = state;
            return member;
        }
        public static  IEnumerable<IdeaInfo> MapIdeaInfo( IEnumerable<IdeaEntity> source)
        {
            return Mapper.Map < IEnumerable<IdeaEntity>,  IEnumerable<IdeaInfo>>(source);
        }
        public static IdeaInfo MapIdeaInfo(IdeaEntity source)
        {
            return Mapper.Map<IdeaEntity,IdeaInfo>(source);
        }





    }


   
}


