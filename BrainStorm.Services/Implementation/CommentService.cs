﻿using BrainStorm.Framework.UnitOfWork.Interfaces;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using BrainStorm.Repositories.Interfaces;
using BrainStorm.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Services.Implementation
{
    public class CommentService : ServiceBase, ICommentService
    {
        public CommentService(IUnitOfWorkFactory factory) : base(factory) { }

        /// <summary>
        /// Get comment by commentId
        /// </summary>
        /// <param name="commentId"></param>
        /// <returns></returns>
        public CommentInfo Get(Int32 commentId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<ICommentRepository>();
                var comment = repo.Get(commentId);
                return CommonMapper.MapCommentInfo(comment);
            }
        }


        /// <summary>
        /// Get ideas comments by idea id
        /// </summary>
        /// <param name="ideaId"></param>
        /// <returns></returns>
        public IEnumerable<CommentInfo> GetByIdeaId(Int32 ideaId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<ICommentRepository>();
                return CommonMapper.MapCommentInfo(repo.GetByIdeaId(ideaId));
            }
        }

        /// <summary>
        /// Get groups comments by group id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public IEnumerable<CommentInfo> GetByGroupId(Int32 groupId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<ICommentRepository>();
                return CommonMapper.MapCommentInfo(repo.GetByGroupId(groupId));
            }
        }

        /// <summary>
        /// Add new comment
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public CommentInfo Add(CommentInfo comment, UserSessionInfo user)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                if (comment.IdeaId.HasValue)
                {
                    comment.GroupId = null;
                }
                var newComment = new CommentEntity();
                newComment.AuthorId = user.UserId;
                newComment.IdeaId = comment.IdeaId;
                newComment.GroupId = comment.GroupId;
                newComment.Created = DateTime.Now;
                newComment.Text = comment.Text.Trim();
                var repo = uow.GetRepository<ICommentRepository>();
                newComment = repo.Add(newComment);
                uow.Commit();
                var repoUser = uow.GetRepository<IUserRepository>();
                newComment.Author = repoUser.Get(newComment.AuthorId);
                return CommonMapper.MapCommentInfo(newComment);
            }
        }

        /// <summary>
        /// Update comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        public CommentInfo Update(CommentInfo comment)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<ICommentRepository>();
                var updateComment = repo.Get(comment.CommentId);
                updateComment.Text = comment.Text.Trim();
                repo.Update(updateComment);
                
                uow.Commit();
                updateComment = repo.Get(updateComment.CommentEntityId);
                var repoUser = uow.GetRepository<IUserRepository>();
                updateComment.Author = repoUser.Get(comment.AuthorId);
                return CommonMapper.MapCommentInfo(updateComment);
            }
        }

        /// <summary>
        /// Remove comment
        /// </summary>
        /// <param name="commentId"></param>
        public void Remove(Int32 commentId)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<ICommentRepository>();
                var comment = repo.Get(commentId);
                repo.Remove(comment);
                uow.Commit();
            }
        }

        /// <summary>
        /// user is author comment
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public Boolean IsUserAuthor(Int32 commentId, UserSessionInfo user)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<ICommentRepository>();
                var comment = repo.Get(commentId);
                if (comment == null)
                {
                    return false;
                }
                return comment.AuthorId == user.UserId;
            }
        }

    }
}
