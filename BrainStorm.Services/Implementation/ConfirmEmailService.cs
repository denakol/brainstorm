﻿using BrainStorm.Framework.UnitOfWork.Interfaces;
using System;
using BrainStorm.Model.Entity;
using BrainStorm.Services.Interfaces;
using BrainStorm.Repositories.Implementation;

namespace BrainStorm.Services.Implementation
{
    public class ConfirmEmailService : ServiceBase, IConfirmEmailService
    {
        public ConfirmEmailService(IUnitOfWorkFactory factory) : base(factory) { }


        /// <summary>
        /// Add confirm email
        /// </summary>
        /// <param name="confirmEmail"></param>
        /// <returns></returns>
        public ConfirmEmailEntity Add(ConfirmEmailEntity confirmEmail)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<ConfirmEmailRepository>();
                var confrimEmailAdded = repo.Add(confirmEmail);
                uow.Commit();
                return confrimEmailAdded;
                
            }
        }

        /// <summary>
        /// GetByEmail confirm email for user
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns></returns>
        public ConfirmEmailEntity Get(String email)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<ConfirmEmailRepository>();
                return repo.GetByEmail(email);
            }
        }

        /// <summary>
        /// GetByEmail confirm email for user
        /// </summary>
        /// <param name="email">Email user</param>
        /// <param name="confirmKey">Key from the letter</param>
        /// <returns></returns>
        public ConfirmEmailEntity Get(String email, String confirmKey)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<ConfirmEmailRepository>();
                return repo.GetByEmailConfirmKey(email,confirmKey);
            }
        }
    }
}
