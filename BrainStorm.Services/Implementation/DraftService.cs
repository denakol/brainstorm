﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrainStorm.Framework.UnitOfWork.Interfaces;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using BrainStorm.Repositories.Interfaces;
using BrainStorm.Services.Interfaces;

namespace BrainStorm.Services.Implementation
{
    public class DraftService : ServiceBase, IDraftService
    {
        public DraftService(IUnitOfWorkFactory factory) : base(factory) { }


        /// <summary>
        /// Get by ideaId
        /// </summary>
        /// <param name="ideaId"></param>
        /// <returns></returns>
        public IdeaForMyPage Get(Int32 ideaId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IIdeaRepository>();
                var idea = CommonMapper.MapIdeaForMyPage(repo.Get(ideaId));
                return idea;
            }
        }

        /// <summary>
        /// Add idea
        /// </summary>
        /// <param name="idea"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public IdeaForMyPage Add(IdeaForMyPage idea, UserSessionInfo user)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {

                var newIdea = new IdeaEntity();
                newIdea.AuthorId = user.UserId;
                newIdea.Name = idea.Name.Trim();
                newIdea.Text = idea.Text.Trim();
                newIdea.CodeExample = idea.CodeExample;
                newIdea.AttachedImages = CommonMapper.MapAttachedImageInfo(idea.AttachedImages).ToList();
                newIdea.Created = DateTime.Now;
                var repo = uow.GetRepository<IIdeaRepository>();
                newIdea = repo.Add(newIdea);
                uow.Commit();
                newIdea = repo.Get(newIdea.IdeaEntityId);
                return CommonMapper.MapIdeaForMyPage(newIdea);
            }

        }

        /// <summary>
        /// Update idea
        /// </summary>
        /// <param name="idea"></param>
        public void Update(IdeaForMyPage idea)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IIdeaRepository>();
                var updateIdea = repo.Get(idea.IdeaId);
                updateIdea.Text = idea.Text.Trim();
                updateIdea.Name = idea.Name.Trim();
                if (idea.GroupId != null) updateIdea.GroupId = idea.GroupId.Value;
                repo.Update(updateIdea);
                uow.Commit();
            }
        }
    }
}
