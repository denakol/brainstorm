﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using BrainStorm.Framework.UnitOfWork.Interfaces;
using BrainStorm.Repositories.DTO;
using BrainStorm.Repositories.Implementation;
using BrainStorm.Services.Interfaces;
using PagedList;
using BrainStorm.Model.Entity;
using BrainStorm.Model;
using BrainStorm.Repositories.Interfaces;

namespace BrainStorm.Services.Implementation
{
    public class GroupService : ServiceBase, IGroupService
    {
        public GroupService(IUnitOfWorkFactory factory)
            : base(factory)
        {
        }


        /// <summary>
        /// Get users group
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<GroupInfo> GetUsersGroup(UserSessionInfo user)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var userGroupRepo = uow.GetRepository<IUserGroupRepository>();
                var inviteUserGroups = userGroupRepo.GetByStateUserId(StateUserGroup.Invite, user.UserId);
                var adoptetUserGroups = userGroupRepo.GetByStateUserId(StateUserGroup.Adoptet, user.UserId);
                var adminGroupsUserGroups = userGroupRepo.GetByStateUserId(StateUserGroup.Admin, user.UserId);
                IEnumerable<GroupInfo> inviteGroups = inviteUserGroups.Select(x => CommonMapper.MapGroupInfo(x.Group)).Where(x => x.IsBlock == false || user.IsAdmin);;
                IEnumerable<GroupInfo> adoptetGroups = adoptetUserGroups.Select(x => CommonMapper.MapGroupInfo(x.Group));
                IEnumerable<GroupInfo> adminGroups = adminGroupsUserGroups.Select(x => CommonMapper.MapGroupInfo(x.Group));
                var groups = adoptetGroups.ToList().Concat(inviteGroups.ToList().Concat(adminGroups.ToList()));
                var result = FillGroupInfo(userGroupRepo, groups, user).Where(x=> x.Archived == false) ;
                return result;
            }
        }

        /// <summary>
        /// Get archive group
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<GroupInfo> GetArchiveGroup(UserSessionInfo user)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IGroupRepository>();
                var groups = repo.GetForAdminArchiveGroup(user.UserId).OrderBy(x => x.GroupEntityId)
                    .Select(x => CommonMapper.MapGroupInfo(x))
                    .ToList();
                var result = new List<GroupInfo>();
                var userGroupRepo = uow.GetRepository<IUserGroupRepository>();
                result = FillGroupInfo(userGroupRepo, groups, user);
                return result;
            }
        }

        /// <summary>
        /// Get group for user
        /// </summary>
        /// <param name="groupId">groupId</param>
        /// <param name="user">User</param>
        /// <returns></returns>
        public GroupInfo GetGroupByUser(Int32 groupId, UserSessionInfo user)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IGroupRepository>();
                var userGroupRepo = uow.GetRepository<IUserGroupRepository>();
                var groupInfo = CommonMapper.MapGroupInfo(repo.Get(groupId));
                groupInfo = FillGroupInfo(userGroupRepo, groupInfo, user);
                groupInfo.IsUserAdmin = user.IsAdmin;
                return groupInfo;
            }
        }

        /// <summary>
        /// Get page groups for user
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="user">Current user</param>
        /// <returns></returns>
        public IEnumerable<GroupInfo> GetPage(int pageNumber, int pageSize, UserSessionInfo user)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IGroupRepository>();
                var groups = repo.GetForUser(user).OrderByDescending(x => x.GroupEntityId).ToPagedList(pageNumber, pageSize)
                    .AsEnumerable()
                    .Select(x => CommonMapper.MapGroupInfo(x))
                    .ToList();
                var result = new List<GroupInfo>();
                var userGroupRepo = uow.GetRepository<IUserGroupRepository>();
                result = FillGroupInfo(userGroupRepo, groups, user);
                return result;
            }
        }

        /// <summary>
        /// Search by name and description and tags.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public IEnumerable<GroupInfo> Search(String query, UserSessionInfo user)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IGroupRepository>();

                var groups =
                   repo.GetForUser(user).Where(n => ((n.Tags != null
                                               && (Regex.IsMatch(n.Tags,
                                                   query, RegexOptions.IgnoreCase)))
                                              || (n.Name != null
                                                  && Regex.IsMatch(n.Name, query, RegexOptions.IgnoreCase))
                                              || (n.Task != null
                                                  && Regex.IsMatch(n.Task, query, RegexOptions.IgnoreCase))))
                                        .Select(x => CommonMapper.MapGroupInfo(x))
                                        .ToList(); ;
                var result = new List<GroupInfo>();
                var userGroupRepo = uow.GetRepository<IUserGroupRepository>();
                result = FillGroupInfo(userGroupRepo, groups, user);
                return result;
            }
        }

        /// <summary>
        /// Add new group
        /// </summary>
        /// <param name="groupInfo">GroupEntity</param>
        /// <param name="admin">Current User</param>
        /// <returns></returns>
        public GroupInfo Add(GroupInfo groupInfo, UserSessionInfo admin)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {

                var groupEntity = new GroupEntity();
                groupEntity.CreationDate = DateTime.Now;
                groupEntity.Name = groupInfo.Name.Trim();
                groupEntity.Task = groupInfo.Task.Trim();
                var member = new List<UserGroupEntity>();
                member.Add(new UserGroupEntity { UserEntityId = admin.UserId, Group = groupEntity, State = StateUserGroup.Admin });
                groupEntity.Members = member;

                var repo = uow.GetRepository<IGroupRepository>();
                repo.Add(groupEntity);
                uow.Commit();

            }
            return groupInfo;
        }


        /// <summary>
        /// Set a field iSblock(Block/Unblock user)
        /// </summary>
        /// <param name="groupInfo"></param>
        public void SetBlock(GroupInfo groupInfo)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IGroupRepository>();
                var group = repo.Get(groupInfo.GroupId);
                group.IsBlock = groupInfo.IsBlock;
                repo.Update(group);
                uow.Commit();
            }
        }



        /// <summary>
        /// Update group
        /// </summary>
        /// <param name="groupInfo"></param>
        public void Update(GroupInfo groupInfo)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IGroupRepository>();
                var group = repo.Get(groupInfo.GroupId);
                group.Task = groupInfo.Task;
                if(groupInfo.DateEnded!=null )
                {
                    group.DateEnded =  Convert.ToDateTime(groupInfo.DateEnded);
                }               
                group.Ended = groupInfo.Ended;
                if (!group.Archived && groupInfo.Archived)
                {
                    group.Archived = groupInfo.Archived;
                }                
                repo.Update(group);
                uow.Commit();
            }
        }

        /// <summary>
        /// Кeturns the user is an administrator of the group
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public Boolean IsUserAdminGroup(Int32 groupId, UserSessionInfo user)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var source = repo.GetByGroupIdUserId(groupId, user.UserId);

                return source!=null?source.State == StateUserGroup.Admin:false;
            }
        }

        /// <summary>
        /// Is user admin group
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
       public  Boolean IsUserAdminGroup(Int32 groupId, Int32 userId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var source = repo.GetByGroupIdUserId(groupId, userId);
                return source != null ? source.State == StateUserGroup.Admin : false;
            }
        }

        /// <summary>
        /// Is brainstorm completed
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public Boolean IsGroupEnded(Int32 groupId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IGroupRepository>();
                var group = repo.Get(groupId);
                return group.Ended;
            }
        }
        /// <summary>
        /// Is group block
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public Boolean IsGroupBlock(Int32 groupId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IGroupRepository>();
                var group = repo.Get(groupId);
                return group.IsBlock;
            }
        }
        /// <summary>
        /// Is group archived
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public Boolean IsGroupArchived(Int32 groupId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IGroupRepository>();
                var group = repo.Get(groupId);
                return group.Archived;
            }
        }


        /// <summary>
        /// In the group, only one administrator
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public Boolean OnlyGroupAdministrator(Int32 groupId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var member = repo.GetMembers(groupId);
                return member.Where(x=> x.State ==StateUserGroup.Admin).Count()==1;
            }
        }

        /// <summary>
        /// Warning! Method Change parameter groupInfo
        /// </summary>
        /// <param name="user"></param>
        /// <param name="groupInfo"></param>
        /// <param name="userGroup"></param>
        private static void AddStateUserInGroup(GroupInfo groupInfo, UserGroupEntity userGroup)
        {
            if (userGroup != null)
            {
                groupInfo.State = userGroup.State;

            }
            else
            {
                groupInfo.State = StateUserGroup.NotSend;
            };
        }
        /// <summary>
        /// Fill group info(admin name and members)
        /// </summary>
        /// <param name="userGroupRepo"></param>
        /// <param name="groups"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private static List<GroupInfo> FillGroupInfo(IUserGroupRepository userGroupRepo, IEnumerable<GroupInfo> groups, UserSessionInfo user)
        {
            var result = new List<GroupInfo>();
            foreach (var groupInfo in groups)
            {
                result.Add(FillGroupInfo(userGroupRepo, groupInfo, user));
            }
            return result;
        }

        /// <summary>
        /// Fill group info(admin name and members)
        /// </summary>
        /// <param name="userGroupRepo"></param>
        /// <param name="groups"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private static GroupInfo FillGroupInfo(IUserGroupRepository userGroupRepo, GroupInfo groupInfo, UserSessionInfo user)
        {


                var source = userGroupRepo.GetMembers(groupInfo.GroupId);
                var admin = source.First(x => x.State == StateUserGroup.Admin).User;
                groupInfo.AdminId = admin.UserEntityId;
                groupInfo.AdminName = String.Format("{0} {1}", admin.FirstName, admin.LastName);
                var members = source.Select(x => CommonMapper.MapMembersGroupInfo(x.User)).ToList();
                groupInfo.Members = members;
                var userGroup = userGroupRepo.GetByGroupIdUserId(groupInfo.GroupId, user.UserId);
                AddStateUserInGroup(groupInfo, userGroup);
                return groupInfo;
        }


    }
}