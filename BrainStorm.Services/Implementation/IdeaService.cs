﻿using BrainStorm.Framework.UnitOfWork.Interfaces;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using BrainStorm.Repositories.Interfaces;
using BrainStorm.Services.Interfaces;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PagedList;
namespace BrainStorm.Services.Implementation
{
    public class IdeaService : ServiceBase, IIdeaService
    {
        public IdeaService(IUnitOfWorkFactory factory) : base(factory) { }


        /// <summary>
        /// Get page idea by userId
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<IdeaForMyPage> GetPageByUserId(Int32 pageNumber, Int32 pageSize, Int32 userId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IIdeaRepository>();
                var result = repo.GetByUserId(userId).Where(x => !(x.Group.Archived || x.Group.Ended)).OrderByDescending(x => x.Created).ToPagedList(pageNumber, pageSize).AsEnumerable();
                return CommonMapper.MapIdeaForMyPage(result);
            }
        }

        /// <summary>
        /// Get page archived idea by userId
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<IdeaForMyPage> GetPageArchiveByUserId(Int32 pageNumber, Int32 pageSize, Int32 userId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IIdeaRepository>();
                var result = repo.GetByUserId(userId).Where(x => x.Group.Archived || x.Group.Ended).OrderByDescending(x => x.Created).ToPagedList(pageNumber, pageSize).AsEnumerable();
                return CommonMapper.MapIdeaForMyPage(result);
            }
        }

        /// <summary>
        /// Get page draft by userId
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<IdeaForMyPage> GetPageDraftByUserId(int pageNumber, int pageSize, int userId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IIdeaRepository>();
                var result = repo.GetDraftByUserId(userId).OrderByDescending(x => x.Created).ToPagedList(pageNumber, pageSize).AsEnumerable();
                return CommonMapper.MapIdeaForMyPage(result);
            }
        }
        /// <summary>
        /// Gte idea by ideaId and user
        /// </summary>
        /// <param name="ideaId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public IdeaInfo Get(Int32 ideaId, UserSessionInfo user)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IIdeaRepository>();

                var idea = CommonMapper.MapIdeaInfo(repo.Get(ideaId));
                var repoVote = uow.GetRepository<IVoteRepository>();

                var repoGroup = uow.GetRepository<IGroupRepository>();
                var group = repoGroup.Get(idea.GroupId);
                var isAdmin = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(idea.GroupId, user.UserId);
                if (group.Ended)
                {
                    idea.CountLike = repoVote.GetCountLike(ideaId);
                    idea.CountDislike = repoVote.GetCountDislike(ideaId);
                    idea.IsVote = true;
                }
                else
                {

                    if (repoVote.GetByIdeaIdUserId(ideaId, user.UserId) != null)
                    {
                        idea.CountLike = repoVote.GetCountLike(ideaId);
                        idea.CountDislike = repoVote.GetCountDislike(ideaId);
                        idea.IsVote = true;
                    }
                    else
                    {
                        if (isAdmin||user.IsAdmin)
                        {
                            idea.CountLike = repoVote.GetCountLike(ideaId);
                            idea.CountDislike = repoVote.GetCountDislike(ideaId);
                            idea.IsVote = false;
                        }
                        else
                        {
                            idea.IsVote = false;
                            idea.CountLike = 0;
                            idea.CountDislike = 0;
                        }
                    }
                }
                return idea;
            }
        }
        /// <summary>
        /// Returns  the group's ideas
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public IEnumerable<IdeaInfo> GetByGroupId(Int32 groupId, UserSessionInfo user)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IIdeaRepository>();
                var repoGroup = uow.GetRepository<IGroupRepository>();
                var ideas = CommonMapper.MapIdeaInfo(repo.GetByGroupId(groupId));
                var repoVote = uow.GetRepository<IVoteRepository>();

                var group = repoGroup.Get(groupId);
                var isAdmin = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(groupId, user.UserId);
                foreach (var idea in ideas)
                {

                    if (group.Ended)
                    {
                        idea.CountLike = repoVote.GetCountLike(idea.IdeaId);
                        idea.CountDislike = repoVote.GetCountDislike(idea.IdeaId);
                        idea.IsVote = true;
                    }
                    else
                    {
                        if (repoVote.GetByIdeaIdUserId(idea.IdeaId, user.UserId) != null)
                        {
                            idea.CountLike = repoVote.GetCountLike(idea.IdeaId);
                            idea.CountDislike = repoVote.GetCountDislike(idea.IdeaId);
                            idea.IsVote = true;
                        }
                        else
                        {
                            if (isAdmin || user.IsAdmin)
                            {
                                idea.CountLike = repoVote.GetCountLike(idea.IdeaId);
                                idea.CountDislike = repoVote.GetCountDislike(idea.IdeaId);
                                idea.IsVote = false;
                            }
                            else
                            {
                                idea.IsVote = false;
                                idea.CountLike = 0;
                                idea.CountDislike = 0;
                            }
                        }
                    }

                }
                return ideas;
            }
        }

        /// <summary>
        /// Add idea
        /// </summary>
        /// <param name="idea"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public IdeaInfo Add(IdeaInfo idea, UserSessionInfo user)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                
                var newIdea = new IdeaEntity();
                newIdea.AuthorId = user.UserId;
                newIdea.GroupId = idea.GroupId;
                newIdea.Name = idea.Name.Trim();                
                newIdea.Text = idea.Text.Trim();
                newIdea.CodeExample = idea.CodeExample;
                newIdea.AttachedImages = CommonMapper.MapAttachedImageInfo(idea.AttachedImages).ToList();
                newIdea.Created = DateTime.Now;
                var repo = uow.GetRepository<IIdeaRepository>();
                newIdea = repo.Add(newIdea);
                uow.Commit();
                newIdea = repo.Get(newIdea.IdeaEntityId);
                return CommonMapper.MapIdeaInfo(newIdea);
            }
            
        }

        /// <summary>
        /// Update idea
        /// </summary>
        /// <param name="idea"></param>
        /// <returns></returns>
        public IdeaInfo Update(IdeaInfo idea)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IIdeaRepository>();
                var updateIdea = repo.Get(idea.IdeaId);
                updateIdea.Text = idea.Text.Trim();
                updateIdea.Name = idea.Name.Trim();
                repo.Update(updateIdea);
                uow.Commit();
                updateIdea = repo.Get(updateIdea.IdeaEntityId);
                var repoUser = uow.GetRepository<IUserRepository>();
                updateIdea.Author = repoUser.Get(idea.AuthorId);
                return CommonMapper.MapIdeaInfo(updateIdea);
            }
        }

        /// <summary>
        /// Remove idea
        /// </summary>
        /// <param name="ideaId"></param>
        public void Remove(Int32 ideaId)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IIdeaRepository>();
                var idea = repo.Get(ideaId);
                repo.Remove(idea);
                uow.Commit();
            }
        }

        /// <summary>
        /// User is author the idea
        /// </summary>
        /// <param name="ideaId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public Boolean IsUserAuthor(Int32 ideaId, UserSessionInfo user)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IIdeaRepository>();
                var idea = repo.Get(ideaId);
                if (idea== null)
                {
                    return false;
                }
                return idea.AuthorId == user.UserId;
            }
        }

      
    }
}
