﻿using BrainStorm.Framework.UnitOfWork.Interfaces;
using BrainStorm.Model;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using BrainStorm.Repositories.Interfaces;
using BrainStorm.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PagedList;
using System.Threading.Tasks;

namespace BrainStorm.Services.Implementation
{
    public class MessageService : ServiceBase, IMessageService
    {
        public MessageService(IUnitOfWorkFactory factory) : base(factory) { }

        /// <summary>
        /// Get messageId
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public InputMessage Get(Int32 messageId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IMessageRepository>();
                var message = repo.Get(messageId);
                return CommonMapper.MapMessageInfo(message);
            }
        }

        /// <summary>
        /// Get output by pageNumber, pageSize, userId
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<OutputMessage> GetOutput(int pageNumber, int pageSize, int userId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IMessageRepository>();
                var result = CommonMapper.MapMessageInfo(repo.Get(userId, TypeMessage.Output).OrderByDescending(x => x.Created).ToPagedList(pageNumber, pageSize));
                var messages = new List<OutputMessage>();
                foreach (var message in result)
                {
                    foreach(var recipient in message.Recipients)
                    {
                        var outputMessage = new OutputMessage();
                        outputMessage.RecipientFullName = recipient.RecipientFullName;
                        outputMessage.ImageUrl = recipient.ImageUrl;
                        outputMessage.RecipientId = recipient.RecipientId;
                        outputMessage.Created = message.Created;
                        outputMessage.Read = recipient.Read;
                        outputMessage.Text = message.Text;
                        outputMessage.MessageEntityId = message.MessageEntityId;
                        messages.Add(outputMessage);
                    }
                    
                }
                return messages;

            }
        }

        /// <summary>
        /// Get input by pageNumber, pageSize, userId
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<InputMessage> GetInput(int pageNumber, int pageSize, int userId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IMessageRepository>();
                var result = repo.Get(userId, TypeMessage.Input).OrderByDescending(x=>x.Created).ToPagedList(pageNumber, pageSize);
                var messages = CommonMapper.MapMessageInfo(result);
                foreach(var message in messages)
                {
                    message.Read = message.Recipients.First(x => x.RecipientId == userId).Read;
                }
                return messages;
            }
        }

        /// <summary>
        /// Read message
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns></returns>
        public void Read(Int32 messageId, Int32 userId)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {

                var repo = uow.GetRepository<IRecipientMessageRepository>();
                var message = repo.GetByMessageIdUserId(messageId, userId);
                if (message != null)
                {
                    message.Read = true;
                }
                repo.Update(message);
                uow.Commit();
            }

        }

        /// <summary>
        /// Send message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public InputMessage SendMessage(InputMessage message)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var newMessage = new MessageEntity();
                newMessage.SenderId = message.SenderId;
                newMessage.Created = DateTime.Now;
                newMessage.Deleted = false;
                newMessage.Text = message.Text;
                newMessage.Recipients = new List<RecipientMessage>();
                foreach(var recipient in message.Recipients)
                {
                    var recipientNew = new RecipientMessage();
                    recipientNew.Read = false;
                    recipientNew.RecipientId = recipient.RecipientId;
                    newMessage.Recipients.Add(recipientNew);            
                }
                var repo = uow.GetRepository<IMessageRepository>();
                newMessage = repo.Add(newMessage);
                uow.Commit();
                newMessage = repo.Get(newMessage.MessageEntityId);
                return CommonMapper.MapMessageInfo(newMessage);

            }

        }

        public void Remove(InputMessage message)
        {

        }

        /// <summary>
        /// Number Of Unread Messages
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Int32 NumberOfUnreadMessages(Int32 userId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IRecipientMessageRepository>();
                return repo.NumberOfUnreadMessages(userId);
            }
        }
    }
}