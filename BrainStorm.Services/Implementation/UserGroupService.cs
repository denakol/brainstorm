﻿using System;
using System.Collections.Generic;
using BrainStorm.Framework.UnitOfWork.Interfaces;
using BrainStorm.Model;
using System.Linq;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using BrainStorm.Repositories.Implementation;
using BrainStorm.Services.Interfaces;
using BrainStorm.Repositories.Interfaces;

namespace BrainStorm.Services.Implementation
{
    public class UserGroupService : ServiceBase, IUserGroupService
    {
        public UserGroupService(IUnitOfWorkFactory factory) : base(factory)
        {
        }

        /// <summary>
        /// Get a group member
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserInfo GetMemberByGroupIdUserId(Int32 groupId, Int32 userId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var source = repo.GetByGroupIdUserId(groupId, userId);
                if (source == null)
                {
                    return null;
                }
                return CommonMapper.MapUserInfo(source.User);
            }
        }

        /// <summary>
        /// Get memeber for group
        /// </summary>
        /// <param name="groupId">group id</param>
        /// <returns></returns>
        public IEnumerable<UserInfo> GetMembers(Int32 groupId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var source = repo.GetMembers(groupId);
                IEnumerable<UserInfo> result = source.Select(x => CommonMapper.MapMembersInfo(x.User,x.State));
                return result.ToList();
            }
        }

        /// <summary>
        /// Get user requests
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public IEnumerable<UserInfo> GetRequests(Int32 groupId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var source = repo.GetByStateGroupId(groupId, StateUserGroup.Send);
                IEnumerable<UserInfo> result = source.Select(x => CommonMapper.MapUserInfo(x.User));
                return result.ToList();
            }
        }
      
        /// <summary>
        /// Send Request to join in group
        /// </summary>
        /// <param name="groupId">group id</param>
        /// <param name="userId">user id</param>
        public void SendRequestToJoin(int groupId, int userId)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var userGroupEntity = new UserGroupEntity
                {
                    GroupEntityId = groupId,
                    UserEntityId = userId,
                    State = StateUserGroup.Send
                };
                repo.Add(userGroupEntity);
                uow.Commit();
            }
        }

        /// <summary>
        /// Send invite user
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        public void SendInvite(Int32 groupId, Int32 userId)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var userGroupEntity = new UserGroupEntity
                {
                    GroupEntityId = groupId,
                    UserEntityId = userId,
                    State = StateUserGroup.Invite
                };
                repo.Add(userGroupEntity);
                uow.Commit();
            }
        }

        /// <summary>
        /// Confirm invite
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        public void ConfirmInvite(Int32 groupId, Int32 userId) 
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var userGroupEntity = repo.GetByGroupIdUserId(groupId,  userId);
                userGroupEntity.State = StateUserGroup.Adoptet;
                repo.Update(userGroupEntity);
                uow.Commit();
            }
        }
        /// <summary>
        /// Assing admin
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        public void AssignAdmin(Int32 groupId, Int32 userId)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var userGroupEntity = repo.GetByGroupIdUserId(groupId, userId);
                userGroupEntity.State = StateUserGroup.Admin;
                repo.Update(userGroupEntity);
                uow.Commit();
            }

        }
        /// <summary>
        /// Block user
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        public void Block(Int32 groupId, Int32 userId)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var userGroupEntity = repo.GetByGroupIdUserId(groupId, userId);
                userGroupEntity.State = StateUserGroup.Block;
                repo.Update(userGroupEntity);
                uow.Commit();
            }

        }

        /// <summary>
        /// Delete invite
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        public void DeleteInvite(Int32 groupId, Int32 userId)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var userGroupEntity = repo.GetByGroupIdUserId(groupId, userId);
                repo.Remove(userGroupEntity);
                uow.Commit();
            }
        }

        /// <summary>
        /// Get the status of a member of group
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public StateUserGroup GetStateByGroupIdUserId(Int32 groupId, Int32 userId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
                var source = repo.GetByGroupIdUserId(groupId, userId);
                return source.State;
            }
        }

        /// <summary>
        /// Get count users invite 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Int32 GetCountInvites(Int32 userId)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var userGroupRepo = uow.GetRepository<IUserGroupRepository>();
                var inviteUserGroups = userGroupRepo.GetByStateUserId(StateUserGroup.Invite, userId);
                return inviteUserGroups.Count();
            }
        }
        /// <summary>
        /// Return the user is a group member 
        /// </summary>
        /// <param name="groupId">group id</param>
        /// <param name="user">user id</param>
        /// <returns></returns>
        public Boolean IsUserGroupMember(Int32 groupId, UserSessionInfo user)
        {
            using (var uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserGroupRepository>();
            
                var member = repo.GetMembers(groupId);
                return member.Any(x => x.UserEntityId == user.UserId&&x.State!=StateUserGroup.Block);
            }
        }
    }
}
