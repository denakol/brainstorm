﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using BrainStorm.Framework.UnitOfWork.Interfaces;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using BrainStorm.Repositories.Implementation;
using BrainStorm.Services.Interfaces;
using PagedList;
using BrainStorm.Repositories.Interfaces;
using BrainStorm.Model;

namespace BrainStorm.Services.Implementation
{
    public class UserService : ServiceBase, IUserService
    {
        public UserService(IUnitOfWorkFactory factory)
            : base(factory)
        {
        }

        /// <summary>
        /// Get All users
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserInfo> Get()
        {
            using (IReadOnlyUnitOfWork uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                var source = repo.Get();
                return CommonMapper.MapUserInfo(source).ToList();
            }
        }

        /// <summary>
        /// Get User by email   
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns></returns>
        public UserEntity Get(String email)
        {
            using (IReadOnlyUnitOfWork uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                return repo.GetByEmail(email);
            }
        }

        /// <summary>
        /// Get user info for Session by email
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns></returns>
        public UserSessionInfo GetUserSessionInfo(Int32 userId)
        {
            using (IReadOnlyUnitOfWork uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                var source = repo.Get(userId);
                return CommonMapper.MapUserSession(source);

            }
        }

        /// <summary>
        /// Get user info include group
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserForMyPage GetUserForMyPage(Int32 userId)
        {
            using (IReadOnlyUnitOfWork uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                var source = repo.Get(userId);
                var user = CommonMapper.MapUserForMyPage(source);

                var userGroupRepo = uow.GetRepository<IUserGroupRepository>();
                var adoptetUserGroups = userGroupRepo.GetByStateUserId(StateUserGroup.Adoptet, user.Id);
                var adminGroupsUserGroups = userGroupRepo.GetByStateUserId(StateUserGroup.Admin, user.Id);
                var adoptetGroups = adoptetUserGroups.Select(x => x.Group);
                var adminGroups = adminGroupsUserGroups.Select(x => x.Group);
                var groups = adoptetGroups.ToList().Concat(adminGroups.ToList());
                user.Groups = CommonMapper.MapGroupSummaryInfo(groups.Where(x => !(x.Ended || x.Ended)));
                return user;
            }
        }

        /// <summary>
        /// Get user info for session by email and password
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="password">Hash password</param>
        /// <returns></returns>
        public UserSessionInfo GetUserSessionInfo(String email, String password)
        {
            using (IReadOnlyUnitOfWork uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                var source = repo.GetUserByEmailPassword(email, password);
                return CommonMapper.MapUserSession(source);
            }
        }


        /// <summary>
        /// Get user info
        /// </summary>
        /// <param name="id"> id user</param>
        /// <returns></returns>
        public UserInfo GetUserInfo(Int32 id)
        {
            using (IReadOnlyUnitOfWork uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                var source = repo.Get(id);
                return CommonMapper.MapUserInfo(source);
            }
        }

        /// <summary>
        /// Get page users
        /// </summary>
        /// <param name="pageNumber">"Page number</param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IEnumerable<UserInfo> GetPage(int pageNumber, int pageSize)
        {
            using (IReadOnlyUnitOfWork uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                var source = repo.Get().OrderBy(x => x.UserEntityId).ToPagedList(pageNumber, pageSize).AsEnumerable();
                return CommonMapper.MapUserInfo(source);
            }
        }

        /// <summary>
        /// Add new user
        /// </summary>
        /// <param name="user">Added User</param>
        /// <returns></returns>
        public UserEntity Add(UserEntity user)
        {
            using (IReadWriteUnitOfWork uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                UserEntity userAdded = repo.Add(user);
                uow.Commit();
                return userAdded;
            }
        }

        /// <summary>
        /// Update user password
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="password">Hash password</param>
        /// <returns></returns>
        public UserEntity UpdatePassword(String email, String password)
        {
            using (IReadWriteUnitOfWork uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                UserEntity user = repo.GetByEmail(email);
                if (user != null)
                {
                    user.Password = password;
                    repo.Update(user);
                    uow.Commit();
                    return user;
                }
                return null;
            }
        }

        /// <summary>
        /// Update user password
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public UserEntity UpdatePassword(Int32 userId, String password)
        {
            using (IReadWriteUnitOfWork uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                UserEntity user = repo.Get(userId);
                if (user != null)
                {
                    user.Password = password;
                    repo.Update(user);
                    uow.Commit();
                    return user;
                }
                return null;
            }
        }

        /// <summary>
        /// Update interest and about
        /// </summary>
        /// <param name="newUser"></param>
        public void UpdateInfo(UserForMyPage newUser)
        {
            using (IReadWriteUnitOfWork uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                UserEntity user = repo.Get(newUser.Id);
                if (user != null)
                {
                    user.Interest = newUser.Interest;
                    user.About = newUser.About;
                    repo.Update(user);
                    uow.Commit();
                }
            }
        }

        /// <summary>
        /// Assign admin or block/unblock user
        /// </summary>
        /// <param name="userUpdate"></param>
        public void UpdateUserAdmin(UserInfo userUpdate)
        {
            using (IReadWriteUnitOfWork uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                UserEntity user = repo.Get(userUpdate.Id);
                user.IsBlock = userUpdate.IsBlock;
                user.IsAdmin = userUpdate.IsAdmin;
                repo.Update(user);
                uow.Commit();

            }
        }

        /// <summary>
        /// Update avatar
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="file"></param>
        public void UpdateAvatar(Int32 userId, String file)
        {
            using (IReadWriteUnitOfWork uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                UserEntity user = repo.Get(userId);
                user.ImageUrl = file;
                repo.Update(user);
                uow.Commit();

            }
        }
        /// <summary>
        /// Add user and delete confirm email
        /// </summary>
        /// <param name="confirmEmail"> confirmEmail</param>
        /// <returns></returns>
        public UserEntity ConfirmRegister(ConfirmEmailEntity confirmEmail)
        {
            using (IReadWriteUnitOfWork uow = factory.CreateReadWriteUnitOfWork())
            {
                var repoUser = uow.GetRepository<IUserRepository>();
                var repoConfirmEmail = uow.GetRepository<ConfirmEmailRepository>();
                var user = new UserEntity(confirmEmail);
                user.ImageUrl = confirmEmail.ImageUrl;
                UserEntity userAdded = repoUser.Add(user);
                repoConfirmEmail.Remove(confirmEmail);
                uow.Commit();
                return userAdded;
            }
        }

        /// <summary>
        /// Search by name and interests.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<UserInfo> SearchByNameInterest(String query)
        {
            using (IReadOnlyUnitOfWork uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                IEnumerable<UserEntity> source =
                    repo.Get().Where(n => (Regex.IsMatch(String.Format("{0} {1}", n.FirstName, n.LastName),
                        query, RegexOptions.IgnoreCase))
                                          || (n.Interest != null
                                              && Regex.IsMatch(n.Interest, query, RegexOptions.IgnoreCase)))
                        .AsEnumerable();
                return CommonMapper.MapUserInfo(source);
            }
        }

        /// <summary>
        /// Search by name.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<UserInfo> SearchByName(String query)
        {
            using (IReadOnlyUnitOfWork uow = factory.CreateReadOnlyUnitOfWork())
            {
                var repo = uow.GetRepository<IUserRepository>();
                IEnumerable<UserEntity> source =
                    repo.Get().Where(n => (Regex.IsMatch(String.Format("{0} {1}", n.FirstName, n.LastName),
                        query, RegexOptions.IgnoreCase)))
                        .AsEnumerable();
                return CommonMapper.MapUserInfo(source);
            }
        }
    }
}