﻿using BrainStorm.Framework.UnitOfWork.Interfaces;
using BrainStorm.Repositories.DTO;
using BrainStorm.Repositories.Interfaces;
using BrainStorm.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Services.Implementation
{
    public class VoteService: ServiceBase, IVoteService
    {
        public VoteService(IUnitOfWorkFactory factory): base(factory)
        {
        }


        /// <summary>
        /// Vote
        /// </summary>
        /// <param name="voteInfo"></param>
        public void DoVote(VoteInfo voteInfo)
        {
            using (var uow = factory.CreateReadWriteUnitOfWork())
            {
                var repo = uow.GetRepository<IVoteRepository>();
                if (repo.GetByIdeaIdUserId(voteInfo.IdeaId, voteInfo.AuthorId)==null)
                {
                    repo.Add(CommonMapper.MapVoteEntity(voteInfo));
                    uow.Commit();
                }
                
            }
        }
    }
}
