﻿using BrainStorm.Repositories.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Services.Interfaces
{
    public interface ICommentService
    {

        /// <summary>
        /// GEt comment by commentId
        /// </summary>
        /// <param name="commentId"></param>
        /// <returns></returns>
        CommentInfo Get(Int32 commentId);
        /// <summary>
        /// Get ideas comments by idea id
        /// </summary>
        /// <param name="ideaId"></param>
        /// <returns></returns>
        IEnumerable<CommentInfo> GetByIdeaId(Int32 ideaId);

        /// <summary>
        /// Get groups comments by group id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        IEnumerable<CommentInfo> GetByGroupId(Int32 groupId);

        /// <summary>
        /// Add new comment
        /// </summary>
        /// <param name="comment"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        CommentInfo Add(CommentInfo comment, UserSessionInfo user);

        /// <summary>
        /// Update comment
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        CommentInfo Update(CommentInfo comment);

        /// <summary>
        /// Remove comment
        /// </summary>
        /// <param name="commentId"></param>
        void Remove(Int32 commentId);

        /// <summary>
        /// user is author comment
        /// </summary>
        /// <param name="commentId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Boolean IsUserAuthor(Int32 commentId, UserSessionInfo user);
    }
}
