﻿using BrainStorm.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Services.Interfaces
{
    public interface IConfirmEmailService
    {
        /// <summary>
        /// Add confirm email
        /// </summary>
        /// <param name="confirmEmail"></param>
        /// <returns></returns>
        ConfirmEmailEntity Add(ConfirmEmailEntity confirmEmail);

        /// <summary>
        /// Get confirm email for user
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns></returns>
        ConfirmEmailEntity Get(String email);

        /// <summary>
        /// Get confirm email for user
        /// </summary>
        /// <param name="email">Email user</param>
        /// <param name="confirmKey">Key from the letter</param>
        /// <returns></returns>
        ConfirmEmailEntity Get(String email, String confirmKey);
    }
}
