﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrainStorm.Repositories.DTO;

namespace BrainStorm.Services.Interfaces
{
    public interface IDraftService
    {
        /// <summary>
        /// Get by ideaId
        /// </summary>
        /// <param name="ideaId"></param>
        /// <returns></returns>
        IdeaForMyPage Get(Int32 ideaId);

        /// <summary>
        /// Add idea
        /// </summary>
        /// <param name="idea"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        IdeaForMyPage Add(IdeaForMyPage idea, UserSessionInfo user);

        /// <summary>
        /// Update idea
        /// </summary>
        /// <param name="idea"></param>
        void Update(IdeaForMyPage idea);
    }
}
