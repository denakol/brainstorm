﻿using BrainStorm.Repositories.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Services.Interfaces
{
    public interface IGroupService
    {
        /// <summary>
        /// Get page groups for user
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="user">Current user</param>
        /// <returns></returns>
        IEnumerable<GroupInfo> GetPage(int pageNumber, int pageSize, UserSessionInfo user);

        /// <summary>
        /// Get users group
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<GroupInfo> GetUsersGroup(UserSessionInfo user);

        /// <summary>
        /// Get archive group
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<GroupInfo> GetArchiveGroup(UserSessionInfo user);

        /// <summary>
        /// Search by name and description and tags.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        IEnumerable<GroupInfo> Search(String query, UserSessionInfo user);

        /// <summary>
        /// Add new group
        /// </summary>
        /// <param name="groupInfo">GroupEntity</param>
        /// <param name="admin">Current User</param>
        /// <returns></returns>
        GroupInfo Add(GroupInfo groupInfo, UserSessionInfo admin);

        /// <summary>
        /// Get group for user
        /// </summary>
        /// <param name="groupId">groupId</param>
        /// <param name="user">User</param>
        /// <returns></returns>
        GroupInfo GetGroupByUser(Int32 groupId, UserSessionInfo user);


        /// <summary>
        /// Set IsBlock
        /// </summary>
        /// <param name="groupInfo"></param>
        void SetBlock(GroupInfo groupInfo);


        /// <summary>
        /// Update group
        /// </summary>
        /// <param name="groupInfo"></param>
        void Update(GroupInfo groupInfo);


        /// <summary>
        /// Returns the user is an administrator of the group
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        Boolean IsUserAdminGroup(Int32 groupId, UserSessionInfo user);

        /// <summary>
        /// Is user admin group
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Boolean IsUserAdminGroup(Int32 groupId, Int32 user);

        /// <summary>
        /// Is brainstorm completed
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        Boolean IsGroupEnded(Int32 groupId);

        /// <summary>
        /// Is group block
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        Boolean IsGroupBlock(Int32 groupId);

        /// <summary>
        /// Is group archived
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        Boolean IsGroupArchived(Int32 groupId);

        /// <summary>
        /// In the group, only one administrator
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        Boolean OnlyGroupAdministrator(Int32 groupId);
    }
}
