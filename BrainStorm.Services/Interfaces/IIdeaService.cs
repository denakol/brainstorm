﻿using BrainStorm.Repositories.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Services.Interfaces
{
    public interface IIdeaService
    {

        IdeaInfo Get(Int32 ideaId, UserSessionInfo user);


        /// <summary>
        /// Returns  the group's ideas
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>       
       IEnumerable<IdeaInfo> GetByGroupId(Int32 groupId, UserSessionInfo user);


       /// <summary>
       /// Get page idea by userId
       /// </summary>
       /// <param name="pageNumber"></param>
       /// <param name="pageSize"></param>
       /// <param name="userId"></param>
       /// <returns></returns>
       IEnumerable<IdeaForMyPage> GetPageByUserId(Int32 pageNumber, Int32 pageSize, Int32 userId);


       /// <summary>
       /// Get page archived idea by userId
       /// </summary>
       /// <param name="pageNumber"></param>
       /// <param name="pageSize"></param>
       /// <param name="userId"></param>
       /// <returns></returns>
       IEnumerable<IdeaForMyPage> GetPageArchiveByUserId(Int32 pageNumber, Int32 pageSize, Int32 userId);


       /// <summary>
       /// Get page draft by userId
       /// </summary>
       /// <param name="pageNumber"></param>
       /// <param name="pageSize"></param>
       /// <param name="userId"></param>
       /// <returns></returns>
       IEnumerable<IdeaForMyPage> GetPageDraftByUserId(int pageNumber, int pageSize, int userId);


        /// <summary>
        /// Add new idea
        /// </summary>
        /// <param name="idea"></param>
        /// <param name="user"></param>
        /// <returns></returns>
       IdeaInfo Add(IdeaInfo idea, UserSessionInfo user);


       /// <summary>
       /// Update idea
       /// </summary>
       /// <param name="idea"></param>
       /// <returns></returns>
       IdeaInfo Update(IdeaInfo comment);

       /// <summary>
       /// Remove idea
       /// </summary>
       /// <param name="ideaId"></param>
       void Remove(Int32 ideatId);

       /// <summary>
       /// User is author the idea
       /// </summary>
       /// <param name="ideaId"></param>
       /// <param name="user"></param>
       /// <returns></returns>
       Boolean IsUserAuthor(Int32 commentId, UserSessionInfo user);

    }
}
