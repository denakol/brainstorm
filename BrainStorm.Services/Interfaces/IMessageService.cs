﻿using BrainStorm.Repositories.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Services.Interfaces
{
    public interface IMessageService
    {
        /// <summary>
        /// Get messageId
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns></returns>
        InputMessage Get(Int32 messageId);

        /// <summary>
        /// Get output by pageNumber, pageSize, userId
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<OutputMessage> GetOutput(int pageNumber, int pageSize, Int32 userId);

        /// <summary>
        /// Get input by pageNumber, pageSize, userId
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        IEnumerable<InputMessage> GetInput(int pageNumber, int pageSize, Int32 userId);

        /// <summary>
        /// Read message
        /// </summary>
        /// <param name="messageId"></param>
        /// <returns></returns>
        void Read(Int32 messageId, Int32 userId);

        /// <summary>
        /// Send message
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        InputMessage SendMessage(InputMessage message);

        void Remove(InputMessage message);

        /// <summary>
        /// Number Of Unread Messages
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Int32 NumberOfUnreadMessages(Int32 userId);
    }
}