﻿using BrainStorm.Model;
using BrainStorm.Repositories.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Services.Interfaces
{
    public interface IUserGroupService
    {



        /// <summary>
        /// Get a group member
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserInfo GetMemberByGroupIdUserId(Int32 groupId, Int32 userId);

        /// <summary>
        /// GetByEmail memeber for group
        /// </summary>
        /// <param name="groupId">group id</param>
        /// <returns></returns>
        IEnumerable<UserInfo> GetMembers(Int32 groupId);

        /// <summary>
        /// Get user requests
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        IEnumerable<UserInfo> GetRequests(Int32 groupId);
        /// <summary>
        /// Send Request to join in group
        /// </summary>
        /// <param name="groupId">group id</param>
        /// <param name="userId">user id</param>
        void SendRequestToJoin(Int32 groupId, Int32 userId);

        /// <summary>
        /// Send invite user
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        void SendInvite(Int32 groupId, Int32 userId);

        /// <summary>
        /// Confirm invite
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        void ConfirmInvite(Int32 groupId, Int32 userId);


        /// <summary>
        /// Assing admin
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        void AssignAdmin(Int32 groupId, Int32 userId);

        /// <summary>
        /// Block user
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        void Block(Int32 groupId, Int32 userId);
        /// <summary>
        /// Delete invite
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        void DeleteInvite(Int32 groupId, Int32 userId);
        /// <summary>
        /// Get the status of a member of group
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        StateUserGroup GetStateByGroupIdUserId(Int32 groupId, Int32 userId);

        /// <summary>
        /// Get count users invite 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Int32 GetCountInvites(Int32 userId);

        /// <summary>
        /// Return the user is a group member 
        /// </summary>
        /// <param name="groupId">group id</param>
        /// <param name="user">user id</param>
        /// <returns></returns>
        Boolean IsUserGroupMember(Int32 groupId, UserSessionInfo user);
    }
}
