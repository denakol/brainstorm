﻿using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Services.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Get All users
        /// </summary>
        /// <returns></returns>
        IEnumerable<UserInfo> Get();

        /// <summary>
        /// Get User by email   
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns></returns>
        UserEntity Get(String email);

        /// <summary>
        /// Add new user
        /// </summary>
        /// <param name="user">Added User</param>
        /// <returns></returns>
        UserEntity Add(UserEntity user);

        /// <summary>
        /// Get user info for Session by email
        /// </summary>
        /// <param name="email">User email</param>
        /// <returns></returns>
        UserSessionInfo GetUserSessionInfo(Int32 userId);

        /// <summary>
        /// Get user info for session by email and password
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="password">Hash password</param>
        /// <returns></returns>
        UserSessionInfo GetUserSessionInfo(String email, String password);

        /// <summary>
        /// Get user info include group
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        UserForMyPage GetUserForMyPage(Int32 userId);

        /// <summary>
        /// Get user info
        /// </summary>
        /// <param name="id"> id user</param>
        /// <returns></returns>
        UserInfo GetUserInfo(Int32 id);    
   
        /// <summary>
        /// Add user and delete confirm email
        /// </summary>
        /// <param name="confirmEmail"> confirmEmail</param>
        /// <returns></returns>
        UserEntity ConfirmRegister(ConfirmEmailEntity confirmEmail);

        /// <summary>
        /// Update user password
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="password">Hash password</param>
        /// <returns></returns>
        UserEntity UpdatePassword(String email, String password);

        /// <summary>
        /// Update user password
        /// </summary>
        /// <param name="email">User email</param>
        /// <param name="password">Hash password</param>
        /// <returns></returns>
        UserEntity UpdatePassword(Int32 userId, String password);

        /// <summary>
        /// Assign admin or block/unblock user
        /// </summary>
        /// <param name="userUpdate"></param>
        void UpdateUserAdmin(UserInfo user);

        /// <summary>
        /// Update interest and about
        /// </summary>
        /// <param name="newUser"></param>
        void UpdateInfo(UserForMyPage newUser);

        /// <summary>
        /// Update avatar
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="file"></param>
        void UpdateAvatar(Int32 userId, String file);

        /// <summary>
        /// Get page users
        /// </summary>
        /// <param name="pageNumber">"Page number</param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        IEnumerable<UserInfo> GetPage(int pageNumber, int pageSize);

        /// <summary>
        /// Search by name and interests.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<UserInfo> SearchByNameInterest(String query);

        /// <summary>
        /// Search by name.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        IEnumerable<UserInfo> SearchByName(String query);
    }
}
