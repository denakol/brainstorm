﻿using BrainStorm.Repositories.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm.Services.Interfaces
{
    public interface IVoteService
    {
        /// <summary>
        /// Vote
        /// </summary>
        /// <param name="voteInfo"></param>
       void DoVote (VoteInfo voteInfo);
    }
}
