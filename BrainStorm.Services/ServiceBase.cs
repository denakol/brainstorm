﻿using BrainStorm.Framework.UnitOfWork.Interfaces;

namespace BrainStorm.Services
{
    public class ServiceBase
    {
        protected IUnitOfWorkFactory factory;

        public ServiceBase(IUnitOfWorkFactory factory)
        {
            this.factory = factory;
        }
    }
}