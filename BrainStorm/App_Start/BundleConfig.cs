﻿using System.Web.Optimization;

namespace BrainStorm.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.unobtrusive*",
                "~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/main").Include(
                "~/Scripts/main.js", 
                "~/Scripts/bootstrap.js"));
            bundles.Add(new ScriptBundle("~/bundles/popup").Include(
                "~/Scripts/popup.js"));
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(   
                "~/Scripts/jquery.signalR-1.1.3.js",
                "~/Scripts/select2.js",
                 "~/Scripts/select2_locale_ru.js", 
                "~/Scripts/angular.js",
                  "~/Scripts/AngularUi/select2.js",
                "~/Scripts/FileUpload/imageupload.js",
                "~/Scripts/angular-resource.js",
                "~/Scripts/ng-infinite-scroll.js",
                "~/Scripts/AngularUi/ui-bootstrap-custom-tpls-0.5.0.js",
                "~/Scripts/AngularUi/elasticTextArea.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/angular-app").Include(
                "~/Scripts/App/directives.js",
                "~/Scripts/App/templates.js",
                "~/Scripts/App/factory.js",
                 "~/Scripts/App/provider.js",
                "~/Scripts/App/resource.js", 
                "~/Scripts/App/service.js", 
                "~/Scripts/App/filters.js",
                "~/Scripts/App/app.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/css/bootstrap.css",
                "~/Content/css/style.css",
                "~/Content/css/select2-bootstrap.css",
                "~/Content/css/bootstrap-glyphicons.css",
                "~/Content/css/select2.css"));
        }
    }
}