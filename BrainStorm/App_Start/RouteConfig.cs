﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BrainStorm.Web.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(name: "groups",
              url: "groups",
              defaults: new { controller = "Home", action = "Home" }
              );

            routes.MapRoute(name: "group/members",
               url: "group/{entry}/members",
               defaults: new { controller = "Home", action = "Home" }
               );
            routes.MapRoute(name: "group/request",
                url: "group/{entry}/requests",
                defaults: new { controller = "Home", action = "Home" }
                );
            routes.MapRoute(name: "group",
                url: "group/{id}",
                defaults: new { controller = "Home", action = "Home" }
                );           
            routes.MapRoute(name: "users",
                url: "users",
                defaults: new { controller = "Home", action = "Home" }
                );

            routes.MapRoute(name: "myGroups",
                url: "myGroups",
                defaults: new { controller = "Home", action = "Home" }
                );
            routes.MapRoute(name: "myGroups/archive",
                url: "myGroups/archive",
                defaults: new { controller = "Home", action = "Home" }
                );
            routes.MapRoute(name: "user",
                url: "user/{entry}",
                defaults: new { controller = "Home", action = "Home" }
                );

            routes.MapRoute(name: "myMessage",
                url: "myMessage",
                defaults: new { controller = "Home", action = "Home" }
                );
            routes.MapRoute(name: "myPage",
                url: "myPage",
                defaults: new { controller = "Home", action = "Home" }
                );
            routes.MapRoute(name: "myDraft",
                url: "myDraft",
                defaults: new { controller = "Home", action = "Home" }
                );

            routes.MapRoute(name: "Template",
                url: "Template/{template}",
                defaults: new { controller = "Home", action = "Template" }
                );

            routes.MapRoute("UsersArchiveGroup",
                "Home/",
                 new { controller = "Home", action = "Home" }
                );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}