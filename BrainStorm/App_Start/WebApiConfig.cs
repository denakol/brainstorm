﻿using System.Web.Http;

namespace BrainStorm.Web.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
              name: "ApigetMyId",
              routeTemplate: "api/{controller}/MyId",
              defaults: new { action = "MyId" }
          );

            config.Routes.MapHttpRoute("Home",
                 "api/Group/{type}",
              new { controller = "Group", action = "UsersGroup" }
            );

            config.Routes.MapHttpRoute(
         name: "DefaultApi2",
         routeTemplate: "api/{controller}/Action/{action}/{id}",
          defaults: new { id = RouteParameter.Optional }
         );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );







        }
    }
}
