﻿using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Core;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Http.Controllers;


namespace BrainStorm.Web.Attribute
{
    /// <summary>
    /// User is admin
    /// </summary>
    public class Admin : AuthorizeAttribute
    {

        public override void OnAuthorization(HttpActionContext httpContext)
        {

            base.OnAuthorization(httpContext);
            Int32 id;
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                httpContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }
            if (!Int32.TryParse(HttpContext.Current.User.Identity.Name, out id))
            {
                httpContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            }
            UserSessionInfo user = ServiceLocator.Current.GetInstance<IUserService>().GetUserSessionInfo(id);
            if (!user.IsAdmin || user.IsBlock)
            {
                httpContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            }      


        }


    }
}