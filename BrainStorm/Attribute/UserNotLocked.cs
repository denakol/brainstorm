﻿using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Core;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace BrainStorm.Web.Attribute
{

    /// <summary>
    ///  User is not locked
    /// </summary>
    public class UserNotLocked : AuthorizeAttribute
    {

        public override void OnAuthorization(HttpActionContext httpContext)
        {

            base.OnAuthorization(httpContext);
            Int32 id;
            CacheItemRemovedCallback onRemove = null;
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                httpContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);

            }
            if (!Int32.TryParse(HttpContext.Current.User.Identity.Name, out id))
            {
                httpContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            }
            bool? isBlock = (bool?)HttpContext.Current.Cache[id.ToString()];
            UserSessionInfo user;
            if (!isBlock.HasValue)
            {
                user = ServiceLocator.Current.GetInstance<IUserService>().GetUserSessionInfo(id);
                isBlock = user.IsBlock;
                HttpContext.Current.Cache.Add(user.UserId.ToString(), user.IsBlock, null, DateTime.Now.AddSeconds(360), Cache.NoSlidingExpiration, CacheItemPriority.High, onRemove);
            }
            if (isBlock.Value)
            {
                httpContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            }


        }


    }
}