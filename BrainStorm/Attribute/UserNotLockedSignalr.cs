﻿using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using Microsoft.AspNet.SignalR;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Caching;

namespace BrainStorm.Web.Attribute
{
    public class UserNotLockedSignalr : AuthorizeAttribute
    {
        protected override bool UserAuthorized(System.Security.Principal.IPrincipal user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            CacheItemRemovedCallback onRemove = null;
            Int32 id;
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return false;
            }
            if (!Int32.TryParse(HttpContext.Current.User.Identity.Name, out id))
            {
                return false;
            }
            bool? isBlock = (bool?)HttpContext.Current.Cache[id.ToString()];
            UserSessionInfo userInfo;
            if (!isBlock.HasValue)
            {
                userInfo = ServiceLocator.Current.GetInstance<IUserService>().GetUserSessionInfo(id);
                isBlock = userInfo.IsBlock;
                HttpContext.Current.Cache.Add(userInfo.UserId.ToString(), userInfo.IsBlock, null, DateTime.Now.AddSeconds(360), Cache.NoSlidingExpiration, CacheItemPriority.High, onRemove);
            }
            if (isBlock.Value)
            {
                return false;
            }
            return true;
        }
    }
}