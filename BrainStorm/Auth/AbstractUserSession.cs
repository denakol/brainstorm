﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Security.Principal;
using System.Web.Security;
using BrainStorm.Model;


namespace BrainStorm.Web.Auth
{
    /// <summary>
    /// Abstract user session
    /// </summary>
    public abstract class AbstractUserSession : IUserSession
    {
        #region public properties

        /// <summary>
        /// GetByEmail authenticated user email
        /// </summary>
        public String UserId
        {
            get
            {
                return HttpContext.Current.User.Identity.Name;
            }
            protected set
            {
                FormsAuthentication.SetAuthCookie(value, _rememberMe);
            }
        }

        /// <summary>
        /// GetByEmail authenticated user
        /// </summary>
        public IPrincipal User
        {
            get
            {
                if (_user == null && UserId != String.Empty)
                {
                    _user = GetUser(UserId);
                }

                return _user;
            }
        }

        #endregion public properties

        #region public methods
        
        /// <summary>
        /// Login user
        /// </summary>
        public virtual void Login(IPrincipal user, Boolean rememberMe)
        {
            if (user == null) { throw new ArgumentNullException(); }
            _rememberMe = rememberMe;
             UserId= ((InfoForSession)user).UserEntityId.ToString();           
            HttpContext.Current.User = user;
            OnLogin(user);
        }

        /// <summary>
        /// Logout user
        /// </summary>
        public void Logout()
        {
            UserId= String.Empty;
            HttpContext.Current.User = null;
            _user = null;
            _rememberMe = false;
            FormsAuthentication.SignOut();
            OnLogout();
        }

        #endregion public methods

        #region protected methods

        /// <summary>
        /// GetByEmail authenticated on email
        /// </summary>
        protected abstract IPrincipal GetUser(String userId);

        protected virtual void OnLogin(IPrincipal user)
        {
        }

        protected virtual void OnLogout()
        {
        }

        #endregion protected methods

        #region private fields

        private IPrincipal _user;
        private Boolean _rememberMe = false;

        #endregion private fields
    }
}
