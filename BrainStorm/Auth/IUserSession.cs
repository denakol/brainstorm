﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Security.Principal;

namespace BrainStorm.Web.Auth
{
    /// <summary>
    /// User session
    /// </summary>
    public interface IUserSession
    {
        /// <summary>
        /// UserId of current session user (Empty string if anonymus)
        /// </summary>
        String UserId { get; }
        /// <summary>
        /// Current session user
        /// </summary>
        IPrincipal User { get; }
        
        /// <summary>
        /// Login user
        /// </summary>
        /// <param name="user"></param>
        void Login(IPrincipal user, Boolean rememberMe);

        /// <summary>
        /// Logout user
        /// </summary>
        void Logout();
    }
}
