﻿using BrainStorm.Framework.UnitOfWork.Interfaces;
using BrainStorm.Model;
using BrainStorm.Services.Implementation;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Mail;
using BrainStorm.Web.Mail.Interfaces;
using BrainStorm.Web.UnitOfWork;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;

namespace BrainStorm.Web
{
    public class Bootstrapper
    {
        public static void Init()
        {
            InitDB();
            InitDependencies();
        }

        public static void InitDB()
        {
            BrainStormInitWrapper.Init();
        }

        public static void InitDependencies()
        {
            var container = new UnityContainer();
            container.RegisterType<IUnitOfWorkFactory, BrainStormUnitOfWorkFactory>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IGroupService, GroupService>();
            container.RegisterType<IConfirmEmailService, ConfirmEmailService>();
            container.RegisterType<IUserGroupService, UserGroupService>();
            container.RegisterType<IIdeaService, IdeaService>();
            container.RegisterType<ICommentService, CommentService>();
            container.RegisterType<IMessageService, MessageService>();
            container.RegisterType<IVoteService, VoteService>();
            container.RegisterType<IDraftService, DraftService>();
            container.RegisterType<IMailSetting, MailSetting>();
            container.RegisterType<IMail, MailSystem>();

            ServiceLocator.SetLocatorProvider(() => new UnityServiceLocator(container));

            
        }
    }
}