﻿using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Auth;
using BrainStorm.Web.Core;
using BrainStorm.Web.Exceptions;
using BrainStorm.Web.Helpers;
using BrainStorm.Web.MapperWeb;
using BrainStorm.Web.Models;
using BrainStorm.Web.Models.Json;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace BrainStorm.Web.Controllers
{
    [Authorize]
    public class AccountApiController : AbstractApiController
    {
        [HttpPost]
        public HttpResponseMessage Post(Password password)
        {
            string hashPassword = Encryptor.MD5Hash(password.Old);
              
            if (Session.User.Password==hashPassword)
            {
                ServiceLocator.Current.GetInstance<IUserService>().UpdatePassword(Session.User.UserId, Encryptor.MD5Hash(password.New));
                var response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            {
                return Request.CreateResponse<ExceptionClient>(HttpStatusCode.Forbidden, new ExceptionClient { TypeError = TypeErrorClient.PasswordWrong });
            }
        }

       
    }
}