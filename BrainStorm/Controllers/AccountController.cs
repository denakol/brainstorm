﻿using System;
using System.Web.Mvc;
using BrainStorm.Framework.Exceptions;
using BrainStorm.Web.Core;
using BrainStorm.Web.Helpers;
using BrainStorm.Web.Models;
using BrainStorm.Web.Models.Account;

namespace BrainStorm.Web.Controllers
{
    public class AccountController : AbstractController
    {
        //
        // GET: /Account/

        public ActionResult Register()
        {
            var model = new RegisterModel();
            return PartialView("_PopupRegister", model);
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    AccountHelper.Register(model);
                    return PartialView("_SuccessModal", new SuccessRegisterModalModel());
                }
                catch (ExceptionAccount ex)
                {
                    ModelState[ex.ModelStateKey].Errors.Add(ex.NameError);
                   
                }
            }
            return PartialView("_PopupRegister", model);
        }

        public ActionResult RememberPassword()
        {
            var model = new EmailModel();
            return PartialView("_PopupRememberPassword", model);
        }

        [HttpPost]
        public ActionResult RememberPassword(EmailModel model)
        {
            if (ModelState.IsValid)
            {
                AccountHelper.RememberPassword(model.Email);
            }
            return PartialView("_PopupRememberPasswordOK", model);
        }

        public ActionResult ConfirmEmail(String email, String keyConfirm)
        {
            try
            {
                AccountHelper.ConfirmRegister(email, keyConfirm);
                return RedirectToAction("Index", "Home", new {IsFirstVisit = "true"});
            }
            catch (ExceptionAccount ex)
            {
                return View("Error", ex);
            }
        }


        [Authorize]
        public ActionResult LogOut()
        {
            Session.Logout();
            return RedirectToAction("Index", "Home");
        }
    }
}
