﻿using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Attribute;
using BrainStorm.Web.Core;
using BrainStorm.Web.MapperWeb;
using BrainStorm.Web.Models.Json;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace BrainStorm.Web.Controllers
{
      [UserNotLocked]
    public class CommentController : AbstractApiController
    {
        //
        // GET: /Comment/
        public IEnumerable<CommentView> Get(Int32 ideaId, Int32 groupId)
        {
            var isMember = ServiceLocator.Current.GetInstance<IUserGroupService>().IsUserGroupMember(groupId, Session.User);
            if (isMember || Session.User.IsAdmin)
            {
                var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(groupId);
                if(groupBlock)
                {
                    if (Session.User.IsAdmin)
                    {
                        return JsonMapper.MapCommentView(ServiceLocator.Current.GetInstance<ICommentService>().GetByIdeaId(ideaId));
                    }
                    return new List<CommentView>();
                }
                else
                {
                    var groupArchive = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupArchived(groupId);
                    var isAdminGroup = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(groupId, Session.User);
                    if (!isAdminGroup && groupArchive)
                    {
                        return new List<CommentView>();
                    }
                    return JsonMapper.MapCommentView(ServiceLocator.Current.GetInstance<ICommentService>().GetByIdeaId(ideaId));
                }                
             
            }
            else
            {
                return new List<CommentView>();
            }
        }
        public IEnumerable<CommentView> Get(Int32 groupId)
        {
            var isMember = ServiceLocator.Current.GetInstance<IUserGroupService>().IsUserGroupMember(groupId, Session.User);
            if (isMember || Session.User.IsAdmin)
            {
                var groupArchive = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupArchived(groupId);
                var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(groupId);
                var isAdminGroup = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(groupId, Session.User);
                if ((!isAdminGroup && groupArchive) || (groupBlock && !Session.User.IsAdmin))
                {
                    return null;
                }
                return JsonMapper.MapCommentView(ServiceLocator.Current.GetInstance<ICommentService>().GetByGroupId(groupId));
            }
            else
            {
                return new List<CommentView>();
            }
        }


        public HttpResponseMessage PostComment(CommentView comment)
        {
            var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupEnded(comment.groupId.Value);
            var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(comment.groupId.Value);
            if (groupEnded || groupBlock)
            {
                var response = Request.CreateResponse<CommentView>(HttpStatusCode.Forbidden, comment);
                return response;
            }

            var isMember = ServiceLocator.Current.GetInstance<IUserGroupService>().IsUserGroupMember(comment.groupId.Value, Session.User);
            if (isMember)
            {

                var commentInfo = JsonMapper.MapCommentInfo(comment);
                commentInfo = ServiceLocator.Current.GetInstance<ICommentService>().Add(commentInfo, Session.User);
                comment = JsonMapper.MapCommentView(commentInfo);
                var response = Request.CreateResponse<CommentView>(HttpStatusCode.Created, comment);
                return response;
            }
            else
            {
                var response = Request.CreateResponse<CommentView>(HttpStatusCode.Forbidden, comment);
                return response;
            }

        }

        [HttpPut]
        public HttpResponseMessage PutComment(CommentView comment)
        {
            if (comment.groupId.HasValue)
            {
                var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupEnded(comment.groupId.Value);
                var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(comment.groupId.Value);
                if (groupEnded || groupBlock)
                {
                    var response = Request.CreateResponse<CommentView>(HttpStatusCode.Forbidden, comment);
                    return response;
                }
            }
            else
            {
                var idea = ServiceLocator.Current.GetInstance<IIdeaService>().Get(comment.ideaId.Value, Session.User);
                var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupEnded(idea.GroupId);
                var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(idea.GroupId);
                if (groupEnded || groupBlock)
                {
                    var response = Request.CreateResponse<CommentView>(HttpStatusCode.Forbidden, comment);
                    return response;
                }
            }


            var isAuthor = ServiceLocator.Current.GetInstance<ICommentService>().IsUserAuthor(comment.commentId, Session.User);
            if (isAuthor)
            {
                var commentInfo = JsonMapper.MapCommentInfo(comment);
                commentInfo = ServiceLocator.Current.GetInstance<ICommentService>().Update(commentInfo);
                comment = JsonMapper.MapCommentView(commentInfo);
                var response = Request.CreateResponse<CommentView>(HttpStatusCode.OK, comment);
                return response;
            }
            {
                var response = Request.CreateResponse<CommentView>(HttpStatusCode.NotModified, comment);
                return response;
            }
        }

        public HttpResponseMessage DeleteComment(Int32 id)
        {

            var comment = ServiceLocator.Current.GetInstance<ICommentService>().Get(id);
            if (comment.IdeaId.HasValue)
            {
                var idea = ServiceLocator.Current.GetInstance<IIdeaService>().Get(comment.IdeaId.Value, Session.User);
                var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(idea.GroupId);
                var isAdminGroup = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(idea.GroupId, Session.User);
                if (!groupBlock&&isAdminGroup || Session.User.IsAdmin)
                {
                    ServiceLocator.Current.GetInstance<ICommentService>().Remove(id);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupEnded(idea.GroupId);
                if (groupEnded||groupBlock)
                {
                    var response = Request.CreateResponse(HttpStatusCode.Forbidden);
                    return response;
                }

            }
            if (comment.GroupId.HasValue)
            {
                var isAdminGroup = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(comment.GroupId.Value, Session.User);
                var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(comment.GroupId.Value);
                if (!groupBlock&&isAdminGroup || Session.User.IsAdmin)
                {
                    ServiceLocator.Current.GetInstance<ICommentService>().Remove(id);
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupEnded(comment.GroupId.Value);
                if (groupEnded||groupBlock)
                {
                    var response = Request.CreateResponse(HttpStatusCode.Forbidden);
                    return response;
                }
            }
            var isAuthor = ServiceLocator.Current.GetInstance<ICommentService>().IsUserAuthor(id, Session.User);
            if (isAuthor)
            {
                ServiceLocator.Current.GetInstance<ICommentService>().Remove(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateResponse(HttpStatusCode.Unauthorized);

        }


    }


}
