﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Remoting.Messaging;
using System.Web.Http;
using BrainStorm.Model;
using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Core;
using BrainStorm.Web.Models.Json;
using Microsoft.Practices.ServiceLocation;
using BrainStorm.Web.Helpers;
using BrainStorm.Web.Attribute;

namespace BrainStorm.Web.Controllers
{
       [UserNotLocked]
    public class DraftController : AbstractApiController
    {
        public IEnumerable<IdeaForMyPage> Get(int pageNumber, int pageSize, IdeaType type)
        {

            IEnumerable<IdeaForMyPage> result;
            switch (type)
            {
                case IdeaType.MyDraft:
                    {
                        result = ServiceLocator.Current.GetInstance<IIdeaService>()
                            .GetPageDraftByUserId(pageNumber, pageSize, Session.User.UserId);
                        break;
                    }
                default:
                    {
                        result = null;
                        break;
                    }
            }
            return result;
        }

        public HttpResponseMessage PostIdea(IdeaForMyPage idea)
        {

            idea = ServiceLocator.Current.GetInstance<IDraftService>().Add(idea, Session.User);
            var response = Request.CreateResponse(HttpStatusCode.Created, idea);
            return response;
        }
        public HttpResponseMessage PutIdea(IdeaForMyPage idea)
        {

            var ideaOld = ServiceLocator.Current.GetInstance<IDraftService>().Get(idea.IdeaId);
            if (ideaOld.GroupId.HasValue || ideaOld.AuthorId != Session.User.UserId)
            {
                var response = Request.CreateResponse(HttpStatusCode.Forbidden);
                return response;
            }
            if (idea.GroupId.HasValue)
            {
                var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupEnded(idea.GroupId.Value);
                if (groupEnded)
                {
                    var response = Request.CreateResponse(HttpStatusCode.Forbidden, idea);
                    return response;
                }
            }
            ServiceLocator.Current.GetInstance<IDraftService>().Update(idea);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
        public HttpResponseMessage DeleteIdea(Int32 id)
        {
            var idea = ServiceLocator.Current.GetInstance<IDraftService>().Get(id);
            if (idea.GroupId.HasValue||idea.AuthorId!=Session.User.UserId)
            {
                var response = Request.CreateResponse(HttpStatusCode.Forbidden);
                return response;
            }
            ServiceLocator.Current.GetInstance<IIdeaService>().Remove(id);
            return Request.CreateResponse(HttpStatusCode.OK);


        }
    }
}
