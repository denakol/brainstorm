﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Core;
using BrainStorm.Web.Models.Json;
using Microsoft.Practices.ServiceLocation;
using System.Net;
using System.Net.Http;
using System;
using BrainStorm.Web.MapperWeb;
using BrainStorm.Web.Attribute;
namespace BrainStorm.Web.Controllers
{
    [UserNotLocked]
    public class GroupController : AbstractApiController
    {
        //public IEnumerable<GroupsView> GetGroups()
        //{
        //    IEnumerable<GroupInfo> group = ServiceLocator.Current.GetInstance<IGroupService>().GetByEmail(Session.User);
        //    IEnumerable<GroupsView> groupView = group.Select(x => new GroupsView(x, Session.User, true)).AsEnumerable();
        //    return groupView;
        //}


        public IEnumerable<GroupsView> GetPageGroups(int pageNumber, int pageSize)
        {
            var group = ServiceLocator.Current.GetInstance<IGroupService>()
                .GetPage(pageNumber, pageSize, Session.User);
            List<GroupsView> groupView =
                group.Select(x => new GroupsView(x, Session.User, true)).AsEnumerable().ToList();
            return groupView;
        }




        [HttpGet]
        public IEnumerable<GroupsView> UsersGroup()
        {
            var group = new List<GroupInfo>();
            group = ServiceLocator.Current.GetInstance<IGroupService>().GetUsersGroup(Session.User).ToList();
            List<GroupsView> groupView =
                   group.Select(x => new GroupsView(x, Session.User, true)).AsEnumerable().ToList();
            return groupView;
        }

        [HttpGet]
        public IEnumerable<GroupsView> UsersGroup(string type)
        {
            var group = new List<GroupInfo>();
            if (type == "archive")
            {
                group = ServiceLocator.Current.GetInstance<IGroupService>().GetArchiveGroup(Session.User).ToList();
                List<GroupsView> groupView =
                        group.Select(x => new GroupsView(x, Session.User, true)).AsEnumerable().ToList();
                return groupView;
            }
            else
            {
                return null;
            }
        
        }



        public HttpResponseMessage PostGroup(GroupInfo group)
        {

            group = ServiceLocator.Current.GetInstance<IGroupService>().Add(group, Session.User);
            var response = Request.CreateResponse<GroupInfo>(HttpStatusCode.Created, group);
            return response;
        }

        public GroupsView GetGroup(Int32 id)
        {

            var group = ServiceLocator.Current.GetInstance<IGroupService>().GetGroupByUser(id, Session.User);
            if (group == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            var result= new GroupsView(group, Session.User, false);
            if(result.isBlock)
            {
                result.task = null;
                result.tags = null;
                
            }
            return result;

        }

        public IEnumerable<GroupsView> GetSearch(string search)
        {
            var results = ServiceLocator.Current.GetInstance<IGroupService>().Search(search, Session.User);
            var groupView = results.Select(x => new GroupsView(x, Session.User, true)).AsEnumerable();
            return groupView;
        }
        public HttpResponseMessage PutGroup(GroupsView group)
        {
            var isRealAdminGroup = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(group.groupId, Session.User);
            var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(group.groupId);
            if(groupBlock||group.isBlock)
            {
                if(Session.User.IsAdmin)
                {
                    ServiceLocator.Current.GetInstance<IGroupService>().SetBlock(JsonMapper.MapGroupInfo(group));
                    var response = Request.CreateResponse<GroupsView>(HttpStatusCode.OK, group);
                    return response;
                }
                else
                {
                    var response = Request.CreateResponse<GroupsView>(HttpStatusCode.NotModified, group);
                    return response;
                }
            }
            if (isRealAdminGroup)
            {
                ServiceLocator.Current.GetInstance<IGroupService>().Update(JsonMapper.MapGroupInfo(group));
                var response = Request.CreateResponse<GroupsView>(HttpStatusCode.OK, group);
                return response;
            }
            {
                var response = Request.CreateResponse<GroupsView>(HttpStatusCode.NotModified, group);
                return response;
            }
            
        }
    }
}