﻿using System;
using System.Web.Mvc;
using BrainStorm.Framework.Exceptions;
using BrainStorm.Web.Core;
using BrainStorm.Web.Models;
using BrainStorm.Web.Models.Account;
using System.IO;
using BrainStorm.Repositories.DTO;
using System.Security.Principal;

namespace BrainStorm.Web.Controllers
{
    public class HomeController : AbstractController
    {
        //
        // GET: /Home/

        public ActionResult Index(string IsFirstVisit)
        {
            if (Session.User != null)
            {
                var uri = String.Format("{0}/groups", Request.Url.AbsoluteUri);
                return Redirect(uri);
            }
            return View(new LogOnModel {IsMultiError = false});
        }

        [HttpPost]
        public ActionResult Index(LogOnModel model)
        {
            try
            {
                Session.Login(model);
                var uri = String.Format("{0}/groups", Request.Url.AbsoluteUri);
                return Redirect(uri);
            }
            catch (ExceptionAccount ex)
            {
                if (ex.MultiError)
                {
                    ModelState["Email"].Errors.Add(ex.NameError);
                    ModelState["Password"].Errors.Add(ex.NameError);
                    model.IsMultiError = true;
                }
                else
                {
                    ModelState[ex.ModelStateKey].Errors.Add(ex.NameError);
                    model.IsMultiError = false;
                }
            }
            return View(model);
        }

        [Authorize]
        public ActionResult Home()
        {
            if(Session.User.IsBlock)
            {
                return RedirectToAction("LogOut", "Account");
            }
            return View();
        }

        [Authorize]
        public ActionResult Template(String template)
        {

            switch (template.ToLower())
            {
                case "groups":
                    return PartialView("~/Views/Home/Partials/Groups.cshtml");
                case "users":
                    return PartialView("~/Views/Home/Partials/Users.cshtml");
                case "usersdetail":
                    return PartialView("~/Views/Home/Partials/UserDetail.cshtml");
                case "members":
                    return PartialView("~/Views/Home/Partials/Members.cshtml");
                case "group":
                    return PartialView("~/Views/Home/Partials/Group.cshtml");
                case "mymessage":
                    return PartialView("~/Views/Home/Partials/MyMessage.cshtml");
                case "inputmessages":
                    return PartialView("~/Views/Home/Partials/InputMessages.cshtml");
                case "outputmessages":
                    return PartialView("~/Views/Home/Partials/OutputMessages.cshtml");
                case "mypage":
                    return PartialView("~/Views/Home/Partials/MyPage/MyPage.cshtml");
                case "myideas":
                    return PartialView("~/Views/Home/Partials/MyPage/MyIdeas.cshtml");
                case "myideasarchive":
                    return PartialView("~/Views/Home/Partials/MyPage/MyIdeasArchive.cshtml");
                case "userpage":
                    return PartialView("~/Views/Home/Partials/UserPage.cshtml");
                case "mydraft":
                    return PartialView("~/Views/Home/Partials/MyDraft.cshtml");
                default:
                    throw new Exception("template not known");
            }

        }

        public ActionResult FirstVisitModal()
        {
            return PartialView("_SuccessModal",new SuccessConfirmEmail());
        }
    }
}
