﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.Practices.ServiceLocation;
using BrainStorm.Services.Interfaces;
using System.Collections.Concurrent;
using BrainStorm.Web.Models.Signalr;
using System.Threading.Tasks;
using BrainStorm.Repositories.DTO;
using BrainStorm.Web.MapperWeb;
using BrainStorm.Web.Models;
using Microsoft.AspNet.SignalR.Hubs;
using BrainStorm.Web.Attribute;

namespace BrainStorm.Web.Controllers.Hubs
{
    [UserNotLockedSignalr]
    public class MessageHub : Hub
    {
        private static readonly ConcurrentDictionary<Int32, User> Users
        = new ConcurrentDictionary<Int32, User>();


        public override Task OnConnected()
        {
            string userId = Context.User.Identity.Name;
            var userEntity = ServiceLocator.Current.GetInstance<IUserService>().GetUserSessionInfo(Convert.ToInt32(userId));
            if (!userEntity.IsBlock)
            {
                string connectionId = Context.ConnectionId;

                var user = Users.GetOrAdd(userEntity.UserEntityId, _ => new User
                {
                    FullName = userEntity.UserFullName,
                    ConnectionIds = new HashSet<string>()
                });

                lock (user.ConnectionIds)
                {

                    user.ConnectionIds.Add(connectionId);
                }

                return base.OnConnected();
            }
            return base.OnDisconnected();
        }


        public override Task OnDisconnected()
        {

            string userId = Context.User.Identity.Name;
            var userEntity = ServiceLocator.Current.GetInstance<IUserService>().GetUserSessionInfo(Convert.ToInt32(userId));
            string connectionId = Context.ConnectionId;
            User user;
            Users.TryGetValue(userEntity.UserEntityId, out user);

            if (user != null)
            {

                lock (user.ConnectionIds)
                {

                    user.ConnectionIds.RemoveWhere(cid => cid.Equals(connectionId));

                    if (!user.ConnectionIds.Any())
                    {

                        User removedUser;
                        Users.TryRemove(userEntity.UserEntityId, out removedUser);
                        ///оповестить что не онлайн;

                    }
                }
            }

            return base.OnDisconnected();
        }

        [HubMethodName("send")]
        public void Send(InputMessage message)
        {
            
            message.SenderId = Convert.ToInt32(Context.User.Identity.Name);
            var recipients = new List<RecipientInfo>();
            foreach(var user in message.Recipients)
            {
                var receiverBlock = ServiceLocator.Current.GetInstance<IUserService>().GetUserSessionInfo(user.RecipientId).IsBlock;
                if(!receiverBlock)
                {
                    recipients.Add(user);
                }
            }
            if (recipients.Count > 0)
            {
                message.Recipients = recipients;
                message = ServiceLocator.Current.GetInstance<IMessageService>().SendMessage(message);
                User sender = GetUser(Context.User.Identity.Name);

                User receiver;
                foreach (var recipient in message.Recipients)
                {
                    if (Users.TryGetValue(recipient.RecipientId, out receiver))
                    {

                        lock (receiver.ConnectionIds)
                        {
                            foreach (var cid in receiver.ConnectionIds)
                            {

                                Clients.Client(cid).received(message);
                            }
                        }


                    }
                }

                lock (sender.ConnectionIds)
                {
                    var messages = new List<OutputMessage>();
                    foreach (var recipientMessage in message.Recipients)
                    {
                        var outputMessage = new OutputMessage();
                        outputMessage.RecipientFullName = recipientMessage.RecipientFullName;
                        outputMessage.RecipientId = recipientMessage.RecipientId;
                        outputMessage.ImageUrl = recipientMessage.ImageUrl;
                        outputMessage.Created = message.Created;
                        outputMessage.Read = recipientMessage.Read;
                        outputMessage.Text = message.Text;
                        outputMessage.MessageEntityId = message.MessageEntityId;
                        messages.Add(outputMessage);
                    }
                    foreach (var cid in sender.ConnectionIds)
                    {
                        Clients.Client(cid).echo(messages);
                    }
                }
            }
            else
            {
                throw new Exception("Нет получателей");
            }

        }


        public void Read(Int32 messageId)
        {
            if (!ServiceLocator.Current.GetInstance<IMessageService>().Get(messageId).Read)
            {
                var userId =  Convert.ToInt32(Context.User.Identity.Name);
                ServiceLocator.Current.GetInstance<IMessageService>().Read(messageId, userId);
                var user = GetUser(Context.User.Identity.Name);
                foreach (var cid in user.ConnectionIds)
                {

                    Clients.Client(cid).read();

                }
                User sender;
                var message = ServiceLocator.Current.GetInstance<IMessageService>().Get(messageId);
                if (Users.TryGetValue(message.SenderId, out sender))
                {
                    foreach (var cid in sender.ConnectionIds)
                    {
                        Clients.Client(cid).recipientRead(messageId);
                    }
                }

            }
        }

        public void BlockUser(Int32 id)
        {

            User blockUser;
            if (Users.TryGetValue(id, out blockUser))
            {

                lock (blockUser.ConnectionIds)
                {
                    foreach (var cid in blockUser.ConnectionIds)
                    {

                        Clients.Client(cid).block();
                    }
                }


            }
        }



        private User GetUser(String userId)
        {

            User user;
            var userIdInt = Convert.ToInt32(userId);
            Users.TryGetValue(userIdInt, out user);
            return user;
        }


    }
}