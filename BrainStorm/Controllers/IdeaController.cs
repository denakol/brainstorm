﻿using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Attribute;
using BrainStorm.Web.Core;
using BrainStorm.Web.Helpers;
using BrainStorm.Web.MapperWeb;
using BrainStorm.Web.Models.Json;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BrainStorm.Web.Controllers
{

    [UserNotLocked]
    public class IdeaController : AbstractApiController
    {


        public HttpResponseMessage PostIdea(IdeaView idea)
        {

            var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(idea.groupId);
            var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupEnded(idea.groupId);
            if (groupEnded || groupBlock)
            {
                var response = Request.CreateResponse<IdeaView>(HttpStatusCode.Forbidden, idea);
                return response;
            }
            var isMember = ServiceLocator.Current.GetInstance<IUserGroupService>().IsUserGroupMember(idea.groupId, Session.User);
            if (isMember)
            {
                var ideaInfo = JsonMapper.MapIdeaInfo(idea);
                ideaInfo = ServiceLocator.Current.GetInstance<IIdeaService>().Add(ideaInfo, Session.User);
                idea = JsonMapper.MapIdeaView(ideaInfo);
                var response = Request.CreateResponse<IdeaView>(HttpStatusCode.Created, idea);
                return response;
            }
            else
            {
                var response = Request.CreateResponse<IdeaView>(HttpStatusCode.Forbidden, idea);
                return response;
            }

        }



        public IEnumerable<IdeaView> Get(Int32 groupId)
        {
            var isMember = ServiceLocator.Current.GetInstance<IUserGroupService>().IsUserGroupMember(groupId, Session.User);
            if (isMember||Session.User.IsAdmin)
            {
                var groupArchive = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupArchived(groupId);
                var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(groupId);
                var isAdminGroup = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(groupId, Session.User);
                if ((!isAdminGroup && groupArchive) || (groupBlock && !Session.User.IsAdmin))
                {
                    return null;
                }
                return JsonMapper.MapIdeaView(ServiceLocator.Current.GetInstance<IIdeaService>().GetByGroupId(groupId, Session.User));
            }
            return new List<IdeaView>();

        }

        public IEnumerable<IdeaForMyPage> Get(int pageNumber, int pageSize, IdeaType type)
        {

            IEnumerable<IdeaForMyPage> result;
            switch (type)
            {
                case IdeaType.MyIdea:
                    {
                        result = ServiceLocator.Current.GetInstance<IIdeaService>().GetPageByUserId(pageNumber, pageSize, Session.User.UserId);
                        break;
                    }
                case IdeaType.Archived:
                    {

                        result = ServiceLocator.Current.GetInstance<IIdeaService>().GetPageArchiveByUserId(pageNumber, pageSize, Session.User.UserId);
                        break;

                    }
                case IdeaType.MyDraft:
                    {
                        result = ServiceLocator.Current.GetInstance<IIdeaService>().GetPageDraftByUserId(pageNumber, pageSize, Session.User.UserId);
                        break;
                    }
                default:
                    {
                        result = null;
                        break;
                    }
            }
            return result;

        }

        public IdeaView Get(Int32 groupId, Int32 ideaId)
        {
            var isMember = ServiceLocator.Current.GetInstance<IUserGroupService>().IsUserGroupMember(groupId, Session.User);
            if (isMember || Session.User.IsAdmin)
            {
                var groupArchive = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupArchived(groupId);
                var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(groupId);
                var isAdminGroup = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(groupId, Session.User);
                if ((!isAdminGroup && groupArchive) || (groupBlock && !Session.User.IsAdmin))
                {
                    return null;
                }
                var ideaInfo = ServiceLocator.Current.GetInstance<IIdeaService>().Get(ideaId, Session.User);
                var idea = JsonMapper.MapIdeaView(ideaInfo);
                return idea;
            }
            return null;
        }


        public HttpResponseMessage PutIdea(IdeaView idea)
        {
            var groupId = ServiceLocator.Current.GetInstance<IIdeaService>().Get(idea.ideaId, Session.User).GroupId;
            var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupEnded(groupId);
            var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(groupId);
            if (groupEnded || groupBlock)
            {
                var response = Request.CreateResponse<IdeaView>(HttpStatusCode.Forbidden, idea);
                return response;
            }
            var isAuthor = ServiceLocator.Current.GetInstance<IIdeaService>().IsUserAuthor(idea.ideaId, Session.User);
            if (isAuthor)
            {
                var ideaInfo = JsonMapper.MapIdeaInfo(idea);
                ideaInfo = ServiceLocator.Current.GetInstance<IIdeaService>().Update(ideaInfo);
                idea = JsonMapper.MapIdeaView(ideaInfo);
                var response = Request.CreateResponse<IdeaView>(HttpStatusCode.OK, idea);
                return response;
            }

          return Request.CreateResponse<IdeaView>(HttpStatusCode.NotModified, idea);

        }

        public HttpResponseMessage DeleteIdea(Int32 id)
        {
            var idea = ServiceLocator.Current.GetInstance<IIdeaService>().Get(id, Session.User);
            var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(idea.GroupId);
            var isAdminGroup = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(idea.GroupId, Session.User);
            if (Session.User.IsAdmin || !groupBlock && isAdminGroup)
            {
                ServiceLocator.Current.GetInstance<IIdeaService>().Remove(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupEnded(idea.GroupId);
            if (groupEnded)
            {
                var response = Request.CreateResponse(HttpStatusCode.Forbidden);
                return response;
            }
            var isAuthor = ServiceLocator.Current.GetInstance<IIdeaService>().IsUserAuthor(id, Session.User);
            if (isAuthor)
            {
                ServiceLocator.Current.GetInstance<IIdeaService>().Remove(id);
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            return Request.CreateResponse(HttpStatusCode.Unauthorized);

        }
    }
}
