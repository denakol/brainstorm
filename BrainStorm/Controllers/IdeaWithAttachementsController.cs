﻿using System.Collections.ObjectModel;
using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Core;
using BrainStorm.Web.Helpers;
using BrainStorm.Web.MapperWeb;
using BrainStorm.Web.Models.Json;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using BrainStorm.Web.Attribute;

namespace BrainStorm.Web.Controllers
{
    [UserNotLocked]
    public class IdeaWithAttachementsController : AbstractApiController
    {
          
        [HttpPost]
        public async Task<HttpResponseMessage> Attachments()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var root = HttpContext.Current.Server.MapPath("~/Content/Attachments");
            Directory.CreateDirectory(root);
            var provider = new MultipartFormDataStreamProvider(root);
            var result = await Request.Content.ReadAsMultipartAsync(provider);

            if (result.FormData["model"] == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            var serializer = new JavaScriptSerializer();
            var model = result.FormData["model"];
            var draft = serializer.Deserialize<IdeaForMyPage>(model);
            if (!draft.GroupId.HasValue)
            {
                var list = SaveFile(result.FileData);
                draft.AttachedImages = list;
                draft = ServiceLocator.Current.GetInstance<IDraftService>().Add(draft, Session.User);
                var response = Request.CreateResponse(HttpStatusCode.Created, draft);
                return response;
            }

            var idea = serializer.Deserialize<IdeaView>(model);

           
            var isMember = ServiceLocator.Current.GetInstance<IUserGroupService>().IsUserGroupMember(idea.groupId, Session.User);
            if (isMember)
            {
                var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(idea.groupId);
                var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupEnded(idea.groupId);
                if (groupEnded || groupBlock)
                {
                    var response = Request.CreateResponse<IdeaView>(HttpStatusCode.Forbidden, idea);
                    return response;
                }
                var list = SaveFile(result.FileData);
                var ideaInfo = JsonMapper.MapIdeaInfo(idea);
                ideaInfo.AttachedImages = list;
                ideaInfo = ServiceLocator.Current.GetInstance<IIdeaService>().Add(ideaInfo, Session.User);

                idea = JsonMapper.MapIdeaView(ideaInfo);

                return Request.CreateResponse<IdeaView>(HttpStatusCode.Created, idea);
            }
            else
            {

                var response = Request.CreateResponse<IdeaView>(HttpStatusCode.Forbidden, idea);
                return response;
            }
        }


          private IEnumerable<AttachedImageInfo> SaveFile(IEnumerable<MultipartFileData> files)
          {
              var list = new List<AttachedImageInfo>();
              Parallel.ForEach(files, file =>
              {
                  var fileExtention = file.Headers.ContentType.MediaType.Split(new Char[] { '/' })[1];
                  var fileInfo = new FileInfo(file.LocalFileName);
                  var fileName = fileInfo.Name + "." + fileExtention;
                  ImageHelper.Resize(file.LocalFileName, file.LocalFileName + "." + fileExtention, 1024);
                  File.Move(file.LocalFileName + "." + fileExtention, fileInfo.DirectoryName + "//" + "big-" + fileName);
                  ImageHelper.Resize(file.LocalFileName, file.LocalFileName + "-." + fileExtention, 256);
                  File.Move(file.LocalFileName + "-." + fileExtention, fileInfo.DirectoryName + "//" + "small-" + fileName);
                  list.Add(new AttachedImageInfo() { Url = fileInfo.Name + "." + fileExtention });
              });
              return list;
          }

    }
}
