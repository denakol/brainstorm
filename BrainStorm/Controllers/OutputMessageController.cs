﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Remoting.Messaging;
using System.Web.Http;
using BrainStorm.Model;
using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Core;
using BrainStorm.Web.Models.Json;
using Microsoft.Practices.ServiceLocation;
using BrainStorm.Web.Helpers;
using BrainStorm.Web.Attribute;

namespace BrainStorm.Web.Controllers
{
    [UserNotLocked]
    public class outputMessageController : AbstractApiController
    {
        public IEnumerable<OutputMessage> GetPageGroups(int pageNumber, int pageSize)
        {

                var message = ServiceLocator.Current.GetInstance<IMessageService>()
               .GetOutput(pageNumber, pageSize, Session.User.UserId);
                return message;

        }


    }
}