﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Core;
using BrainStorm.Web.Exceptions;
using BrainStorm.Web.MapperWeb;
using BrainStorm.Web.Models.Json;
using Microsoft.Practices.ServiceLocation;
using BrainStorm.Web.Helpers;
using BrainStorm.Web.Attribute;
using Microsoft.AspNet.SignalR;
using BrainStorm.Web.Controllers.Hubs;

namespace BrainStorm.Web.Controllers
{
    [Admin]
    public class UserAdminController : AbstractApiController
    {

        /// <summary>
        /// Block/unblock/assign  user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage PostAdminAction(UserInfo user)
        {
            var isCurrentUser = user.Id == Session.User.UserId;
            if (isCurrentUser)
            {
                var response = Request.CreateResponse(HttpStatusCode.Forbidden, user);
                return response;
               
            }
            var realyUser = ServiceLocator.Current.GetInstance<IUserService>().GetUserInfo(user.Id);
            if(!(realyUser.IsAdmin||(user.IsAdmin&&user.IsBlock)))
            {
                ServiceLocator.Current.GetInstance<IUserService>().UpdateUserAdmin(user);

                ///if there was a change of the field isBlock
                if(realyUser.IsAdmin == user.IsAdmin)
                {
                    ///the value stored in the cache
                    var InCache = HttpContext.Current.Cache[user.Id.ToString()];
                    if (InCache!=null)
                    {
                        HttpContext.Current.Cache.Remove(user.Id.ToString());
                    }
                    if(user.IsBlock)
                    {
                        MailHelper.SendBlockUser(realyUser.Email);
                    }
                }
                var response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.Forbidden, user);
                return response;
            }
              
        }

    }
}
