﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Core;
using BrainStorm.Web.Exceptions;
using BrainStorm.Web.MapperWeb;
using BrainStorm.Web.Models.Json;
using Microsoft.Practices.ServiceLocation;
using BrainStorm.Web.Helpers;

namespace BrainStorm.Web.Controllers
{
    [Authorize]
    public class UserController : AbstractApiController
    {
        public IEnumerable<UserView> GetUsers()
        {
            IEnumerable<UserInfo> user = ServiceLocator.Current.GetInstance<IUserService>().Get();
            IEnumerable<UserView> userView = user.Select(x => new UserView(x, true)).AsEnumerable();
            return userView;
        }

        public IEnumerable<UserView> GetPageUsers(int pageNumber, int pageSize)
        {
            IEnumerable<UserInfo> users = ServiceLocator.Current.GetInstance<IUserService>()
                .GetPage(pageNumber, pageSize);
            IEnumerable<UserView> userViews = users.Select(x => new UserView(x, true)).AsEnumerable();
            return userViews;
        }

        public IEnumerable<UserView> GetSearch(string search, UserTypeSearch type)
        {
            if (search != null)
            {
                switch(type)
                {
                    case UserTypeSearch.NameAndInterest:
                        {
                            IEnumerable<UserInfo> results = ServiceLocator.Current.GetInstance<IUserService>().SearchByNameInterest(search);
                            IEnumerable<UserView> userView = results.Select(x => new UserView(x, true)).AsEnumerable();
                            return userView;
                        }
                    case UserTypeSearch.Name:
                        {
                            IEnumerable<UserInfo> results = ServiceLocator.Current.GetInstance<IUserService>().SearchByName(search);
                            IEnumerable<UserView> userView = results.Select(x => new UserView(x, true)).AsEnumerable();
                            return userView;
                        }
                    case UserTypeSearch.NameNotBlockUser:
                        {
                            IEnumerable<UserInfo> results = ServiceLocator.Current.GetInstance<IUserService>().SearchByName(search);
                            IEnumerable<UserView> userView = results.Where(x=>!x.IsBlock).Select(x => new UserView(x, true)).AsEnumerable();
                            return userView;
                        }

                }             
            }
            return null;
        }


        public UserForMyPage GetUser(int id)
        {
            var user = ServiceLocator.Current.GetInstance<IUserService>().GetUserForMyPage(id);
            if (user == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            if(user.IsBlock)
            {
                user.Interest = null;
                user.Groups = null;
                user.About = null;
            }
            return user;

        }

        [HttpPost]
        public async Task<HttpResponseMessage> Avatar()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            MultipartFileStreamProvider result;

            var root = HttpContext.Current.Server.MapPath("~/Content/Avatar");
            Directory.CreateDirectory(root);
            var provider = new MultipartFormDataStreamProvider(root);
            result = await Request.Content.ReadAsMultipartAsync(provider);

            var files = result.FileData;
            var file = files[0];
            var fileExtention = file.Headers.ContentType.MediaType.Split(new Char[] { '/' })[1];
            var fileInfo = new FileInfo(file.LocalFileName);
            var fileName = fileInfo.Name + "." + fileExtention;

            
            ImageHelper.Resize(file.LocalFileName, file.LocalFileName + "." + fileExtention, 200);
            File.Move(file.LocalFileName + "." + fileExtention, fileInfo.DirectoryName + "//" + fileName);
            ServiceLocator.Current.GetInstance<IUserService>().UpdateAvatar(Session.User.UserEntityId, fileName);
            var response = Request.CreateResponse(HttpStatusCode.Created, new { name = fileName });
            return response;


        }

        [HttpPut]
        public HttpResponseMessage Update(UserForMyPage user)
        {
            var isCurrentUser = user.Id == Session.User.UserId;
            if (isCurrentUser)
            {
                ServiceLocator.Current.GetInstance<IUserService>().UpdateInfo(user);
                var response = Request.CreateResponse(HttpStatusCode.OK);
                return response;
            }
            {
                var response = Request.CreateResponse<UserForMyPage>(HttpStatusCode.Forbidden, user);
                return response;
            }
        }

        [HttpGet]
        public MyInfo MyId()
        {
            return new MyInfo
            {
                id = Session.User.UserId,
                isUserAdminSite = Session.User.IsAdmin,
                fullName = Session.User.UserFullName,
                isBlock = Session.User.IsBlock
            };
        }
    }
}