﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Core;
using BrainStorm.Web.Models.Json;
using Microsoft.Practices.ServiceLocation;
using BrainStorm.Model;
using BrainStorm.Web.Exceptions;
using BrainStorm.Web.Attribute;

namespace BrainStorm.Controllers
{
    [UserNotLocked]
    public class UserGroupController : AbstractApiController
    {


        public IEnumerable<UserView> GetMembers(Int32 groupId)
        {
            IEnumerable<UserInfo> user = ServiceLocator.Current.GetInstance<IUserGroupService>().GetMembers(groupId);
            IEnumerable<UserView> userView = user.Select(x => new UserView(x, true)).AsEnumerable();
            return userView;
        }


        [HttpGet]
        public CountInvites getCountInvites()
        {
            return new CountInvites()
            {
                count = ServiceLocator.Current.GetInstance<IUserGroupService>().GetCountInvites(Session.User.UserId)
            };
        }

        [HttpGet]
        public IEnumerable<UserView> GetRequests(Int32 id)
        {
            IEnumerable<UserInfo> user = ServiceLocator.Current.GetInstance<IUserGroupService>().GetRequests(id);
            IEnumerable<UserView> userView = user.Select(x => new UserView(x, true)).AsEnumerable();
            return userView;
        }

        //public UserGroup GetUserGroup(int id)
        //{
        //    UserGroup usergroup = db.UserGroups.Find(id);
        //    if (usergroup == null)
        //    {
        //        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
        //    }

        //    return usergroup;
        //}

        [HttpPost]
        public HttpResponseMessage PostUserGroup(UserGroup userGroup)
        {
            if (ModelState.IsValid)
            {
                switch (userGroup.state)
                {
                    case StateUserGroup.Send:
                        {
                            var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupEnded(userGroup.groupId);
                            var groupArchive = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupArchived(userGroup.groupId);
                            if (groupEnded || groupArchive)
                            {
                                return Request.CreateResponse(HttpStatusCode.Forbidden);
                            }
                            ServiceLocator.Current.GetInstance<IUserGroupService>()
                                .SendRequestToJoin(userGroup.groupId, Session.User.UserId);
                            break;
                        }
                    case StateUserGroup.Invite:
                        {
                            var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupEnded(userGroup.groupId);
                            if (groupEnded)
                            {
                                return Request.CreateResponse(HttpStatusCode.Forbidden);
                            }
                            var isMemberCurrentUser = ServiceLocator.Current.GetInstance<IUserGroupService>().IsUserGroupMember(userGroup.groupId, Session.User);
                            var user = ServiceLocator.Current.GetInstance<IUserGroupService>().GetMemberByGroupIdUserId(userGroup.groupId, userGroup.userId);
                            var InvitedUserHasNoRelationshipWithGroup = user == null;
                            if (isMemberCurrentUser && InvitedUserHasNoRelationshipWithGroup)
                            {
                                ServiceLocator.Current.GetInstance<IUserGroupService>()
                                .SendInvite(userGroup.groupId, userGroup.userId);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.Forbidden);
                            }
                            break;

                        }
                    case StateUserGroup.Adoptet:
                        {
                            var state = ServiceLocator.Current.GetInstance<IUserGroupService>().GetStateByGroupIdUserId(userGroup.groupId, userGroup.userId);
                            if (state == StateUserGroup.Invite)
                            {
                                ServiceLocator.Current.GetInstance<IUserGroupService>().ConfirmInvite(userGroup.groupId, userGroup.userId);
                                break;
                            }
                            if (state == StateUserGroup.Send || state == StateUserGroup.Block)
                            {
                                var isRealAdminGroup = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(userGroup.groupId, Session.User);
                                if (isRealAdminGroup)
                                {
                                    ServiceLocator.Current.GetInstance<IUserGroupService>().ConfirmInvite(userGroup.groupId, userGroup.userId);
                                }
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.Forbidden);
                            }
                            break;
                        }
                    case StateUserGroup.NotSend:
                        {
                            var onlyGroupAdministrator = ServiceLocator.Current.GetInstance<IGroupService>().OnlyGroupAdministrator(userGroup.groupId);
                            var isRealAdminGroup = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(userGroup.groupId, Session.User);
                            if (onlyGroupAdministrator && isRealAdminGroup)
                            {
                                return Request.CreateResponse<ExceptionClient>(HttpStatusCode.Forbidden, new ExceptionClient { TypeError = TypeErrorClient.OnlyAdmin});
                            }
                           
                            if (isRealAdminGroup || Session.User.UserId == userGroup.userId)
                            {
                                ServiceLocator.Current.GetInstance<IUserGroupService>().DeleteInvite(userGroup.groupId, userGroup.userId);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.Forbidden);
                            }
                            break;
                        }

                    case StateUserGroup.Admin:
                        {
                            var isRealAdminGroup = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(userGroup.groupId, Session.User);
                            if (isRealAdminGroup)
                            {
                                ServiceLocator.Current.GetInstance<IUserGroupService>().AssignAdmin(userGroup.groupId, userGroup.userId);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.Forbidden);
                            }
                            break;
                        }
                    case StateUserGroup.Block:
                        {
                            var isRealAdminGroup = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(userGroup.groupId, Session.User);
                            var blockUserIsAdmin = ServiceLocator.Current.GetInstance<IGroupService>().IsUserAdminGroup(userGroup.groupId, userGroup.userId);
                            if (isRealAdminGroup && !blockUserIsAdmin)
                            {

                                ServiceLocator.Current.GetInstance<IUserGroupService>().Block(userGroup.groupId, userGroup.userId);
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.Forbidden);
                            }
                            break;
                        }
                }
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, userGroup);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = userGroup.groupId }));
                return response;
            }
            return Request.CreateResponse(HttpStatusCode.Forbidden);
        }


    }
}