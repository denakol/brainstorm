﻿using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Attribute;
using BrainStorm.Web.Core;
using BrainStorm.Web.MapperWeb;
using BrainStorm.Web.Models.Json;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BrainStorm.Web.Controllers
{
    [UserNotLocked]
    public class VoteController : AbstractApiController
    {
        [HttpPost]
        public HttpResponseMessage Vote(VoteView vote)
        {
            if (vote.authorId == Session.User.UserId)
            {
                var idea = ServiceLocator.Current.GetInstance<IIdeaService>().Get(vote.ideaId, Session.User);
                var groupEnded = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupArchived(idea.GroupId);
                var groupBlock = ServiceLocator.Current.GetInstance<IGroupService>().IsGroupBlock(idea.GroupId);
                if(groupBlock||groupEnded)
                {
                    return Request.CreateResponse<VoteView>(HttpStatusCode.Forbidden, vote);
                }
                ServiceLocator.Current.GetInstance<IVoteService>().DoVote(JsonMapper.MapVoteInfo(vote));
                var response = Request.CreateResponse<VoteView>(HttpStatusCode.Created, vote);
                return response;
            }
            var res = Request.CreateResponse<VoteView>(HttpStatusCode.NotModified, vote);
            return res;
        }
    }
}
