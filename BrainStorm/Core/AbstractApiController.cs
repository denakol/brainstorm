﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace   BrainStorm.Web.Core
{
    public class AbstractApiController : ApiController
    {
        #region public properties

        /// <summary>
        /// GetByEmail session
        /// </summary>
        public new UserSession Session
        {
            get
            {
                if (_sesion == null)
                {
                    _sesion = new UserSession();
                }
                return _sesion;
            }
        }

        #endregion public properties

        #region private properites

        private UserSession _sesion;

        #endregion private properites

    }
}
