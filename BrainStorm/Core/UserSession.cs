﻿using System;
using System.Security.Principal;
using BrainStorm.Framework.Exceptions;
using BrainStorm.Model.Entity;
using BrainStorm.Repositories.DTO;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Models.Account;
using Microsoft.Practices.ServiceLocation;
using BrainStorm.Web.Auth;

namespace BrainStorm.Web.Core
{
    public class UserSession : AbstractUserSession
    {

        private const String NAME_ERROR_USER_BLOCK = "Пользователь заблокирован администратором ";
        private const String NAME_ERROR_EMAIL_NNOT_CONFIRM="Введенный email неактивен";
        private const String NAME_ERROR_EMAIL_PASSWORD_NOT_VALID = "Неправильная пара email/пароль";
        #region public properties

        /// <summary>
        ///     Session user
        /// </summary>
        public new UserSessionInfo User
        {
            get { return (UserSessionInfo) base.User; }
        }

        #endregion public properties

        public void Login(LogOnModel model)
        {
            string hashPassword = Encryptor.MD5Hash(model.Password);
            UserSessionInfo user = ServiceLocator.Current.GetInstance<IUserService>()
                .GetUserSessionInfo(model.Email, hashPassword);
            if (user != null)
            {
                if (user.IsBlock)
                {
                    throw new ExceptionAccount
                    {
                        MultiError = true,
                        NameError = NAME_ERROR_USER_BLOCK
                    };
                } 
                Login(user, model.RememberMe);
            }
            else
            {
                ConfirmEmailEntity userNonConfirm =
                    ServiceLocator.Current.GetInstance<IConfirmEmailService>().Get(model.Email);
                if (userNonConfirm != null)
                {
                    throw new ExceptionAccount 
                    {
                        ModelStateKey = "Email", 
                        NameError = NAME_ERROR_EMAIL_NNOT_CONFIRM
                    };
                }
                throw new ExceptionAccount
                {
                    MultiError = true,
                    NameError = NAME_ERROR_EMAIL_PASSWORD_NOT_VALID
                };
            }
        }

        #region protected methods

        /// <summary>
        ///     Method is run after login
        /// </summary>
        protected override void OnLogin(IPrincipal user)
        {
            base.OnLogin(user);
        }

        /// <summary>
        ///     Method is run after logout
        /// </summary>
        protected override void OnLogout()
        {
        }

        /// <summary>
        ///     GetByEmail authenticated user
        /// </summary>
        protected override IPrincipal GetUser(String userId)
        {
            Int32 userIdInt;
            if (Int32.TryParse(userId, out userIdInt))
            {
                return ServiceLocator.Current.GetInstance<IUserService>().GetUserSessionInfo(userIdInt);
            }
            return null;

        }

        #endregion protected methods
    }
}