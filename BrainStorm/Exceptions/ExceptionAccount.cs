﻿using System;

namespace BrainStorm.Framework.Exceptions
{
    /// <summary>
    /// Exception for register and log on
    /// </summary>
    public class ExceptionAccount : Exception
    {
        /// <summary>
        /// Name error
        /// </summary>
        public String NameError { get; set; }

        /// <summary>
        /// Field in which the error
        /// </summary>
        public String ModelStateKey { get; set; }

        /// <summary>
        /// Mmultiple error
        /// </summary>
        public Boolean MultiError { get; set; }
    }
}