﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrainStorm.Web.Exceptions
{
    public class ExceptionClient
    {
        [JsonProperty("type")]
        public TypeErrorClient TypeError { get; set; }
    }
}