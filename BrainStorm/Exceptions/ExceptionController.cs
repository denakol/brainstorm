﻿using System;

namespace BrainStorm.Framework.Exceptions
{
    public class ExceptionController : Exception
    {
        public String NameError { get; set; }
        public String ModelStateKey { get; set; }
        public Boolean MultiError { get; set; }
    }
}