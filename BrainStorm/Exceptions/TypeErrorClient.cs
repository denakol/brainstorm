﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrainStorm.Web.Exceptions
{
    public  enum TypeErrorClient
    {
        OnlyAdmin = 1,
        PasswordWrong = 2
    }
}