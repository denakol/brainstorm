﻿using System;
using BrainStorm.Framework.Exceptions;
using BrainStorm.Model.Entity;
using BrainStorm.Services.Interfaces;
using BrainStorm.Web.Models.Account;
using Microsoft.Practices.ServiceLocation;
using BrainStorm.Web.Auth;

namespace BrainStorm.Web.Helpers
{
    public static class AccountHelper
    {
        public const String ERROR_USER_ALREDY_REGISTER =  "Пользователь уже есть на сайте";
        public const String ERROR_EMAIL_NOT_CONFIRM = "Необходимо потвердить это email, если письмо вам не пришло, нажмите кнопку выслать повторно письмо";
        public const String ERROR_EMAIL_CONFIRM_KEY_NOT_VALID = "Либо email потвержден уже, либо неправильный ключ потверждения";
        public const String DEFAULT_AVATAR_NAME = "default_avatar.jpg";
        public static void Register(RegisterModel registerModel)
        {
            
            var confirmEmail = new ConfirmEmailEntity
            {
                FirstName = registerModel.FirstName.Trim(),
                LastName = registerModel.LastName.Trim(),
                Email = registerModel.Email.Trim().ToLower(),
                Password = Encryptor.MD5Hash(registerModel.Password),
                ConfirmKey = Encryptor.MD5Hash(registerModel.Password)
            };

            if (ServiceLocator.Current.GetInstance<IConfirmEmailService>().Get(registerModel.Email) == null)
            {
                if (ServiceLocator.Current.GetInstance<IUserService>().Get(registerModel.Email) != null)
                {
                    throw new ExceptionAccount
                    {
                        NameError = ERROR_USER_ALREDY_REGISTER,
                        ModelStateKey = "Email"
                    };
                }
            }
            else
            {
                throw new ExceptionAccount
                {
                    NameError =
                       ERROR_EMAIL_NOT_CONFIRM,
                    ModelStateKey = "Email"
                };
            }
            ServiceLocator.Current.GetInstance<IConfirmEmailService>().Add(confirmEmail);

            MailHelper.SendConfirm(confirmEmail.Email, confirmEmail.ConfirmKey);
          
        }

        public static UserEntity ConfirmRegister(String email, String confirmKey)
        {
            ConfirmEmailEntity userConfirm = ServiceLocator.Current.GetInstance<IConfirmEmailService>()
                .Get(email, confirmKey);
            if (userConfirm == null)
            {
                throw new ExceptionAccount
                {
                    NameError = ERROR_EMAIL_CONFIRM_KEY_NOT_VALID
                };
            }
            userConfirm.ImageUrl = DEFAULT_AVATAR_NAME;
            UserEntity user = ServiceLocator.Current.GetInstance<IUserService>().ConfirmRegister(userConfirm);
            return user;
        }

        public static void RememberPassword(String email)
        {
            string newPassword = GetPassword();
            string newEncryptPassword = Encryptor.MD5Hash(newPassword);
            UserEntity user = ServiceLocator.Current.GetInstance<IUserService>()
                .UpdatePassword(email, newEncryptPassword);
            if (user != null)
            {
                MailHelper.SendNewPassword(email, newPassword);
            }
        }

        private static String GetPassword(int x = 6)
        {
            string pass = "";
            var r = new Random();
            while (pass.Length < x)
            {
                var c = (char) r.Next(33, 125);
                if (Char.IsLetterOrDigit(c))
                    pass += c;
            }
            return pass;
        }
    }
}