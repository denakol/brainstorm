﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrainStorm.Web.Helpers
{
    public enum IdeaType
    {
        MyIdea=1,
        Archived = 2,
        MyDraft = 3
    }
}