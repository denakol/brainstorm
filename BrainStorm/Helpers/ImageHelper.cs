﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace BrainStorm.Web.Helpers
{
    public class ImageHelper
    {
        /// <summary>
        /// Opens the file, changes the size of the original image in a square with the party, and saves the output image with the name "resizeFilename"
        /// </summary>
        /// <param name="originalFilename">Original file</param>
        /// <param name="resizeFilename">The name of the saved image</param>
        /// <param name="sideSquare"></param>
        public static void Resize(string originalFilename, string resizeFilename, int sideSquare)
        {
            while (!ImageHelper.RetryUntilFileReadable(originalFilename, 10, 50)) ;
            Image imgOriginal = Image.FromFile(originalFilename);
            Image imgResize = Resize(imgOriginal, sideSquare);
            imgResize.Save(resizeFilename);
            imgResize.Dispose();
            imgOriginal.Dispose();

        }

        /// <summary>
        /// Resize the image in a square with a given side
        /// </summary>
        /// <param name="originalFilename"></param>
        /// <param name="resizeFilename"></param>
        /// <param name="sideSquare">Side of the square</param>
        private static Image Resize(Image img, int sideSquare)
        {
            int logoSize = sideSquare;
            float sourceWidth = img.Width;
            float sourceHeight = img.Height;
            float destHeight = 0;
            float destWidth = 0;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;
            if (sourceWidth > logoSize / 2)
            {
                int h = logoSize / 2;
                destWidth = h;

                destHeight = (float)(sourceHeight * h / sourceWidth);
            }
            else
            {
                int h = logoSize / 2;
                destHeight = h;
                destWidth = (float)(sourceWidth * h / sourceHeight);
            }
            Bitmap bmPhoto = new Bitmap((int)destWidth, (int)destHeight,
                                        PixelFormat.Format32bppPArgb);
            bmPhoto.SetResolution(img.HorizontalResolution, img.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
            grPhoto.DrawImage(img,
                new Rectangle(destX, destY, (int)destWidth, (int)destHeight),
                new Rectangle(sourceX, sourceY, (int)sourceWidth, (int)sourceHeight),
                GraphicsUnit.Pixel);
            grPhoto.Dispose();
            return bmPhoto;
        }


        private static bool RetryUntilFileReadable(string filePath, int maxRetries, int sleepPerRetryInMilliseconds)
        {
            var retries = maxRetries;
            var fileReadable = false;
            while (!fileReadable && retries > 0)
            {
                try
                {
                    using (FileStream fileStream = File.OpenRead(filePath))
                    {
                        fileReadable = true;
                    }
                }
                catch (Exception)
                {
                    retries--;
                    Thread.Sleep(sleepPerRetryInMilliseconds);
                }
            }
            return fileReadable;
        }
    }
}