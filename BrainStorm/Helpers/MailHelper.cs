﻿using System;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;

using Microsoft.Practices.ServiceLocation;
using System.Web;
using BrainStorm.Web.Mail.Interfaces;
namespace BrainStorm.Web.Helpers
{
    public static class MailHelper
    {
        
        private static readonly IMail _mailSystem = ServiceLocator.Current.GetInstance<IMail>();
        private const String FROM = "postmaster@denakol.ru";
        private static String DOMAIN = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority;
        private const String HEADER_INVITE = "Потверждение email";
        private const String TEMPLATE_INVITE = @"Для доступа к системе, необходимо потвердить свой email, для этого перейдите по ссылке <a href=""{0}/Account/ConfirmEmail?email={1}&&keyConfirm={2}""> перейти </a> ";
        private const String HEADER_REMEMBER_PASSWORD = "Потверждение email";
        private const String TEMPLATE_REMEMBER_PASSWORD = @"Вы получили новый пароль {0}, используйте его для входа в систему";
        private const String TEMPLATE_BLOCK = "Ваша учетная запись заблокирована";
        private const String HEADER_SERVICE = "Сервисное сообщение";

        public static void SendConfirm(String to, String keyConfirm)
        {
            String body =
                String.Format(TEMPLATE_INVITE, DOMAIN, to, keyConfirm);
            var mail = new MailMessage(FROM, to, HEADER_INVITE, body);
            _mailSystem.SendMail(mail);

        }

        public static void SendNewPassword(String to, String password)
        {
            String body =
                String.Format(
                    TEMPLATE_REMEMBER_PASSWORD,
                    password);
            var mail = new MailMessage(FROM, to, HEADER_REMEMBER_PASSWORD, body);
            _mailSystem.SendMail(mail);
        }

        /// <summary>
        /// Send user a blocking message 
        /// </summary>
        /// <param name="to"></param>
        public static void SendBlockUser(String to)
        {
            var mail = new MailMessage(FROM, to, HEADER_SERVICE, TEMPLATE_BLOCK);
            _mailSystem.SendMail(mail);
        }


    }
}
