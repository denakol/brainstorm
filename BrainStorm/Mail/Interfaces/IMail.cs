﻿using System.Net.Mail;

namespace BrainStorm.Web.Mail.Interfaces
{
    public interface IMail
    {
        void SendMail(MailMessage mail);
    }
}