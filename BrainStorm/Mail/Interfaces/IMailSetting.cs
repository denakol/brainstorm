﻿using System;

namespace BrainStorm.Web.Mail.Interfaces
{
    public interface IMailSetting
    {
        String Host { get; }
        String UserName { get; }
        String UserPassword { get; }
        Int32 Port { get; }
    }
}