﻿using System;
using System.Web.Configuration;
using BrainStorm.Web.Mail.Interfaces;

namespace BrainStorm.Web.Mail
{
    public class MailSetting : IMailSetting
    {
        public String Host
        {
            get { return WebConfigurationManager.AppSettings["HostEmail"]; }
        }

        public String UserName
        {
            get { return WebConfigurationManager.AppSettings["UserNameEmail"]; }
        }

        public String UserPassword
        {
            get { return WebConfigurationManager.AppSettings["UserPasswordEmail"]; }
        }

        public Int32 Port
        {
            get { return Int32.Parse(WebConfigurationManager.AppSettings["PortEmail"]); }
        }
    }
}