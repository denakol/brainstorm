﻿using System.Net;
using System.Net.Mail;
using Microsoft.Practices.Unity;
using BrainStorm.Web.Mail.Interfaces;
using System.Threading;

namespace BrainStorm.Web.Mail
{
    public class MailSystem : IMail
    {
        private static IMailSetting _setting;
        public MailSystem(IUnityContainer container)
        {
            _setting = container.Resolve<IMailSetting>();
        }

        public void SendMail(MailMessage mail)
        {            
            mail.IsBodyHtml = true;
            var smtp = new SmtpClient(_setting.Host)
            {
                Credentials = new NetworkCredential(_setting.UserName, _setting.UserPassword),
                Port = _setting.Port,
                EnableSsl = true
            };
            smtp.Send(mail);
        }
    }
}