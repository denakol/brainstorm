﻿using BrainStorm.Repositories.DTO;
using BrainStorm.Web.Models.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using BrainStorm.Model.Entity;
namespace BrainStorm.Web.MapperWeb
{
    public class JsonMapper
    {
        static JsonMapper()
        {
            Mapper.CreateMap<AttachedImageInfo, AttachedImageView>();
            Mapper.CreateMap< AttachedImageView,AttachedImageInfo>();
            Mapper.CreateMap<GroupsView,GroupInfo>()
                .ForMember(dest => dest.CreationDate, opt => opt.MapFrom(src => src.dateCreated))
                .ForMember(dest=>dest.Members, opt => opt.Ignore());
            Mapper.CreateMap<IdeaInfo, IdeaView>()
             .ForMember(dest => dest.ideaId, opt => opt.MapFrom(src => src.IdeaId))
             .ForMember(dest => dest.attachedImages, opt => opt.MapFrom(src => src.AttachedImages != null ? src.AttachedImages : null));

            Mapper.CreateMap<IdeaView, IdeaInfo>()
             .ForMember(dest => dest.AttachedImages, opt => opt.MapFrom(src => src.attachedImages != null ? src.attachedImages : null));

            Mapper.CreateMap<VoteView, VoteInfo>();
            Mapper.CreateMap<CommentInfo, CommentView>();
            Mapper.CreateMap<CommentView, CommentInfo>();
            Mapper.CreateMap<OutputMessage, MessageView>();
            Mapper.CreateMap<MessageView, OutputMessage>();

        }

        public static OutputMessage MapMessageInfo(MessageView source)
        {
            return Mapper.Map<MessageView, OutputMessage>(source);
        }
        public static MessageView MapMessageView(OutputMessage source)
        {
            return Mapper.Map<OutputMessage, MessageView>(source);
        }

        public static IEnumerable<MessageView> MapMessageView(IEnumerable<OutputMessage> source)
        {
            return Mapper.Map < IEnumerable<OutputMessage>, IEnumerable<MessageView>>(source);
        }


        public static VoteInfo MapVoteInfo(VoteView source)
        {
            return Mapper.Map<VoteView, VoteInfo>(source);
        }
        public static GroupInfo MapGroupInfo(GroupsView source)
        {
            return Mapper.Map<GroupsView,GroupInfo> (source);
        }
        public static IdeaInfo MapIdeaInfo(IdeaView source)
        {
            return Mapper.Map<IdeaView, IdeaInfo>(source);
        }
        public static IEnumerable<IdeaView> MapIdeaView(IEnumerable<IdeaInfo> source)
        {
            return Mapper.Map<IEnumerable<IdeaInfo>, IEnumerable<IdeaView>>(source);
        }
        public static IdeaView MapIdeaView(IdeaInfo source)
        {
            return Mapper.Map<IdeaInfo, IdeaView>(source);
        }

        public static IEnumerable<CommentView> MapCommentView(IEnumerable<CommentInfo> source)
        {
            return Mapper.Map < IEnumerable<CommentInfo>, IEnumerable<CommentView>>(source);
        }

        public static CommentInfo MapCommentInfo(CommentView source)
        {
            return Mapper.Map<CommentView, CommentInfo>(source);
        }

        public static CommentView MapCommentView(CommentInfo source)
        {
            return Mapper.Map<CommentInfo, CommentView>(source);
        }
       
    }
}