﻿using System.ComponentModel.DataAnnotations;

namespace BrainStorm.Web.Models.Account
{
    public class EmailModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}