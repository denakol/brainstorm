﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BrainStorm.Web.Models.Account
{
    public class LogOnModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public String Email { get; set; }

        [Required]
        [DataType(DataType.Password, ErrorMessage = "Введите корректный пароль")]
        [Display(Name = "Пароль")]
        public String Password { get; set; }

        [Display(Name = "Запомнить")]
        public Boolean RememberMe { get; set; }

        public Boolean IsFirstVisit { get; set; }
        public Boolean IsMultiError { get; set; }
    }
}