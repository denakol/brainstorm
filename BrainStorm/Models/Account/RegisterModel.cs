﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BrainStorm.Web.Models.Account
{
    public class RegisterModel
    {
        [Required]
        [Display(Name = "Имя")]
        public String FirstName { get; set; }

        [Required]
        [Display(Name = "Фамилия")]
        public String LastName { get; set; }


        [Required]
        [EmailAddress(ErrorMessage = "Введите корректный Email")]
        [Display(Name = "Email")]
        public String Email { get; set; }

        [Required]
        [DataType(DataType.Password, ErrorMessage = "Введите корректный пароль")]
        [Display(Name = "Пароль")]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Длина пароля должна быть не менее 6 символов")]
        [RegularExpression(@"(.+[^a-z].+)|(.+[^a-z])|([^a-z].+)",
            ErrorMessage = "Пароль должен содержать один символ в верхнем регистре")]
        public String Password { get; set; }

        [Required]
        [Compare("Password")]
        [Display(Name = "Потвердите пароль")]
        public String ConfirmPassword { get; set; }
    }
}