﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrainStorm.Web.Models.Json
{
    public class AttachedImageView
    {
        public String url { get; set; }
    }
}