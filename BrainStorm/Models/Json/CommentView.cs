﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrainStorm.Web.Models.Json
{
    public class CommentView
    {
        public Int32 commentId { get; set; }
        /// <summary>
        /// Text
        /// </summary>
        public String text { get; set; }
        /// <summary>
        /// Creation date
        /// </summary>
        public String created { get; set; }

        

        /// <summary>
        /// Author comment
        /// </summary>
        public Int32 authorId { get; set; }

        /// <summary>
        /// Author name
        /// </summary>
        public String authorName { get; set; }

        public String authorImage { get; set; }
        /// <summary>
        /// Group where the comment is written
        /// </summary>
        public Nullable<Int32> groupId { get; set; }

        /// <summary>
        /// Idea where the comment is written
        /// </summary>
        public Nullable<Int32> ideaId { get; set; }

        /// <summary>
        /// is current user is author coomment
        /// </summary>
        public Boolean isCurrentUserAuthor { get; set; }


        /// <summary>
        /// for js
        /// </summary>
        public Boolean isEdit { get; set; }

        /// <summary>
        /// for js
        /// </summary>
        public String tempText { get; set; }
    }
}