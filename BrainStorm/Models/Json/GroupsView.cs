﻿using BrainStorm.Model;
using BrainStorm.Repositories.DTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
namespace BrainStorm.Web.Models.Json
{
    public class GroupsView
    {
        public GroupsView()
        { }
        public GroupsView(GroupInfo group, UserSessionInfo user, bool cut)
        {
            groupId = group.GroupId;
            name = group.Name;
            task = group.Task;
            tags = group.Tags;
            archived = group.Archived;
            dateCreated = group.CreationDate;
            dateEnded = group.DateEnded;
            adminName = group.AdminName;
            adminId = group.AdminId;
            isUserAdminSite = user.IsAdmin;
            isUserAdminSite = group.AdminId == user.UserId;
            isBlock = group.IsBlock;
            ended = group.Ended;
            state = (Int32)(group.State);
            if (cut)
            {

                if (tags != null)
                {
                    if (tags.Length > 70)
                    {
                        tags = tags.Substring(0, 70);
                    }
                }
                if (task != null)
                {
                    if (task.Length > 140)
                    {
                        task = task.Substring(0, 140);
                    }
                }

            }
            var membersTemp = new List<MembersGroupView>();
            if (group.Members != null)
            {
                if (group.Members.Count() > 5)
                {
                    membersCount = group.Members.Count() - 5;
                    isGroupBig = true;
                    var count = 0;
                    foreach (var member in group.Members)
                    {
                        membersTemp.Add(new MembersGroupView { userId = member.UserId, imgUrl =  member.ImageUrl!=null?member.ImageUrl.Value:null });
                        count++;
                        if (count == 5)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    isGroupBig = false;
                    membersTemp.AddRange(@group.Members.Select(member => new MembersGroupView { userId = member.UserId, imgUrl = member.ImageUrl != null ? member.ImageUrl.Value : null }));
                }
            }
            members = membersTemp.AsEnumerable();
            currentUserOnlyAdmin =false;
        }      
        public Int32 groupId { get; set; }
        public String name { get; set; }
        public String task { get; set; }
        public String tags { get; set; }
        public String dateCreated { get; set; }
         public String dateEnded { get; set; }
        public String adminName { get; set; }
        public Int32 adminId { get; set; }
        
        public Int32 state { get; set; }

        public Int32 membersCount { get; set; }
        public IEnumerable<MembersGroupView> members { get; set; }
        public Boolean isUserAdminSite { get; set; }
        public Boolean isUserAdminGroup { get; set; }
        public Boolean isBlock { get; set; }
        public Boolean isGroupBig { get; set; }

        public Boolean ended { get; set; }
        public Boolean archived { get; set; }
        public Boolean currentUserOnlyAdmin { get; set; }
        

    }
}