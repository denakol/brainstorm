﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrainStorm.Web.Models.Json
{
    public class IdeaView
    {
        /// <summary>
        /// Id
        /// </summary>
        public Int32 ideaId;

        /// <summary>
        /// Text
        /// </summary>
        public String text { get; set; }

        /// <summary>
        /// name task
        /// </summary>
        public String name { get; set; }

        /// <summary>
        /// Code Example
        /// </summary>
        public String codeExample { get; set; }

        /// <summary>
        /// Сreation date
        /// </summary>
        public String created { get; set; }


        public Boolean isVote { get; set; }
        /// <summary>
        /// count like
        /// </summary>
        public Int32 countLike { get; set; }

        /// <summary>
        /// count dislike
        /// </summary>
        public Int32 countDislike { get; set; }
        /// <summary>
        /// Attached image
        /// </summary>
        public IEnumerable<AttachedImageView> attachedImages { get; set; }

        /// <summary>
        /// Full name author
        /// </summary>
        public String authorName { get; set; }

        public String authorImage { get; set; }
  
        /// <summary>
        /// Author id
        /// </summary>
        public Int32 authorId { get; set; }

        /// <summary>
        /// group id
        /// </summary>
        public Int32 groupId { get; set; }

        public Boolean tempText { get; set; }
        public Boolean tempName { get; set; }
        public Boolean isEdit { get; set; }

    }
}