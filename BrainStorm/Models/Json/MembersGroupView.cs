﻿using System;

namespace BrainStorm.Web.Models.Json
{
    public class MembersGroupView
    {
        public Int32 userId { get; set; }
        public String imgUrl{get;set;}
    }
}