﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrainStorm.Web.Models.Json
{
    public class MessageView
    {
        public Int32 messageEntityId { get; set; }


        public Int32 senderId { get; set; }
        public virtual String senderFullName { get; set; }


        public Int32 recipientId { get; set; }
        public virtual String recipientFullName { get; set; }


        public String created { get; set; }

        public String message { get; set; }
    }
}