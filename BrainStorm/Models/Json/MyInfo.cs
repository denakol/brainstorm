﻿using System;

namespace BrainStorm.Web.Models.Json
{
    public class MyInfo
    {
        public Int32 id { get; set; }
        public Boolean isUserAdminSite { get; set; }
        public String fullName { get; set; }
        public Boolean isBlock { get; set; }
    }
}