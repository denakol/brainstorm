﻿using BrainStorm.Model;
using System;

namespace BrainStorm.Web.Models.Json
{
    public class UserGroup
    {
        public Int32 groupId { get; set; }
        public StateUserGroup state { get; set; }
        public Int32 userId { get; set; }
    }
}