﻿using BrainStorm.Model;
using BrainStorm.Repositories.DTO;
using System;
using System.Security.Policy;

namespace BrainStorm.Web.Models.Json
{
    public class UserView
    {
        public UserView(UserInfo user, bool cut)
        {
            fullName = String.Format("{0} {1}", user.FirstName, user.LastName);
            imageUrl =user.ImageUrl!=null?user.ImageUrl.Value:null;
            id = user.Id;
            interest = user.Interest;
            about = user.About;
            if (cut)
            {

                if (interest != null)
                {
                    if (interest.Length > 70)
                    {
                        interest = interest.Substring(0, 70);
                    }
                }
                if (about != null)
                {
                    if (about.Length > 140)
                    {
                        about = about.Substring(0, 140);
                    }
                }

            }
            text = fullName;
            state = user.State;
            isBlock = user.IsBlock;
            isAdmin = user.IsAdmin;
        }
        public Int32 id { get; set; }
        public String fullName { get; set; }
        public String imageUrl { get; set; }
        public String about { get; set; }
        public String interest { get; set; }
        public String text { get; set; }
        public StateUserGroup state { get; set; }
        public Boolean isBlock { get; set; }
        public Boolean isAdmin { get; set; }
    }
}