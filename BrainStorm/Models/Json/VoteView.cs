﻿using BrainStorm.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrainStorm.Web.Models.Json
{
    public class VoteView
    {
        public Int32 ideaId { get; set; }
        public Int32 authorId { get; set; }
        public VoteType voteType { get; set; }
    }
}