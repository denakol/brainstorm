﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrainStorm.Web.Models
{
    public class Password
    {
        [JsonProperty("old")]
        public String Old { get; set; }
          [JsonProperty("newP")]
        public String New { get; set; }
          [JsonProperty("repeat")]
        public String Repeat { get; set; }
    }
}