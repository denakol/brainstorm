﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrainStorm.Web.Models.Signalr
{
    public class User
    {
        public String FullName { get; set; }
        public HashSet<string> ConnectionIds { get; set; }
    }
}