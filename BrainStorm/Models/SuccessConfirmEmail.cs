﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrainStorm.Web.Models
{
    public class SuccessConfirmEmail: SuccessModalModel
    {
         private const String HEADER  = "Адрес потвержден";
        private const String BODY = "Адрес электронной почты потвержден. Вы можете начать работу с системой.";
        public SuccessConfirmEmail()
        {
            this.Body = BODY;
            this.Header = HEADER;
        }
    }
}