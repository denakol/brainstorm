﻿using System;

namespace BrainStorm.Web.Models
{
    public class SuccessModalModel
    {
        public String Header { get; set; }
        public String Body { get; set; }
    }
}