﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrainStorm.Web.Models
{
    public class SuccessRegisterModalModel : SuccessModalModel
    {
        private const String HEADER  = "Потверждение Email";
        private const String BODY = "На ваш email отправлено письмо, следуйте по инструкции, для потверждения email";
        public SuccessRegisterModalModel()
        {
            this.Header = HEADER;
            this.Body = BODY;
        }
    }
}