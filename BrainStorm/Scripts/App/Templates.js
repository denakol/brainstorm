﻿
angular.module("template", []).run(["$templateCache", function ($templateCache) {
    $templateCache.put("template/window.html",
      '<div class="modal-dialog modal-small">' +
          '<div class="modal-content">' +
              '<div class="modal-header">' +
                  '<h3>{{message.head}}</h3>' +
              '</div>' +
              '<div class="modal-body">' +
                    '<label>{{message.text}}</label>' +
               '</div>' +
               '<div class="modal-footer">' +
                '<button ng-click="confirm()" class="btn btn-primary pull-left" >Да</button>' +
                '<button ng-click="close()" class="btn btn-primary" >Нет</button>' +
                '<div class="">'+
                   '<div class="alert alert-warning" ng-repeat="alert in Alerts.messages" ng-click="Alerts.remove(alert)">' +
                      '{{alert.text}}'+
                   '</div>'+
                ' </div>'+
               '</div>' +
          '</div>' +
     '</div>');
    $templateCache.put("template/message.html",
      '<div class="modal-dialog modal-small">' +
          '<div class="modal-content">' +
              '<div class="modal-header">' +
                  '<h3>{{title}}</h3>' +
                    '</div>' +
                     '<div class="modal-body">' +
                    '<label>{{message}}</label>' +
                  '</div>' +
                  '<div class="modal-footer">' +
                '<button ng-click="close()" class="btn btn-primary" >OK</button>' +
           '</div>' +
          '</div>' +
     '</div>');

    $templateCache.put("template/responseMessage.html",
    '<div class="modal-dialog modal-small">' +
        '<div class="modal-content">' +
            '<div class="modal-header">' +
                '<h3>Новое сообщение</h3>' +
                  '</div>' +
                  '<form class="form-horizontal" ng-submit="close()" >' +
                   '<div class="modal-body">' +
                     ' <div class="form-group">'+
                          '<label class = "col-lg-4 control-label" >Получатель</label>'+
                          '<div class="col-lg-6">'+
                             '<input type="text" class="form-control" ng-model="senderFullName" disabled>' +
                         ' </div>'+
                       '</div>'+
                    ' <div class="form-group">' +
                          '<div class="col-lg-12">' +
                             '<textarea ng-model="text" class="col-lg-12 form-control" required></textarea>' +
                         ' </div>' +
                       '</div>' +
                '</div>' +
                '<div class="modal-footer">' +
              '<button  class="btn btn-primary" >Отправить</button>' +
         '</div>' +
          '<div class="alerts">' +
                       '<div class="alert alert-danger" ng-repeat="alert in Alerts.messages" ng-click="Alerts.remove(alert)">' +
                       '{{alert.text}}' +
                    '</div>' +
               '</div>' +
         '</form>'+
        '</div>' +
   '</div>');
    $templateCache.put("template/leave.html",
    '<div class="modal-dialog modal-small">' +
        '<div class="modal-content">' +
            '<div class="modal-header">' + '<h3>Покинуть группу</h3>' + '</div>' +
                '<div class="modal-body">' +
                    '<label>Вы уверенны что хотите покинуть группу?</label>' +
                '</div>' +
                '<div class="modal-footer">' +
                    '<button ng-click="confirm()" class="btn btn-primary pull-left" >Да</button>' +
                    '<button ng-click="close()" class="btn btn-primary" >Нет</button>' +
                   
                '</div>' +
                 '<div class="alerts">' +
                       '<div class="alert alert-danger" ng-repeat="alert in Alerts.messages" ng-click="Alerts.remove(alert)">' +
                       '{{alert.text}}' +
                    '</div>' +
               '</div>' +
            '</div>' +
         '</div>');

    $templateCache.put("template/password.html",
    '<div class="modal-dialog modal-small">' +
          '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<h3>Сменить пароль</h3>' +
            '</div>' +
            '<form name="myForm" class="form-horizontal" ng-submit="change(password)" >' +
            '<div class="modal-body">' +

              '<div class="form-group">' +
                    '<label class="col-lg-3 control-label">Старый пароль</label>' +
                    '<div class="col-lg-9">' +
                        '<input class="form-control" name="old" ng-model="password.old"  ng-minlength="6" type="password" required>' +
                   ' </div>' +
            '  </div>' +
              '<div class="form-group">' +
                    '<label class="col-lg-3 control-label">Новый пароль</label>' +
                    '<div class="col-lg-9">' +
                        '<input class="form-control" name="new" ng-model="password.newP" type="password" ng-minlength="6"  required>' +
                   ' </div>' +
            '  </div>' +
            '<div class="form-group">' +
                    '<label class="col-lg-3 control-label">Повторите пароль</label>' +
                    '<div class="col-lg-9">' +
                        '<input class="form-control" name="repeat" ng-model="password.repeat" type="password"  required>' +
                   ' </div>' +
            '  </div>' +
           '</div>' +
            '<div class="modal-footer">' +
            '<button type="submit" id="submit" class="btn btn-primary pull-left" >Изменить</button>' +
            '<a href="" ng-click="close()" class="btn btn-primary" >Отмена</a>' +
            '</div>' +
            '<div class="alerts">' +
                       '<div class="alert alert-danger" ng-show="myForm.old.$error.minlength ||myForm.new.$error.minlength">' +
                       'Длина пароля должна быть больше 6' +
                    '</div>' +
                     '<div class="alert alert-danger" ng-show="password.repeat!=password.newP && !myForm.old.$error.minlength">' +
                       'Пароли должны совпадать' +
                    '</div>' +
                    '<div class="alert alert-danger" ng-repeat="alert in Alerts.messages" ng-click="Alerts.remove(alert)">' +
                      '{{alert.text}}'+
                   '</div>'+
               '</div>' +
            '</form>' +
        '</div>' +
        '</div>');
    

    $templateCache.put("template/avatar.html",
    '<div class="modal-dialog modal-small">' +
        '<div class="modal-content">' +
            '<div class="modal-header">' + '<h3>Изменить аватар</h3>' + '</div>' +
                '<div class="modal-body clearfix">' +
                     '<input type="file" accept="image/*" image="image2" resize-max-height="100" resize-max-width="100" resize-quality="0.7" />' +
                        '<img ng-show="image2" ng-src="{{image2.resized.dataURL}}" />'+
                '</div>' +
                '<div class="modal-footer">' +
                    '<button ng-click="send()" class="btn btn-primary pull-left" >Сохранить</button>' +
                    '<button ng-click="close()" class="btn btn-primary" >Нет</button>' +

                '</div>' +
                 '<div class="alerts">' +
                       '<div class="alert alert-danger" ng-repeat="alert in Alerts.messages" ng-click="Alerts.remove(alert)">' +
                       '{{alert.text}}' +
                    '</div>' +
               '</div>' +
            '</div>' +
         '</div>');
    $templateCache.put("template/ImageBox.html",
    '<div class="modal-dialog image-box">' +
        '<div class="modal-content">' +
            '<div class="modal-header">' +
                 '<button type="button" class="close" ng-click="close()">&times;</button>' +
            '</div>' +
                '<div class="modal-body clearfix ">' +
       ' <div class="outer-outer">'+
       ' <div class="outer">' +
                        '<img class="inner" ng-src="/Content/Attachments/big-{{img.url}}"/>' +
        '</div>'+
                '</div>' +
        '</div>'+
            '</div>' +
         '</div>');
}]);