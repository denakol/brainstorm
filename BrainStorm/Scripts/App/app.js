﻿angular.module('Home', ['UserServices',
    'infinite-scroll',
    'ngScrollTo',
    'serviceBr',
    'myFilters',
    'ui.bootstrap',
'imageupload',
, 'monospaced.elastic',
'Providers',
'ui.select2',
'template',
'BrFactory'
])
    .config(function ($routeProvider, MessageHubProvider, $locationProvider) {
        $routeProvider.
            when('/groups', { templateUrl: '/Template/Groups', controller: GroupsController }).
            when('/group/:id', { templateUrl: '/Template/Group', controller: GroupController }).
            when('/group/:id/members', { templateUrl: '/Template/Users', controller: MembersController }).
             when('/group/:id/requests', { templateUrl: '/Template/Users', controller: RequestsController }).
            when('/users', { templateUrl: '/Template/Users', controller: UsersController }).
            when('/myGroups', { templateUrl: '/Template/Groups', controller: MyGroupsController }).
             when('/myGroups/archive', { templateUrl: '/Template/Groups', controller: MyGroupsArchiveController }).
            when('/user/:id', { templateUrl: '/Template/UserPage', controller: UserController }).
             when('/myMessage', { templateUrl: '/Template/MyMessage', controller: MessageController }).
            when('/myPage', { templateUrl: '/Template/MyPage', controller: MyPageController }).
            when('/myDraft', { templateUrl: '/Template/MyDraft', controller: MyDraftController }).
            otherwise({ redirectTo: '/groups' });
        MessageHubProvider.startHub(MessageHubProvider.defaultServer, 'MessageHub');

        $locationProvider.html5Mode(true);
    });
;

function MainController($scope, $location, $dialog, $templateCache, $timeout,$window, $http, sharedService, MessageTimeout, Message, User, Member, Enum, MessageHub) {
    $scope.isGroupsOrUsersView = true;
    $scope.id = User.getMyId();
    $scope.Enum = Enum;
    $scope.myGroupInfo = Member.countInvites();
    $scope.countMessages = Message.countMessages();
    $scope.item = [{ value: "" }, { value: "" }];
    $scope.search = function () {
        sharedService.search($scope.query);
    };
    $scope.$on('$routeChangeSuccess', function () {
        var items = $scope.item;
        var regGroup = new RegExp("/group/\\d", "g");

        var path = $location.path();
        $scope.path = $location.path();
        MessageTimeout.setTimeout($scope.path == '/myMessage');
        switch (true) {
            case path == '/groups':
                $scope.searchPlaceholder = "Поиск по имени, тегам и тексту задачи";
                $scope.isGroupsOrUsersView = true;
                break;
            case path == '/users':
                $scope.searchPlaceholder = "Поиск по имени и интересам";
                $scope.isGroupsOrUsersView = true;
                break;
            case path == '/myGroups':
                $scope.searchPlaceholder = "Поиск по имени, тегам и тексте задачи";
                $scope.isGroupsOrUsersView = true;
                break;
            case path == '/myMessage':
                $scope.isGroupsOrUsersView = false;
                break;
            case path == '/myPage':
                $scope.isGroupsOrUsersView = false;
                break;
            case path == '/myDraft':
                $scope.isGroupsOrUsersView = false;
                break;
            case regGroup.test(path):
                $scope.isGroupsOrUsersView = false;
                break;
            default:

        }
    });
    $scope.userId = User.getMyId();
    $scope.openNewGroupModal = function () {
        var t = '<div class="modal-dialog modal-small">' +
          '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<h3>Новая группа</h3>' +
            '</div>' +
            '<form name="newGroupForm" class="form-horizontal" ng-submit="add(group)" >' +
            '<div class="modal-body">' +

              '<div class="form-group">' +
                    '<label class="col-lg-3 control-label">Название</label>' +
                    '<div class="col-lg-9">' +
                        '<input class="form-control" name="Name" ng-model="group.name" type="text" placeholder="Введите имя новой группы" required>' +
                   ' </div>' +
            '  </div>' +
               '<div class="form-group">' +
                    '<label class="col-lg-3 control-label">Задача</label>' +
                   ' <div class="col-lg-9">' +
                       '<textarea class="col-lg-12" name="Task" ng-model="group.task" placeholder="Введите задачу для группы" required></textarea>' +
                    '</div>' +
              ' </div>' +
           '</div>' +
            '<div class="modal-footer">' +
            '<button type="submit" id="submit" class="btn btn-primary pull-left" >Создать</button>' +
            '<button ng-click="close(group)" class="btn btn-primary" >Отмена</button>' +
            '</div>' +
            '</form>' +
        '</div>' +
        '</div>';

        $scope.opts = {
            backdrop: true,
            keyboard: true,
            backdropClick: true,
            template: t,
            controller: 'NewGroupController'
        };
        var d = $dialog.dialog($scope.opts);
        d.open();

    };

    var path = $location.path();
    $scope.path = $location.path();
    var regGroup = new RegExp("/group/\\d", "g");
    switch (true) {
        case path == '/groups':
            $scope.searchPlaceholder = "Поиск по имени, тегам и тексте задачи";
            $scope.isGroupsOrUsersView = true;
            break;
        case path == '/users':
            $scope.searchPlaceholder = "Поиск по имени и интересам";
            $scope.isGroupsOrUsersView = true;
            break;
        case path == '/myMessage':
            $scope.isGroupsOrUsersView = false;
            break;
        case path == '/myPage':
            $scope.isGroupsOrUsersView = false;
            break;
        case path == '/myDraft':
            $scope.isGroupsOrUsersView = false;
            break;
        case regGroup.test(path):
            $scope.isGroupsOrUsersView = false;
            break;
    }
    MessageHub.on('received', function (data) {
        $scope.countMessages.count++;
    });
    MessageHub.on('block', function ( data) {
        $scope.isBlockUser = true;
        $scope.time = 60
        $timeout(function () {
            $window.location.reload()
        }, 60000);
        var timer= function() {
            $scope.time--;
            if (($scope.time) > 0)
                $timeout(timer, 1000);

        };
        timer();

     
    });
    
    MessageHub.on('read', function (data) {

        if ($scope.countMessages.count > 0) {
            $scope.countMessages.count--;
        }
    });



}

function NewGroupController($scope, dialog, Group, sharedService) {
    $scope.group = { name: "", task: "" }

    $scope.add = function (group) {
        var newGroup = new Group();
        newGroup.name = group.name;
        newGroup.task = group.task;
        newGroup.$save(function success() {
            dialog.close();
            ///Переделать на функцию обновления такое же действие название другое.
            sharedService.search("");
        });
    };
    $scope.close = function (result) {
        dialog.close(result);
    };
}

function GroupsController($scope,$location, Group, Member, User, sharedService, Enum) {
    $scope.groups = [];
    $scope.counter = 1;
    $scope.id = User.getMyId();
    $scope.query = "";
    $scope.predicat = 'groupId';
    $scope.reverse = true;
    $scope.busy = false;
    $scope.loadMore = function () {
        $scope.scrollDisable = true;
        $scope.dowland = true;
        var items = Group.pageGroups({ pageNumber: $scope.counter, pageSize: 10 }, function success() {
            if (items.length !== 0) {
                for (var i = 0; i < items.length; i++) {
                    $scope.groups.push(items[i]);
                }
                $scope.counter += 1;
                $scope.scrollDisable = false;

            }
            $scope.dowland = false;
        });

    };
    $scope.join = function (group) {
        var member = new Member();
        member.groupId = group.groupId;
        member.state = Enum.stateGroup.send;
        member.userId = $scope.id.id;
        member.$save(function success() {
            group.state = Enum.stateGroup.send;
        });
    };
    $scope.$on('startSearch', function () {
        $scope.query = sharedService.query;
        if ($scope.query) {
            if ($scope.query.length > 2) {

                if (!$scope.dowladed || $scope.query.length < 4) {
                    $scope.busy = true;
                    $scope.scrollDisable = true;
                    $scope.info = false;
                    $scope.notResult = false;
                    $scope.groups = Group.search({ search: $scope.query }, function success() {
                        $scope.busy = false;
                        $scope.dowladed = true;
                        if ($scope.groups.length === 0)
                            $scope.notResult = true;
                        ;

                    }

                    );
                } else {
                    if ($scope.groups.length === 0) {
                        $scope.notResult = true;

                    }
                    $scope.info = false;
                }
                $scope.tempSearch = $scope.query;
            }
            else {
                $scope.groups = [];
                if (!$scope.notResult) {
                    $scope.info = true;
                }

            }
        }
        else {

            $scope.groups = Group.pageGroups({ pageNumber: 1, pageSize: 10 });
            $scope.flag = false;
            $scope.counter = 2;
            $scope.info = false;
            $scope.tempSearch = "";
            $scope.notResult = false;
            $scope.scrollDisable = false;
            $scope.dowladed = false;

        }
    });

    $scope.confirmInvite = function (group) {
        var member = new Member();
        member.groupId = group.groupId;
        member.state = Enum.stateGroup.adoptet;
        member.userId = $scope.id.id;
        member.$save(function success() {
            group.state = Enum.stateGroup.adoptet;
            $scope.myGroupInfo.count -= 1;
        });
    };

    $scope.block = function (group) {
        group.isBlock = true;
        Group.update(group,
            function succsess() {
             
            },
            function error() {
                group.isBlock = false;
            });


    };

    $scope.unBlock = function (group, predicat) {
        group.isBlock =false;
        Group.update(group, function succsess() {

        },
            function error() {     
                group.isBlock = false;
            });
    };

};

function UsersController($scope,$timeout, User, sharedService, UserAdmin, MessageHub) {
    $scope.counter = 1;
    $scope.busy = false;
    $scope.isUsers = true;
    $scope.notBackButton = true;
    $scope.users = [];
    $scope.loadMore = function () {
        $scope.dowland = true;
        $scope.scrollDisable = true;
        var items = User.pageUsers({ pageNumber: $scope.counter, pageSize: 20 }, function success() {
            if (items.length !== 0) {

                for (var i = 0; i < items.length; i++) {
                    $scope.users.push(items[i]);
                }
                $scope.counter += 1;
                $scope.scrollDisable = false;

            }
            $scope.dowland = false;

        });

    };

    $scope.$on('startSearch', function () {
        $scope.query = sharedService.query;
        if ($scope.query) {
            if ($scope.query.length > 2) {
                if (!$scope.dowladed || $scope.query.length < 4) {
                    $scope.busy = true;
                    $scope.scrollDisable = true;
                    $scope.info = false;
                    $scope.notResult = false;
                    $scope.users = User.search({ search: $scope.query, type: 2 }, function success() {
                        $scope.busy = false;
                        $scope.dowladed = true;
                        if ($scope.users.length === 0)
                            $scope.notResult = true;
                        ;

                    }

                    );
                } else {
                    if ($scope.users.length === 0) {
                        $scope.notResult = true;
                    }
                    $scope.info = false;
                }
                $scope.tempSearch = $scope.query;
            }
            else {
                $scope.users = [];
                if (!$scope.notResult) {
                    $scope.info = true;
                }

            }
        }
        else {

            $scope.users = User.pageUsers({ pageNumber: 1, pageSize: 20 });
            $scope.counter = 2;
            $scope.flag = false;
            $scope.info = false;
            $scope.tempSearch = "";
            $scope.notResult = false;
            $scope.scrollDisable = false;
            $scope.dowladed = false;

        }
    });


    $scope.assign = function (user) {
        user.assign = true;
        user.time = 25;
        user.promisesAssign = $timeout(function () {
            var us = new UserAdmin();
            us.Id = user.id
            us.isAdmin = true;
            us.$save(function succsess() {
                user.isAdmin = true;
            },
            function error() {
                user.isAdmin = false;
            }
            );

        }, 25000);

        var timer = function () {
            user.time--;

            if (((user.time) > 0 ) &&user.assign)
                $timeout(timer, 1000);

        };
       timer();



    };

    $scope.cancelAssign = function (user) {
        $timeout.cancel(user.promisesAssign);
        user.assign = false;

       
    };
    $scope.confirmAssign = function(user)
    {
        $timeout.cancel(user.promisesAssign);
        var us = new UserAdmin();
        us.Id = user.id
        us.isAdmin = true;
        us.$save(function succsess() {
            user.isAdmin = true;
        },
        function error() {
            user.isAdmin = false;
        }
        );
    }
    $scope.block = function (user) {
        $timeout.cancel(user.promisesAssign);
        var us = new UserAdmin();
        us.Id = user.id
        us.isBlock = true;
        us.$save(function succsess() {
            user.isBlock = true;
            MessageHub.invoke('BlockUser', user.id, function (messageOnlyReceiver) {              
            });
        },
        function error() {

        }
        );
    };
    $scope.unblock = function (user) {
        var us = new UserAdmin();
        us.Id = user.id;
        us.isBlock = false;
        us.$save(function succsess() {
            user.isBlock = false;
        },
        function error() {
            user.isBlock = true;
        }
        );
    };

    $scope.$on('$locationChangeStart', function (event, next, current) {
        angular.forEach($scope.users, function (user) {
            if (user.assign) {
                $timeout.cancel(user.promisesAssign);
            };
        }
       );
    });
};

function UserController($scope, $routeParams, $dialog, User, MessageHub) {
    $scope.user = User.get({ id: $routeParams.id });
    $scope.send = function (message) {

        var messageInfo = { text: message, recipients: [] };
        messageInfo.recipients.push({ recipientId: $routeParams.id });

        MessageHub.invoke('send', messageInfo, function (messageOnlyReceiver) {
            $scope.isSending = false;
            var d = $dialog.messageBox("Отправлено", "Ваше собщение отправлено, вы можете найти его в вкладке исходящие", "EEEE");
            d.open();
        });
    };
}

function MembersController($scope, $routeParams, Member, Enum, $filter) {
    $scope.users = Member.members({ groupId: $routeParams.id });
    $scope.users.$then(function () {
        var user = $filter('getUserById')($scope.users, $scope.id.id);
        if (user) {
            $scope.isMember = true;
            $scope.isAdmin = user.state == Enum.stateGroup.admin;
        }
    });
    $scope.groupId = $routeParams.id;
    $scope.assign = function (user) {
        var member = new Member();
        member.groupId = $scope.groupId;
        member.state = Enum.stateGroup.admin;
        member.userId = user.id;
        member.$save(function success() {
            user.state = Enum.stateGroup.admin;
        });
    };
    $scope.block = function (user) {
        var member = new Member();
        member.groupId = $scope.groupId;
        member.state = Enum.stateGroup.block;
        member.userId = user.id;
        member.$save(function success() {
            user.state = Enum.stateGroup.block;
        });
    };
    $scope.unblock = function (user) {
        var member = new Member();
        member.groupId = $scope.groupId;
        member.state = Enum.stateGroup.adoptet;
        member.userId = user.id;
        member.$save(function success() {
            user.state = Enum.stateGroup.adoptet;
        });
    };
};

function RequestsController($scope, $routeParams, Member, Enum) {
    $scope.users = Member.requests({ Id: $routeParams.id });
    $scope.groupId = $routeParams.id;
    $scope.isRequests = true;
    $scope.confirm = function (user) {
        var member = new Member();
        member.groupId = $scope.groupId;
        member.state = Enum.stateGroup.adoptet;
        member.userId = user.id;
        member.$save(function success() {
            $scope.users.splice($scope.users.indexOf(user), 1);
        });
    };
    $scope.delete = function (user) {
        var member = new Member();
        member.groupId = $scope.groupId;
        member.state = Enum.stateGroup.notSend;
        member.userId = user.id;
        member.$save(function success() {
            $scope.users.splice($scope.users.indexOf(user), 1);
        });
    };

}

function GroupController($scope, $http, $route, $rootScope, $routeParams, Member, $dialog, Group, Enum) {
    $scope.group = Group.get({ id: $routeParams.id });
    $scope.group.$then(function () {
        $scope.isMember = $scope.group.state == Enum.stateGroup.adoptet || $scope.group.state == Enum.stateGroup.admin;
        $scope.isAdminGroup = $scope.group.state == Enum.stateGroup.admin;
        $scope.enable = $scope.group.ended;
        $scope.state = function () {
            if ($scope.group.ended) {
                if ($scope.group.archived) {
                    return Enum.stateBrainstorm.get(Enum.stateBrainstorm.archive);
                }
                return Enum.stateBrainstorm.get(Enum.stateBrainstorm.ended);
            }

            return Enum.stateBrainstorm.get(Enum.stateBrainstorm.inProcces);
        };
    }
   );

    $rootScope.group = $scope.group;
    $rootScope.id = $scope.id;
    var tempTask;
    $scope.isEditTask = false;

    $scope.invite = function () {
        var t = '<div class="modal-dialog modal-small">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<h3>Пригласить в группу</h3>' +
            '</div>' +
            '<form name="newGroupForm" class="form-horizontal" ng-submit="add(selected)" >' +
            '<div class="modal-body">' +
            '<div class="form-group">' +
            '<label class="col-lg-3 control-label">Имя</label>' +
            '<div class="col-lg-9">' +
            ' <input  class="form-control" type="text" ng-model="selected" ng-change="changeUser()"typeahead="state as state.fullName for state in states($viewValue)|filter:$viewValue">' +
            ' </div>' +
            '  </div>' +
            '</div>' +
            ' <div class="validation-summary-errors" ng-show="isNonSelected">' +
            ' <ul>' +
            '<li>Начните вводить имя пользователя и выберите пользователя из списка</li>' +
            '</ul>' +
            '</div>' +
            '<div class="modal-footer">' +
            '<button type="submit" id="submit" class="btn btn-primary pull-left" >Пригласить</button>' +
            '<button ng-click="close(selected)" class="btn btn-primary" >Отмена</button>' +
            '</div>' +
            '</form>' +
            '</div>' +
            '</div>';

        $scope.opts = {
            backdrop: true,
            keyboard: true,
            scope: $scope,
            backdropClick: true,
            template: t,
            controller: 'InviteController'
        };
        var d = $dialog.dialog($scope.opts);
        d.open();

    };
    $scope.ended = function (group) {
        $scope.group.ended = true;
        $scope.group.$update(
            function succsess() {
                $route.reload();
            },
            function error() {
                $scope.group.ended = false;

            });
    };
    $scope.archive = function (group) {
        $scope.group.ended = true;
        $scope.group.$update(
            function succsess() {
                $route.reload();
            },
            function error() {
                $scope.group.ended = false;

            });
    };
    $scope.reopen = function (group) {
        $scope.group.ended = false;
        $scope.group.$update(
            function succsess() {
                $route.reload();
            },
            function error() {
                $scope.group.ended = true;

            });
    };
    $scope.archived = function (group) {
        $scope.group.archived = true;
        $scope.group.$update(
            function succsess() {
                $route.reload();
            },
            function error() {
                $scope.group.archived = true;

            });
    };
    $scope.leave = function () {

        $scope.opts = {
            backdrop: true,
            keyboard: true,
            backdropClick: true,
            templateUrl: 'template/leave.html',
            controller: 'LeaveController'
        };
        var d = $dialog.dialog($scope.opts);
        d.open().then(function (result) {
            if (result) {
                if (result.status == Enum.statusDialog.сanceled) {
                    return;
                }
                if (result.status == Enum.statusDialog.ok) {
                    $scope.isMember = false;
                    $scope.group.state = Enum.stateGroup.notSend;
                }
                else {
                    if (result.type = Enum.typeError.onlyAdmin) {
                        var d = $dialog.messageBox("Действие запрещено", "Вы являетесь единственным администратором группы и поэтому не можете покинуть ее. Вы можете завершить брейншторм и затем заархивировать группу на странице группы");
                        d.open();
                    }
                }
            }
        });
    };


    $scope.join = function (group) {
        var member = new Member();
        member.groupId = group.groupId;
        member.state = Enum.stateGroup.send;
        member.userId = $scope.id.id;
        member.$save(function success() {
            group.state = Enum.stateGroup.send;
        });
    };

    $scope.beginEditTask = function (task) {
        if ($scope.isAdminGroup && !$scope.group.archived) {
            tempTask = angular.copy(task);
            $scope.isEditTask = true;
        }
    };
    $scope.editTask = function () {
        $scope.group.$update({ editTask: "EditTask" },
            function succsess() {
                $scope.isEditTask = false;
            },
            function error() {
                $scope.group.task = angular.copy(tempTask);
            });
    };
    $scope.endEditTask = function () {
        $scope.group.task = angular.copy(tempTask);
        $scope.isEditTask = false;
    };


}

function LeaveController($scope, $rootScope, dialog, Member, Enum, Alerts) {

    $scope.Alerts = Alerts;

    $scope.close = function () {
        dialog.close({ status: Enum.statusDialog.сanceled });
    };


    $scope.confirm = function () {
        var member = new Member();
        member.groupId = $scope.group.groupId;
        member.state = Enum.stateGroup.notSend;
        member.userId = $scope.id.id;
        member.$save(function success() {
            dialog.close({ status: Enum.statusDialog.ok });
        },
            function error(data) {
                if (data.status == 403) {
                    dialog.close({ status: Enum.statusDialog.error, type: data.data.type })
                } else {
                    $scope.Alerts.error('Неизвестная ошибка');
                }

            });
    };
}

function InviteController($scope, dialog, User, Member, limitToFilter, Enum) {

    $scope.isNonSelected = false;
    $scope.selected = "";
    $scope.states = function (userName) {
        return User.search({ search: userName, type: 1 }).$then(function (response) {
            return limitToFilter(response.data, 15);
        });
    };
    $scope.add = function (selected) {
        if (selected.id != undefined) {
            $scope.isNonSelected = false;
            var member = new Member();
            member.groupId = $scope.group.groupId;
            member.state = Enum.stateGroup.invite;
            member.userId = selected.id;
            member.$save(function success() {
                dialog.close();
            });

        }
        else {
            $scope.isNonSelected = true;
        }

    };

    $scope.changeUser = function () {
        $scope.isNonSelected = false;
    };
    $scope.close = function (result) {
        dialog.close(result);
    };
}

function CommentTaskController($scope, $routeParams, $filter, Comment) {
    $scope.comment = { text: "" };


    $scope.id.$then(function () {
        $scope.group.$then(function (result) {
            if ($scope.group.isBlock && !$scope.id.isUserAdminSite) {
                return;
            };

            if (!$scope.group.archived || $scope.isAdminGroup) {
                $scope.comments = Comment.query({ groupId: $routeParams.id });
            }


        });
    });

    $scope.addComment = function (comment) {
        var newcomment = new Comment();
        newcomment.text = comment.text;
        newcomment.groupId = $routeParams.id;
        newcomment.$save(function success(result) {
            $scope.comments.push(result);
            $scope.comment = "";
        });
    };

    $scope.removeComment = function (comment) {
        comment.$delete(function success() {
            $scope.comments.splice($scope.comments.indexOf(comment), 1);
        });
    };
    $scope.beginEditComment = function (comment) {
        comment.tempString = angular.copy(comment.text);
        comment.isEdit = true;
    };
    $scope.editComment = function (comment) {
        comment.$update(function success() {
            comment.isEdit = false;
        }
        );
    };
    $scope.endEditComment = function (comment) {
        comment.text = angular.copy(comment.tempString);
        comment.isEdit = false;
        comment.tempString = "";
    };
}

function IdeaController($scope, $http, $routeParams, $dialog, $filter, Idea, Comment, Enum, Vote) {
    $scope.ideas = [];
    $scope.idea = { name: "", text: "" };

    $scope.id.$then(function () {
        $scope.group.$then(function (result) {
            if ($scope.group.archived && !$scope.isAdminGroup || $scope.group.isBlock && !$scope.id.isUserAdminSite) {
                return;
            }
            $scope.ideas = Idea.query({ groupId: $routeParams.id },
                     function success(result) {
                         angular.forEach($scope.ideas, function (idea) {
                             idea.comments = Comment.query({ ideaId: idea.ideaId, groupId: $routeParams.id });
                         });
                     });

            if ($scope.group.ended) {
                $scope.ideas.$then(function () {
                    var predicate = 'countLike';
                    $scope.ideas = $filter('orderBy')($scope.ideas, predicate, true);

                });
            }
        });
    });

    $scope.images = [];


    $scope.addIdea = function (idea) {
        $scope.isSent = true;
        $scope.codeShow = false;
        var newIdea = new Idea();
        newIdea.name = idea.name;
        newIdea.text = idea.text;
        newIdea.codeExample = idea.codeExample;
        newIdea.groupId = $routeParams.id;
        if ($scope.images.length > 0) {
            $http({
                method: 'POST',
                url: "/Api/IdeaWithAttachements/Attachments",
                headers: { 'Content-Type': false },
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("model", angular.toJson(data.idea));
                    for (var i = 0; i < data.files.length; i++) {
                        formData.append("file" + i, data.files[i].file);
                    }
                    return formData;
                },
                data: { files: $scope.images, idea: newIdea }
            }).
                success(function (data, status, headers, config) {
                    data.comments = [];
                    newIdea = new Idea(data);
                    $scope.ideas.push(newIdea);
                    $scope.images = [];
                    idea.name = "";
                    idea.text = "";
                    idea.codeExample = "";
                    $scope.isSent = false;
                }).
                error(function (data, status, headers, config) {
                    $scope.isSent = false;
                    alert("произошла ошибка");
                });
        } else {
            newIdea.$save(function success(result) {
                result.comments = [];
                $scope.ideas.push(result);
                $scope.images = [];
                idea.name = "";
                idea.text = "";
                idea.codeExample = "";
                $scope.isSent = false;
            });
        }


    };
    $scope.removeIdea = function (idea) {
        idea.$delete(function success() {
            $scope.ideas.splice($scope.ideas.indexOf(idea), 1);
        });
    };
    $scope.beginEditIdea = function (idea) {
        idea.tempName = angular.copy(idea.name);
        idea.tempString = angular.copy(idea.text);
        idea.isEdit = true;
    };
    $scope.editIdea = function (idea) {
        Idea.update({ ideaId: idea.ideaId, text: idea.text, name: idea.name }, function success(result) {
            idea.isEdit = false;
        }
        );
    };
    $scope.endEditIdea = function (idea) {
        idea.text = angular.copy(idea.tempText);
        idea.isEdit = false;
        idea.tempString = "";
    };


    $scope.addComment = function (comment, idea) {
        var newcomment = new Comment();
        newcomment.ideaId = idea.ideaId;
        newcomment.text = comment.text;
        newcomment.groupId = $routeParams.id;
        newcomment.$save(function success(result) {
            var idea = $filter('getIdeaById')($scope.ideas, result.ideaId);
            idea.comments.push(result);
            comment.text = "";
        });
    };
    $scope.removeComment = function (comment, idea) {
        comment.$delete(function success() {
            idea.comments.splice(idea.comments.indexOf(comment), 1);
        });
    };
    $scope.beginEditComment = function (comment) {
        comment.tempString = angular.copy(comment.text);
        comment.isEdit = true;
    };
    $scope.editComment = function (comment) {
        comment.$update(function success() {
            comment.isEdit = false;
        }
        );
    };
    $scope.endEditComment = function (comment) {
        comment.text = angular.copy(comment.tempString);
        comment.isEdit = false;
        comment.tempString = "";
    };


    $scope.like = function (idea) {
        var vote = new Vote();
        vote.authorId = $scope.id.id;
        vote.ideaId = idea.ideaId;
        vote.voteType = Enum.voteType.like;
        vote.$save(function success() {
            idea.isVote = true;
            var updateIdea = Idea.get({ groupId: $routeParams.id, ideaId: idea.ideaId }).$then(function (result) {
                idea.countLike = result.data.countLike;
                idea.countDislike = result.data.countDislike;
            }
            );

        });

    };

    $scope.dislike = function (idea) {
        var vote = new Vote();
        vote.authorId = $scope.id.id;
        vote.ideaId = idea.ideaId;
        vote.voteType = Enum.voteType.dislike;
        vote.$save(function success() {
            idea.isVote = true;
            var updateIdea = Idea.get({ groupId: $routeParams.id, ideaId: idea.ideaId }).$then(function (result) {
                idea.countLike = result.data.countLike;
                idea.countDislike = result.data.countDislike;
            }
            );
        });
    };

    $scope.imageBox = function (image) {
        $scope.opts = {
            backdrop: true,
            keyboard: true,
            scope: $scope,
            backdropClick: true,
            templateUrl: 'template/ImageBox.html',
            controller: 'ImageBoxController',
            resolve: {
                image: function () {
                    return image;
                }
            }
        };
        var d = $dialog.dialog($scope.opts);
        d.open();
    };

}

function MyGroupsController($scope, $filter, Group, Member, User, sharedService, Enum) {
    $scope.groups = [];
    $scope.groups = Group.usersGroups();
    $scope.predicat = 'state';
    $scope.reverse = true;
    $scope.confirmInvite = function (group) {
        var member = new Member();
        member.groupId = group.groupId;
        member.state = Enum.stateGroup.adoptet;
        member.userId = $scope.id.id;
        member.$save(function success() {
            group.state = Enum.stateGroup.adoptet;
            $scope.myGroupInfo.count -= 1;
        });
    };


    $scope.block = function (group) {
        group.isBlock = true;
        Group.update(group,
            function succsess() {

            },
            function error() {
                group.isBlock = false;
            });


    };

    $scope.unBlock = function (group, predicat) {
        group.isBlock = false;
        Group.update(group, function succsess() {

        },
            function error() {
                group.isBlock = false;
            });
    };

};

function MyGroupsArchiveController($scope, Group, Member, User, sharedService, Enum) {
    $scope.groups = [];
    $scope.groups = Group.archiveUsersGroups();
};

function MessageController($scope, $http, $templateCache, User, MessageHub, Message, Enum, $dialog, MessageTimeout, $filter, OutputMessage) {
    function $constructor() {
        $http.get('/Template/inputmessages', { cache: $templateCache }).success(function (result) {
            $templateCache.put('input.html', result);
            $scope.templateInput = 'input.html';
        });
        $http.get('/Template/outputmessages', { cache: $templateCache }).success(function (result) {
            $templateCache.put('output.html', result);
            $scope.templateOutput = 'output.html';
        });

        $scope.counterInput = 1;
        $scope.counterOutput = 1;
        $scope.busy = false;
        $scope.isSent = false;
        MessageTimeout.setTimeout(true);
        $scope.inputMessages = MessageTimeout;
        $scope.outputMessages = [];
        $scope.input = true;
        $scope.newMessage = { recipients: [], text: '' };
        $scope.option = {
            multiple: true,
            minimumInputLength: 1,
            ajax: {
                url: '/api/User?type=3',
                data: function (term) {
                    return 'search=' + encodeURIComponent(term);
                },
                results: function (data, page) {
                    return { results: data, text: 'fullName' };
                }
            }
        };

        if (!MessageHub.isInitialize()) {
            MessageHub.on('received', function (data) {
                $scope.inputMessages.unshift(data);

                if ($scope.inputMessages.length > 10) {
                    $scope.inputMessages.splice(10 * ($scope.counterInput - 1) - 1, 1);
                }

            });

            MessageHub.on('echo', function (data) {
                angular.forEach(data, function (message) {
                    $scope.outputMessages.unshift(message);
                });

                if ($scope.outputMessages.length > 10) {
                    $scope.outputMessages.splice(10 * ($scope.counterOutput - 1) - data.length, data.length);
                }
            });
            MessageHub.initialize(true);
        }

    }
    $constructor();

    $scope.recipientsMessage = function (recipients) {
        var str = "";
        var count = 0;
        angular.forEach(recipients, function (recipient) {

            if (count != 0) {
                str += ", " + recipient.recipientFullName;
            } else {
                str += recipient.recipientFullName;
                count++;
            }

        });
        return str;
    };
    $scope.loadMoreInput = function () {
        $scope.dowland = true;
        $scope.scrollDisableInput = true;
        var type;
        if ($scope.input) {
            var items = Message.query({ pageNumber: $scope.counterInput, pageSize: 10 }, function success() {
                if (items.length !== 0) {

                    for (var i = 0; i < items.length; i++) {
                        $scope.inputMessages.push(items[i]);
                    }
                    $scope.counterInput += 1;
                    if ($scope.counterInput > 3) {
                        $scope.showUpper = true;
                    }
                    $scope.scrollDisableInput = false;

                }
                $scope.dowland = false;

            });
        }

    };
    $scope.loadMoreOutput = function () {
        $scope.dowland = true;
        $scope.scrollDisableOutput = true;
        var type;
        if (!$scope.input) {
            var items = OutputMessage.query({ pageNumber: $scope.counterOutput, pageSize: 10 }, function success() {
                if (items.length !== 0) {

                    for (var i = 0; i < items.length; i++) {
                        $scope.outputMessages.push(items[i]);
                    }
                    $scope.counterOutput += 1;
                    if ($scope.counterOutput > 3) {
                        $scope.showUpper = true;
                    }
                    $scope.scrollDisableOutput = false;

                }
                $scope.dowland = false;

            });
        }

    };
    $scope.send = function (message) {

        var messageInfo = { text: message.text, recipients: [] };
        angular.forEach(message.recipients, function (recipient) {
            messageInfo.recipients.push({ recipientId: recipient.id })

        });
        MessageHub.invoke('send', messageInfo, function (messageOnlyReceiver) {
            $scope.isSent = true;
        });
        if ($scope.input) {
            var d = $dialog.messageBox("Отправлено", "Ваше собщение отправлено, вы можете найти его в вкладке исходящие", "EEEE");
            d.open();
        }

    };
    $scope.up = function () {
        $scope.showUpper = false;
    };

    $scope.inputRoute = function () {
        if ($scope.input == false) {
            $scope.input = true;
            MessageTimeout.setTimeout(true);
            MessageTimeout.onTimeout();
        }
    };
    $scope.outputRoute = function () {
        if ($scope.input == true) {
            MessageTimeout.setTimeout(false);
            $scope.input = false;
            if ($scope.counterOutput < 2) {
                $scope.input = false;
                $scope.dowland = true;
                $scope.scrollDisableOutput = true;
                var items = OutputMessage.query({ pageNumber: $scope.counterOutput, pageSize: 10 }, function success() {
                    if (items.length !== 0) {
                        $scope.outputMessages = [];
                        for (var i = 0; i < items.length; i++) {
                            $scope.outputMessages.push(items[i]);
                        }
                        $scope.counterOutput += 1;
                        if ($scope.counterOutput > 3) {
                            $scope.showUpper = true;
                        }
                        $scope.scrollDisableOutput = false;

                    }
                    $scope.dowland = false;

                });;
            }

            if (!$scope.initOutput) {
                MessageHub.on('recipientRead', function (data) {
                    $filter('getMessageId')($scope.outputMessages, data).read = true;

                });
            }
            $scope.initOutput = true;
        }
    };


    $scope.respond = function (message) {
        $scope.opts = {
            backdrop: true,
            keyboard: true,
            scope: $scope,
            backdropClick: true,
            templateUrl: 'template/responseMessage.html',
            controller: 'ResponseController',
            resolve: {
                senderFullName: function () {
                    return message.senderFullName;
                },
                recipietId: function () {
                    return message.senderId;
                }
            }
        };
        var d = $dialog.dialog($scope.opts);
        d.open();
    };


    $scope.$on('$routeChangeSuccess', function () {
        MessageTimeout.onTimeout();
        MessageTimeout.setTimeout($scope.path == '/myMessage');
    });


};



function ResponseController($scope, dialog, senderFullName, Alerts, MessageHub, recipietId) {
    $scope.senderFullName = senderFullName;
    $scope.Alerts = Alerts;
    $scope.close = function () {
        var result = { status: 'OK', text: $scope.text };
        var messageOnlyReceiver = { text: result.text, recipients: [{ recipientId: recipietId }] };
        MessageHub.invoke('send', messageOnlyReceiver, function (messageOnlyReceiver) {
            dialog.close();
        }, function () { $scope.Alerts.error("Нельзя отправить сообщение заблокированному пользователю") });

    };
};

function MyPageController($scope, $http, $templateCache, $dialog, Idea, Enum, User, MessageHub) {
    function $constructor() {
        $http.get('/Template/myideas', { cache: $templateCache }).success(function (result) {
            $templateCache.put('myideas.html', result);
            $scope.templateMyIdeas = 'myideas.html';
        });
        $http.get('/Template/myideasarchive', { cache: $templateCache }).success(function (result) {
            $templateCache.put('myideasarchive.html', result);
            $scope.templateArchive = 'myideasarchive.html';
        });

        $scope.counterInput = 1;
        $scope.counterOutput = 1;
        $scope.busy = false;
        $scope.isSent = false;
        $scope.MyIdeas = [];
        $scope.ideasArhive = [];
        $scope.myIdeas = true;
        $scope.id.$then(function () {
            $scope.myInfo = User.get({ id: $scope.id.id });
        });
        $scope.savingInterest = false;
        $scope.savingAbout = false;
    }
    $constructor();

    $scope.myIdeasRoute = function () {
        if ($scope.myIdeas == false) {
            $scope.myIdeas = true;
        }
    };
    $scope.archiveRoute = function () {
        if ($scope.myIdeas == true) {
            $scope.myIdeas = false;
            if ($scope.counterOutput < 2) {
                $scope.myIdeas = false;
                $scope.dowland = true;
                $scope.scrollDisableOutput = true;
                var items = Idea.query({ pageNumber: $scope.counterOutput, pageSize: 10, type: Enum.typeIdea.archive }, function success() {
                    if (items.length !== 0) {
                        $scope.outputMessages = [];
                        for (var i = 0; i < items.length; i++) {
                            $scope.ideasArhive.push(items[i]);
                        }
                        $scope.counterOutput += 1;
                        if ($scope.counterOutput > 3) {
                            $scope.showUpper = true;
                        }
                        $scope.scrollDisableOutput = false;

                    }
                    $scope.dowland = false;

                });;
            }
        }
    };

    $scope.loadMore = function () {
        $scope.dowland = true;
        $scope.scrollDisable = true;
        if ($scope.myIdeas) {
            var items = Idea.query({ pageNumber: $scope.counterInput, pageSize: 10, type: Enum.typeIdea.myIdea }, function success() {
                if (items.length !== 0) {

                    for (var i = 0; i < items.length; i++) {
                        $scope.MyIdeas.push(items[i]);
                    }
                    $scope.counterInput += 1;
                    if ($scope.counterInput > 3) {
                        $scope.showUpper = true;
                    }
                    $scope.scrollDisable = false;

                }
                $scope.dowland = false;

            });
        }

    };
    $scope.loadMoreArchive = function () {
        $scope.dowland = true;
        $scope.scrollDisableArchive = true;
        if (!$scope.myIdeas) {
            var items = Idea.query({ pageNumber: $scope.counterOutput, pageSize: 10, type: Enum.typeIdea.archive }, function success() {
                if (items.length !== 0) {

                    for (var i = 0; i < items.length; i++) {
                        $scope.ideasArhive.push(items[i]);
                    }
                    $scope.counterOutput += 1;
                    if ($scope.counterOutput > 3) {
                        $scope.showUpper = true;
                    }
                    $scope.scrollDisableArchive = false;

                }
                $scope.dowland = false;

            });
        }

    };

    $scope.editInterestFunc = function (myInfo) {
        $scope.savingInterest = true;
        myInfo.$update(function succsess() {
            $scope.savingInterest = false;
        });
        $scope.$apply();
    };

    $scope.editAboutFunc = function (myInfo) {
        $scope.savingAbout = true;
        myInfo.$update(function succsess() {
            $scope.savingAbout = false;
        });
        $scope.$apply();
    };

    $scope.password = function (message) {
        $scope.opts = {
            backdrop: true,
            keyboard: true,
            scope: $scope,
            backdropClick: true,
            templateUrl: 'template/password.html',
            controller: 'PasswordController'
        };
        var d = $dialog.dialog($scope.opts);
        d.open();
    };


    $scope.editAvatar = function () {
        $scope.opts = {
            backdrop: true,
            keyboard: true,
            scope: $scope,
            backdropClick: true,
            templateUrl: 'template/avatar.html',
            controller: 'AvatarController'
        };
        var d = $dialog.dialog($scope.opts);
        d.open().then(function (result) {
            if (result) {
                $scope.myInfo.imageUrl = result;
            }
        });
    };
    $scope.imageBox = function (image) {
        $scope.opts = {
            backdrop: true,
            keyboard: true,
            scope: $scope,
            backdropClick: true,
            templateUrl: 'template/ImageBox.html',
            controller: 'ImageBoxController',
            resolve: {
                image: function () {
                    return image;
                }
            }
        };
        var d = $dialog.dialog($scope.opts);
        d.open();
    };
}

function PasswordController($scope, Account, dialog, Alerts, Enum) {
    $scope.password = new Account({ old: "", newP: "", repeat: "" });
    $scope.Alerts = Alerts;
    $scope.change = function (password) {
        if (password.newP === password.repeat) {
            password.$save(function success() {
                dialog.close({ status: Enum.statusDialog.ok });
            },
                function error(data) {
                    if (data.data.type == Enum.typeError.passwordWrong) {
                        $scope.Alerts.error("Старый пароль неправильный");
                    } else {
                        $scope.Alerts.error('Неизвестная ошибка');
                    }

                });
            ;
        }
    };

    $scope.close = function () {
        dialog.close();
    };

}

function AvatarController($scope, $http, Avatar, Alerts, dialog) {


    $scope.Alerts = Alerts;
    $scope.close = function () {
        dialog.close();
    };

    $scope.send = function () {
        $http({
            method: 'POST',
            url: "/Api/User/Avatar",
            headers: { 'Content-Type': false },
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("file", data.file.file);
                return formData;
            },
            data: { file: $scope.image2 }
        }).
                success(function (data) {
                    dialog.close(data.name);
                }).
                error(function (data) {
                    $scope.Alerts.error("произошла ошибка");
                });
    };
}

function MyDraftController($scope, $http, Enum, Draft, User, $dialog) {

    function $constructor() {
        $scope.ideas = [];
        $scope.idea = { name: "", text: "" };
        $scope.id.$then(function () {
            $scope.myInfo = User.get({ id: $scope.id.id });
            $scope.myInfo.$then(function () {

                $scope.myInfo.groups.unshift({
                    groupName: "Опубликовать"
                });
            });
        });
        $scope.counter = 1;
        $scope.busy = false;
        $scope.images = [];
    }
    $constructor();
    $scope.loadMore = function () {
        $scope.dowland = true;
        $scope.scrollDisable = true;
        var items = Draft.query({ pageNumber: $scope.counter, pageSize: 10, type: Enum.typeIdea.myDraft }, function success() {
            if (items.length !== 0) {

                for (var i = 0; i < items.length; i++) {
                    $scope.ideas.push(items[i]);
                }
                $scope.counter += 1;
                if ($scope.counter > 3) {
                    $scope.showUpper = true;
                }
                $scope.scrollDisable = false;

            }
            $scope.dowland = false;

        });
    };
    $scope.addIdea = function (idea) {
        $scope.isSent = true;
        $scope.codeShow = false;
        var newIdea = new Draft();
        newIdea.name = idea.name;
        newIdea.text = idea.text;
        newIdea.codeExample = idea.codeExample;
        if ($scope.images.length > 0) {
            $http({
                method: 'POST',
                url: "/Api/IdeaWithAttachements/Attachments",
                headers: { 'Content-Type': false },
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("model", angular.toJson(data.idea));
                    for (var i = 0; i < data.files.length; i++) {
                        formData.append("file" + i, data.files[i].file);
                    }
                    return formData;
                },
                data: { files: $scope.images, idea: newIdea }
            }).
                success(function (data, status, headers, config) {
                    data.comments = [];
                    newIdea = new Draft(data);
                    $scope.ideas.unshift(newIdea);
                    $scope.images = [];
                    idea.name = "";
                    idea.text = "";
                    idea.codeExample = "";
                    $scope.isSent = false;
                }).
                error(function (data, status, headers, config) {
                    $scope.isSent = false;
                    alert("произошла ошибка");
                });
        } else {
            newIdea.$save(function success(result) {
                result.comments = [];
                $scope.ideas.unshift(result);

                if ($scope.ideas.length > 10) {
                    $scope.ideas.splice(10 * ($scope.counter - 1), 1);
                }

                $scope.images = [];
                idea.name = "";
                idea.text = "";
                idea.codeExample = "";
                $scope.isSent = false;
            });
        }


    };
    $scope.removeIdea = function (idea) {
        idea.$delete(function success() {
            $scope.ideas.splice($scope.ideas.indexOf(idea), 1);
        });
    };
    $scope.beginEditIdea = function (idea) {
        idea.tempName = angular.copy(idea.name);
        idea.tempString = angular.copy(idea.text);
        idea.isEdit = true;
    };
    $scope.editIdea = function (idea) {
        Draft.update({ ideaId: idea.ideaId, text: idea.text, name: idea.name }, function success(result) {
            idea.isEdit = false;
        }
        );
    };
    $scope.endEditIdea = function (idea) {
        idea.text = angular.copy(idea.tempString);
        idea.isEdit = false;
        idea.tempString = "";
    };
    $scope.update = function (selected, idea) {
        Draft.update({ ideaId: idea.ideaId, text: idea.text, name: idea.name, groupId: selected.groupId }, function success(result) {
            $scope.ideas.splice($scope.ideas.indexOf(idea), 1);
        }
        );
    };

    $scope.imageBox = function (image) {
        $scope.opts = {
            backdrop: true,
            keyboard: true,
            scope: $scope,
            backdropClick: true,
            templateUrl: 'template/ImageBox.html',
            controller: 'ImageBoxController',
            resolve: {
                image: function () {
                    return image;
                }
            }
        };
        var d = $dialog.dialog($scope.opts);
        d.open();
    };
}
function selectController($scope) {

    $scope.myInfo.$then(function () {
        $scope.selected = $scope.myInfo.groups[0];
    });

}
function ImageBoxController($scope, dialog, image) {
    $scope.img = image;
    $scope.close = function () {
        dialog.close();
    };
}