﻿angular.module("ngScrollTo", [])
    .directive("scrollTo", ["$window", function ($window) {
        return {
            restrict: "AC",
            compile: function () {
                var document = $window.document;

                function scrollInto(idOrName) {
                    if (!idOrName)
                        $window.scrollTo(0, 0);
                    var el = document.getElementById(idOrName);
                    if (!el) {
                        el = document.getElementsByName(idOrName);
                        if (el && el.length)
                            el = el[0];
                        else
                            el = null;
                    }

                    if (el)
                        el.scrollIntoView();
                }

                return function (scope, element, attr) {
                    element.bind("click", function (event) {
                        scrollInto(attr.scrollTo);
                    });
                };
            }
        };
    }])
    .directive('backButton', function () {
        return {
            restrict: 'A',
            template: '<button class="btn btn-primary">{{back}}</button>',
            scope: {
                back: '@back'
            },
            link: function (scope, element, attrs) {
                element.bind('click', function () {
                    history.back();
                    scope.$apply();
                });
            }
        };
    })
    .directive('businessCard', function ($interpolate) {
        var html ='<div ng-repeat="recipient in recipients" >'
        '<div class="name">{{recipient.recipientFullName}}</div>' +
          '</div>'
      var interpolateContentWith = $interpolate(html);
      return {
          restrict: 'A',
          scope:
              {
                 recipients: '@recipients'
              },
          link: function (scope, element, attributes) {
              var options = {
                  html: true,
                  trigger: 'hover',
                  content: interpolateContentWith(recipients)
              };
              $(element).popover(options);
          }
      }
    }).directive('ngHasfocus', function () {
    return function (scope, element, attrs) {
        scope.$watch(attrs.ngHasfocus, function (nVal, oVal) {
            if (nVal)
                element[0].focus();
            else
                var k = 3;
        });

        element.bind('blur', function () {
            scope.$apply(attrs.ngHasfocus + " = false");
            scope.$eval(attrs.ngFunc);
        });

        element.bind('keydown', function (e) {
            if (e.which == 13) {
                scope.$apply(attrs.ngHasfocus + " = false");
                scope.$eval(attrs.ngFunc);
            }
          
        });
    }
});






