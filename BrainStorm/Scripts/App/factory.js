﻿angular.module('BrFactory', [])
    .factory('MessageTimeout', ['$timeout', 'MessageHub', function ($timeout, MessageHub) {



    function setTimeout(flag) {
        this.enableTimeout = flag;
    }
    function push(data) {
        if (!data.read && this.enableTimeout) {
            if (!data.isTimeout) {
                data.isTimeout = true;
                this.promises.push($timeout(function () {
                    MessageHub.invoke('read', data.messageId, function () {
                        data.read = true;
                    });
                }, 2000))
            }
        }
        this.messages.push(data);
    }
    function unshift(data) {
        this.messages.unshift(data);
        if (this.enableTimeout) {
            this.promises.push($timeout(function () {
                MessageHub.invoke('read', data.messageId, function () {
                    data.read = true;
                });
            }, 2000))
        }
    }

    function onTimeout() {
        var enableTimeout = this.enableTimeout;
        var promises = this.promises;
        angular.forEach(this.messages, function (message) {
            if (!message.read && enableTimeout) {
                if (!message.isTimeout) {
                    message.isTimeout = true;
                    promises.push($timeout(function () {
                        MessageHub.invoke('read', message.messageId, function () {
                            message.read = true;
                        });
                    }, 2000))
                }
            }

        });

    }


    function length() {
        return this.messages.length();
    }
    function splice(position, length) {
        messages.splice(position, length);
    }
    return {
        messages: [],
        promises: [],
        push: push,
        splice: splice,
        length: length,
        unshift: unshift,
        setTimeout: setTimeout,
        onTimeout: onTimeout
    };
    }])
    .factory('Alerts', function ($timeout) {
    var Alerts = {};

    Alerts.messages = [];

    var addMessage = function (msgText, type) {
        var msg = { text: msgText, type: type };
        Alerts.messages.push(msg);
        $timeout(function () {
            Alerts.remove(msg);
        }, 2000);
    };

    Alerts.error = function (msg) {
        addMessage(msg, 'error');
    };

    Alerts.info = function (msg) {
        addMessage(msg, 'info');
    };

    Alerts.remove = function (alert) {
        Alerts.messages.splice(Alerts.messages.indexOf(alert), 1);
    }

    return Alerts;
})
;
