﻿
'use strict'
angular.module('myFilters', [])
    .filter('searchToGroup', function () {
        return function (items, query) {
            if (query) {
                var re = new RegExp(query, 'i');
                var filtered = [];
                angular.forEach(items, function (item) {
                    if (re.test(item.tags) || re.test(item.task) || re.test(item.name)) {
                        filtered.push(item);
                    }
                }
                );
                return filtered;
            }
            return items;
        };
    }).filter('getIdeaById', function () {
        return function (ideas, ideaId) {
            var findIdea;
            angular.forEach(ideas, function (idea) {
                if (idea.ideaId == ideaId) {
                    findIdea = idea;
                }
            });
            return findIdea;
        };
    }).filter('getMessageId', function () {
        return function (messages, messageId) {
            var findMessage;
            angular.forEach(messages, function (message) {
                if (message.messageId == messageId) {
                    findMessage = message;
                }
            });
            return findMessage;
        };
    }).filter('getUserById', function () {
        return function (users, userId) {
            var findUser;
            angular.forEach(users, function (user) {
                if (user.id == userId) {
                    findUser = user;
                }
            });
            return findUser;
        };
    }).filter('searchToUser', function () {
        return function (items, query) {
            if (query) {
                var re = new RegExp(query, 'i');
                var filtered = [];
                angular.forEach(items, function (item) {
                    if (re.test(item.fullName) || re.test(item.interest)) {
                        filtered.push(item);
                    }
                }
                );
                return filtered;
            }
            return items;
        };
    }).filter('textToLine', function () {
        return function (text) {
            return text.split('\n');
        };
    }).filter('orderByCountLike', function () {
        return function (array, enable) {
            function compare(a, b) {
                if (a.countLike < b.countLike)
                    return 1;
                if (a.countLike > b.countLike)
                    return -1;
                if (a.countDislike < b.countDislike)
                    return -1;
                if (a.countDislike > b.countDislike)
                    return 1;
                return 0;
            }
            if (enable) {               
                var arrayCopy = [];
                for (var i = 0; i < array.length; i++) { arrayCopy.push(array[i]); };
                return arrayCopy.sort(compare);
            }
            return array;

        };
    });
