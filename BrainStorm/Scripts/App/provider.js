﻿
angular.module("Providers", []).provider('MessageHub',
    function () {

        var connection;
        var proxy;
        var _initialize;
        function initialize(flag)
        {
            _initialize = flag;
        }
        function isInitialize()
        {
            return _initialize;
        }
        return{

            startHub: function(serverUrl, hubName, startOptions)
            {
               connection = $.hubConnection();
               proxy = connection.createHubProxy(hubName);
               connection.start(startOptions).done(function () { });
               initialize(false);
            },

            $get: ['$rootScope',function ($rootScope) {
                return {
                    on: function (eventName, callback) {
                        proxy.on(eventName, function (result) {
                            $rootScope.$apply(function () {
                                if (callback) {
                                    callback(result);
                                }
                            });
                        });
                    },
                    off: function (eventName, callback) {
                        proxy.off(eventName, function (result) {
                            $rootScope.$apply(function () {
                                if (callback) {
                                    callback(result);
                                }
                            });
                        });
                    },
                    invoke: function (methodName, data, callbackDone, callbackFail) {
                        proxy.invoke(methodName, data)
                            .done(function (result) {
                                $rootScope.$apply(function () {
                                    if (callbackDone) {
                                        callbackDone(result);
                                    }
                                });
                            })
                            .fail(function (result) {

                                $rootScope.$apply(function () {
                                    if (callbackFail) {
                                        callbackFail(result);
                                    }
                                });
                            });
                    },
                    connection: connection,
                    proxy: proxy,
                    initialize: initialize,
                    isInitialize: isInitialize
                }
            }]};
    });