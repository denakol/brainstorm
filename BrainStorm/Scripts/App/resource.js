﻿angular.module('UserServices', ['ngResource']).
        factory('User', function ($resource) {
            return $resource('/api/User/:Id/:getMyId', {}, {
                pageUsers: { method: 'GET', isArray: true, },
                search: { method: 'GET', isArray: true },
                getMyId: { method: 'GET', params: { getMyId: 'Myid' } },
                update: {method: 'PUT'}
            });
        }).factory('Group', function ($resource) {
            return $resource('/api/Group/:Id/:count/:archive', {}, {
                pageGroups: { method: 'GET', isArray: true },
                search: { method: 'GET', isArray: true },
                update: { method: 'PUT' },
                usersGroups: { method: 'GET', isArray: true },
                archiveUsersGroups: { method: 'GET', isArray: true, params: { archive: 'archive' } }
            });
        }).factory('Idea', function ($resource) {
            return $resource('/api/Idea/:Id', { Id: "@ideaId" }, {
                update: { method: 'PUT' },
                getIdea: { method: 'GET' }
            });
        }).factory('Draft', function ($resource) {
            return $resource('/api/Draft/:Id', { Id: "@ideaId" }, {
                update: { method: 'PUT' }
            });
        }).factory('Comment', function ($resource) {
            return $resource('/api/Comment/:Id', { Id: "@commentId" }, {
                update: { method: 'PUT' }

            });
        }).factory('Files', function ($resource) {
            return $resource('/Api/IdeaWithAttachements/Attachments', {
                add: { method: 'Post', headers: { 'Content-Type': false } }
            });
        }).factory('Vote', function ($resource) {
            return $resource('/api/Vote/:Id', { Id: "@voteId" }, {

            });
        }).factory('Message', function ($resource) {
            return $resource('/api/InputMessage/:isAction/:action/:Id', { Id: "@messageId" }, {
                countMessages: { method: 'GET', params: { action: 'NumberOfUnreadMessages', isAction: 'Action' } },
            });
        }).factory('OutputMessage', function ($resource) {
            return $resource('/api/OutputMessage/:Id', { Id: "@messageId" }, {

            });
        }).factory('Account', function ($resource) {
            return $resource('/api/AccountApi/:Id', { Id: "@messageId" }, {
                avatar: {method: 'Post'}
            });
        }).factory('Avatar', function ($resource) {
            return $resource('/api/User/:Id', {  }, {
                avatar: { method: 'Post' }
            });
        }).factory('UserAdmin', function ($resource) {
            return $resource('/api/UserAdmin/:Id', {}, {
            });
        }).factory('Member', function ($resource) {
            return $resource('/api/UserGroup/:isAction/:action/:Id', { Id: "@id" }, {
                members: { method: 'GET', isArray: true },
                countInvites: { method: 'GET', params: { action: 'getCountInvites', isAction: 'Action' } },
                requests: { method: 'GET', params: { action: 'GetRequests', isAction: 'Action' }, isArray: true },

            });
        });