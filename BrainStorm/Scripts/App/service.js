﻿angular.module('serviceBr', [])
    .factory('sharedService', function ($rootScope) {
        var sharedService = {};
        sharedService.query = '';
        sharedService.search = function (query) {
            this.query = query;
            $rootScope.$broadcast('startSearch');
        };
        return sharedService;
    }).service('Enum', [function () {
        var service = {
            stateGroup: { send: 1, adoptet: 2, invite: 3, notSend: 4 ,block:5,admin:6 },
            typeMessage : {input:1,output:2},
            voteType: { like: 1, dislike: 2 },
            stateBrainstorm: { inProcces: 1, ended: 2, archive: 3 },
            typeError: { onlyAdmin: 1, passwordWrong:2 },
            statusDialog: { ok: 1, error: 2, сanceled: 3 },
            typeIdea : {myIdea:1, archive:2, myDraft:3}
        };
        service.stateGroup.get = function (id)
        {
            switch (id) {
                case 1:
                    return 'sent';
                case 2:
                    return 'adoptet';
                case 3:
                    return 'invite';
                case 4:
                    return 'notSent';
            }

        }
        service.stateBrainstorm.get = function (id) {
            switch (id) {
                case service.stateBrainstorm.inProcces:
                    return 'идет';
                case service.stateBrainstorm.ended:
                    return 'завершен';
                case service.stateBrainstorm.archive:
                    return 'в архиве';
            }

        }

        return service;

    }]);
