﻿var baseUri='http://localhost:52580/';

$(document).ready(function () {

    var bool = $("#IsFirstVisit").val() == "true";
    if (bool)
    {
        uri = '/home/FirstVisitModal'
        $.ajax({
            type: "GET",
            url: uri,
            success: function (data) {
                showModalData(data, $('#popup'));
            }
        });
    }

    $('#register').click(function () {
        uri = 'Account/Register'
        $.ajax({
            type: "GET",
            url: uri,
            success: function (data) {
                showModalData(data, $('#popup'));
            }
        });
        return false;
    });
    $('#rememberPassword').click(function () {
        uri = 'Account/RememberPassword'
        $.ajax({
            type: "GET",
            url: uri,
            success: function (data) {
                showModalData(data, $('#popup'));
            }
        });
        return false;
    });
    function showModalData(data, modal) {
        modal.empty();
        modal.append(data);
        modal.modal('show');
    };
   

    

   


})