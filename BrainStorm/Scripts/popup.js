﻿$(document).ready(function () {
    
    $.validator.unobtrusive.parse("form");
    $('#submit').click(function () {
        if ($("#formPopup").valid()) {
            $.ajax({
                type: "POST",
                url: uri,
                data: $("#formPopup").serialize(),
                success: function (data) {
                    showModalData(data, $('#popup'));
                }
            });
        }
    });

    function showModalData(data, modal) {
        modal.empty();
        modal.append(data);
        modal.modal('show');
    };
});