﻿using System.Data.Entity;
using BrainStorm.Framework.UnitOfWork;
using BrainStorm.Model;
using BrainStorm.Repositories.Implementation;
using BrainStorm.Repositories.Interfaces;
using Microsoft.Practices.Unity;

namespace BrainStorm.Web.UnitOfWork
{
    public class BrainStormUnitOfWorkFactory : UnitOfWorkFactoryBase
    {
        public BrainStormUnitOfWorkFactory()
        {
            Container.RegisterType<DbContext, BrainStormContext>(new InjectionConstructor());
                // force Unity to use default ctor
            Container.RegisterType<IGroupRepository, GroupRepository>();
            Container.RegisterType<IUserRepository, UserRepository>();
            Container.RegisterType<IConfirmEmailRepository, ConfirmEmailRepository>();
            Container.RegisterType<IUserGroupRepository, UserGroupRepository>();
            Container.RegisterType<IIdeaRepository, IdeaRepository>();
            Container.RegisterType<ICommentRepository, CommentRepository>();
            Container.RegisterType<IVoteRepository, VoteRepository>();
            Container.RegisterType<IMessageRepository, MessageRepository>();
            Container.RegisterType<IRecipientMessageRepository, RecipientMessageRepository>();

        }
    }
}